# Takes a load of images, calculates the MSE and plots them over time
# Can also be run in 'offline' mode, where images are saved and then loaded


############################################
################ Libraries #################
############################################

# Libraries
import time
import numpy as np
from PIL import Image
from PIL import ImageDraw
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import glob
import matplotlib.pyplot as plt
import os
import shutil

# My own code
import slm.driver as slm
import camera.driver as cam
import util.calc_mse as util









############################################
############# Configuration ################
############################################

# Constants
SLM_NATIVE_PIXELS_X  = 1280
SLM_NATIVE_PIXELS_Y  = 1280
REPLAY_PIXELS_X      = 224#1216#160#640
REPLAY_PIXELS_Y      = 224#1216#160#640

# How many images are we plotting?
NUM_IMAGES_TO_TAKE = 10#20


# Paths to our temporary image folders
no_flip_output_path             = "./output/plot_temp/no_flip_images/"
one_percent_flip_output_path    = "./output/plot_temp/one_percent_flip_images/"
ten_percent_flip_output_path    = "./output/plot_temp/ten_percent_flip_images/"
forty_percent_flip_output_path  = "./output/plot_temp/forty_percent_flip_images/"
# Delete the folders
shutil.rmtree(no_flip_output_path)
shutil.rmtree(one_percent_flip_output_path)
shutil.rmtree(ten_percent_flip_output_path)
shutil.rmtree(forty_percent_flip_output_path)
# Create new/fresh folders for output
os.makedirs(no_flip_output_path)
os.makedirs(one_percent_flip_output_path)
os.makedirs(ten_percent_flip_output_path)
os.makedirs(forty_percent_flip_output_path)
# Which mode are we operating in?
# CHANGE THIS
# temp_image_output_dir = static_output_path
# temp_image_output_dir = switching_output_path

# SLM Display - what image are we showing
# image_location = "./input/hologram/" + "romeo_gs_200.bmp"
# image_location = "./input/hologram/" + "smiley_face_gs_200.bmp"
# image_location = "./input/hologram/" + "smiley_face_gs_20_big.bmp"
# image_location = "./input/hologram/" + "smiley_face_gs_20_little.bmp"
# image_location = "./input/hologram/" + "smiley.bmp"
# image_location = "./input/hologram/" + "peppers_dct.bmp"
# image_location = "./input/hologram/" + "peppers34.bmp"
# image_location = "./input/hologram/" + "mandrill2.bmp"
# image_location = "./input/hologram/" + "mandrill34.bmp"
# image_location = "./input/hologram/" + "vertical_grating.bmp"
# image_location = "./input/hologram/" + "checkerboard.bmp"
# image_location = "./input/hologram/" + "checkerboard_single_pixel.bmp"
# image_location = "./input/hologram/" + "flat_grey_gs_20.bmp"
image_location = "./input/hologram/" + "blank.bmp"
# image_location = "./input/hologram/" + "dot.bmp"
hologram_img_raw = Image.open(image_location)
# Resize for our SLM
hologram_img_raw = hologram_img_raw.resize((SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
hologram_img_gray = hologram_img_raw.convert("L")
# Convert our Image into an array
hologram_array = np.asarray(hologram_img_gray)
buffwriteA = hologram_array
# The other buffer is just inverted
buffwriteB = (buffwriteA + 1) % 2
# We use a variable setting for how many pixels we flip each iteration
element_count_slm = len(hologram_array)
# Have a big long array to lookup our indices in our array
index_lookup_array = np.arange(element_count_slm)
# total_number_of_pixels = element_count_slm*element_count_slm
total_number_of_pixels = element_count_slm # Just do lines for-now
# Start with X percent of all pixels
# number_of_pixels_to_change = (np.int)(element_count_slm * 0.4 )
no_pixels_to_flip                        = 0.0
one_percent_number_of_pixels_to_change   = (np.int)(total_number_of_pixels * 0.01 )
ten_percent_number_of_pixels_to_change   = (np.int)(total_number_of_pixels * 0.10 )
forty_percent_number_of_pixels_to_change = (np.int)(total_number_of_pixels * 0.40 )
print("One percent   - number of pixels to change: ", one_percent_number_of_pixels_to_change)
print("Ten percent   - number of pixels to change: ", ten_percent_number_of_pixels_to_change)
print("Forty percent - number of pixels to change: ", forty_percent_number_of_pixels_to_change)

# Target - what are we comparing to for our MSE
# Load up our Target Image
# image_location = "./input/" + "smiley_face_inv.png"
# image_location = "./input/" + "smiley_face_inv_edges.png"
image_location = "./input/" + "flat.png"
# image_location = "./input/" + "Dot.png"
# image_location = "./input/" + "circle_inv.png"
target_img_raw = Image.open(image_location)
# Dimensions of our target needs to match our camera
target_img_raw = target_img_raw.resize((REPLAY_PIXELS_X, REPLAY_PIXELS_Y))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
target_img_gray = target_img_raw.convert("F")
# Print out what format we are in to make sure
print("Loaded Target Image Format is: ", target_img_gray.format, target_img_gray.size, target_img_gray.mode)
# Convert our Image into an array
target_array = np.asarray(target_img_gray)

# Define our ROI Mask
# ROI is simply a simply a rectangular mask which is specified as [x0, y0, x1+1, y1+1 ]
# ROI entry is the same as the ImageDraw.rectangle SPI (https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html#functions)
roi_mask_xy = [46, 103, 51, 111]









############################################
############## Image Capture ###############
############################################

# Init our Hardware
# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()
# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init()

# We have this as a function so we can call it several times on slightly different configs (static SLM, switching 1%, 5%, 10%, etc...)
def capture_image_sequence_and_calc_mse(config_select_mode, hologram_array, number_of_pixels_to_change):

    # Data Structures
    buffwriteA = np.asarray(hologram_array)
    buffwriteB = np.asarray(hologram_array)

    # Super-loop to take Images, saving as we go
    for i in range(NUM_IMAGES_TO_TAKE):

        # Now perturb out SLM image for the next cycle
        # Note we only do this if we are looking at a dynamic mode (ie: not in statuc)
        if (config_select_mode!=no_flip_output_path):

            # First we try to flip some bits to compute a candidate improved solution
            # How many pixels are we going to flip?
            number_of_pixels_to_flip = number_of_pixels_to_change
            # Convert out array to 1D temporarily for flipping
            candid_hologram_array    = np.copy(hologram_array)
            candid_hologram_array_1D = candid_hologram_array.reshape(-1)
            # Generate a mask of pixels indices to modify - this is just a simple 1D vector
            # mask = np.random.randint(0, element_count, size=number_of_pixels_to_flip)
            mask = np.random.choice(index_lookup_array, number_of_pixels_to_flip, replace=False)
            # print(mask)
            # Use the mask to flip our bits
            # candid_hologram_array_1D[mask] = np.logical_not(candid_hologram_array_1D[mask])
            # candid_hologram_array_1D[mask] = (candid_hologram_array_1D[mask] + 1) % 2
            # selected_values[:] = 3
            # Convert back to 2D and update for next iteration
            candid_hologram_array = candid_hologram_array_1D.reshape(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y)
            # Temporary - modifying an entire row at a time for debug
            temp_mask = mask#[1, 3, 4]
            candid_hologram_array[temp_mask, :] = 255  # Syntax to edit all of the column values for a given row
            # candid_hologram_array[:, temp_mask] = 255  # Syntax to edit all of the column values for a given row
            # Update our Buffers for next iteration
            buffwriteA = candid_hologram_array
            # buffwriteB = (buffwriteA + 1) % 2
            buffwriteB = 255 - buffwriteA

        # Update our SLM frame
        slm.update_frame_buffer(buffwriteA)

        # Buffer Swap
        slm.spi_show_buf_A()


        # Update it on our QT GUI
        # bufA_view.show()
        # bufA_view.setImage(replay_field_image)

        # Update our SLM frame
        slm.update_frame_buffer(buffwriteB)

        # Buffer Swap
        slm.spi_show_buf_B()

        # Print out our SLM status
        # slm.check_status()

        # Get our next camera picture
        camera_img_B = cam.grab_imag()
        replay_field_image_B = np.copy(Image.fromarray(camera_img_B, 'L'))
        # Update it on our QT GUI
        # bufB_view.show()
        # bufB_view.setImage(replay_field_image)
        # Alternate option is to do some extra filtering and clear all the values below the noise-floor
        # replay_field_image_filtered = np.where( replay_field_image<17, 0, replay_field_image)
        # bufB_view.show()
        # bufB_view.setImage(replay_field_image_filtered)

        # Draw our ROI on the A Buffer
        image_drawer_replay_A = ImageDraw.Draw(replay_field_A_image)
        image_drawer_replay_A.rectangle(roi_mask_xy, outline='green')

        # Save our Image
        image_file_name = config_select_mode + "capture" + str(i) + ".bmp"
        replay_field_A_image.save(image_file_name)
        print("Image Captured: " + image_file_name)

        # Save our SLM bitmap that we sent        
        slm_buffer_image = Image.fromarray(buffwriteA, 'L')
        # slm_buffer_image.show()
        slm_buffer_file_name = config_select_mode + "slm_buffer" + str(i) + ".bmp"
        slm_buffer_image.save(slm_buffer_file_name)
        print("SLM Bitmap: " + slm_buffer_file_name)


    # Load all the images we just saved into a List
    images_to_load = glob.glob( config_select_mode + '/capture*.bmp')
    # Our list to load, we store a list of arrays
    images_to_plot_array_list = []
    # Wildcard Search
    for image in images_to_load:
        # Open + Load into our list
        with Image.open(image) as file:
            image_as_array = np.array(file)
            images_to_plot_array_list.append(image_as_array)

    # Now, iterate through our list, calculating MSE for each step
    mse_values_list = []
    for captured_image in images_to_plot_array_list:
        # Calc MSE - no ROI
        # mse = util.calc_mse_whole_image(captured_image, target_array)
        # Calc MSE - with ROI
        mse = util.calc_mse_with_roi(captured_image, target_array, roi_mask_xy)
        print(mse)
        mse_values_list.append(mse)

    # Return list of MSE values
    return mse_values_list
        

# Run capture for each config mode we are interested in
no_pixels_flipped_mse_list          = capture_image_sequence_and_calc_mse(no_flip_output_path,              hologram_array, no_pixels_to_flip)
one_percent_pixels_flipped_mse_list = capture_image_sequence_and_calc_mse(one_percent_flip_output_path,     hologram_array, one_percent_number_of_pixels_to_change)
ten_percent_pixels_flipped_mse_list = capture_image_sequence_and_calc_mse(ten_percent_flip_output_path,     hologram_array, ten_percent_number_of_pixels_to_change)
forty_percent_pixels_flipped_mse_list = capture_image_sequence_and_calc_mse(forty_percent_flip_output_path, hologram_array, forty_percent_number_of_pixels_to_change)



############################################
########### Plot MSE over Time #############
############################################

# Now plot
# print()
x = np.arange(0, len(no_pixels_flipped_mse_list)) 
# y = mse_values_list
plt.title("MSE over Time") 
plt.xlabel("Image Number") 
plt.ylabel("MSE") 
plt.plot(x, no_pixels_flipped_mse_list,            label="No  Pixels Flipped") 
plt.plot(x, one_percent_pixels_flipped_mse_list,   label="1%  Pixels Flipped") 
plt.plot(x, ten_percent_pixels_flipped_mse_list,   label="10% Pixels Flipped") 
plt.plot(x, forty_percent_pixels_flipped_mse_list, label="40% Pixels Flipped") 
plt.legend(loc="upper left")
plt.show()

    # Housekeeping at the end of the loop
    # QtGui.QApplication.processEvents()    # you MUST process the plot now
    # print("Still Alive...")
    # time.sleep(0.2)
    # time.sleep(0.1)



# # Display the widget as a new window
# window.show()

# ## Start Qt event loop unless running in interactive mode or using pyside.
# if __name__ == '__main__':
#     import sys
#     if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
#         QtGui.QApplication.instance().exec_()