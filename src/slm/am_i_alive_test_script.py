
# Test Script for debugging connections
# When things are working, you should see the output:
# COM4
# Test Sequence 1 Values - Check Hardware ID Register
# Sent: f800
# Recv: 20  
# Test Sequence 2 Values - Check Status Register
# Sent: 8100
# Recv: 06  
# Test Sequence 3 Values - Set Clock Register
# Sent: 0940
# Recv: 00  
# Test Sequence 3 Values - Readback Clock Register
# Sent: 8900
# Recv: 40

# No SLM connected, they are all 0
# COM4
# Test Sequence 1 Values - Check Hardware ID Register
# Sent: f800
# Recv: 00  
# Test Sequence 2 Values - Check Status Register
# Sent: 8100
# Recv: 00  
# Test Sequence 3 Values - Set Clock Register
# Sent: 0940
# Recv: 00  
# Test Sequence 3 Values - Readback Clock Register
# Sent: 8900

# PySerial - installed via pip
import serial
import time





# Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
driver_port_ser = serial.Serial('COM9', 3125000, timeout=1)      # Need to look this up manually
# Check which port was REALLY used
print(driver_port_ser.name)







# Test Sequence #1, look at the Hardware Configuration Register and the result you get, should be 'F8' ideally
write_me = bytes.fromhex('F800')
# write_me = bytes.fromhex('CCCC')
driver_port_ser.write(write_me)
# Wait 10ms so we know value has been picked up
time.sleep(1)
# Read the Reply Byte - Should hopefully get 0xF8
recv = driver_port_ser.read(10)
# Print Results
print("Test Sequence 1 Values - Check Hardware ID Register")
print("Sent: " + write_me.hex())
print("Recv: " + recv.hex())
# Clear our buffers for the next operation
driver_port_ser.reset_input_buffer()
driver_port_ser.reset_output_buffer()
time.sleep(1)





# Test Sequence #2, try to set the clock
write_me = bytes.fromhex('8100')
driver_port_ser.write(write_me)
# Wait 10ms so we know value has been picked up
time.sleep(1)
# Read the Reply Byte - Should hopefully get 0xF8
recv = driver_port_ser.read(10)
# Print Results
print("Test Sequence 2 Values - Check Status Register")
print("Sent: " + write_me.hex())
print("Recv: " + recv.hex())
# Clear our buffers for the next operation
driver_port_ser.reset_input_buffer()
driver_port_ser.reset_output_buffer()
time.sleep(1)





# Test Sequence #3, set clock
write_me = bytes.fromhex('0940')
driver_port_ser.write(write_me)
# Wait 10ms so we know value has been picked up
time.sleep(1)
# Read the Reply Byte - Should hopefully get 0xF8
recv = driver_port_ser.read(2)
# Clear our buffers for the next operation
driver_port_ser.reset_input_buffer()
driver_port_ser.reset_output_buffer()
time.sleep(1)
# Print Results
print("Test Sequence 3 Values - Set Clock Register")
print("Sent: " + write_me.hex())
print("Recv: " + recv.hex())

# Readback the clock set
write_me = bytes.fromhex('8900')
driver_port_ser.write(write_me)
# Wait 10ms so we know value has been picked up
time.sleep(1)
# Read the Reply Byte - Should hopefully get 0xF8
recv = driver_port_ser.read(2)
# Clear our buffers for the next operation
driver_port_ser.reset_input_buffer()
driver_port_ser.reset_output_buffer()
time.sleep(1)
# Print Results
print("Test Sequence 3 Values - Readback Clock Register")
print("Sent: " + write_me.hex())
print("Recv: " + recv.hex())






# Close the serial port
driver_port_ser.close()




