# Use our local python bindings for the FT601 DLL
from slm.ft601.build.lib.ftd3xx import ftd3xx
# import ftd3xx._ftd3xx_win32 as _ft
import slm.ft601.ftd3xx._ftd3xx_win32 as _ft

# PySerial - installed via pip
import serial
import time

# For logging/debug
import logging

# Temp for testing
from PIL import Image
import numpy as np

# Static variable for D3XX so we get scope and lifetime over multiple function calls
D3XX = 0
# Same for our COM port
driver_port_ser = 0





def GetInfoFromStringDescriptor(stringDescriptor):

    desc = bytearray(stringDescriptor)
	
    len = int(desc[0])
    Manufacturer = ""
    for i in range(2, len, 2):
        Manufacturer += "{0:c}".format(desc[i])
    desc = desc[len:]

    len = desc[0]
    ProductDescription = ""
    for i in range(2, len, 2):
        ProductDescription += "{0:c}".format(desc[i])
    desc = desc[len:]

    len = desc[0]
    SerialNumber = ""
    for i in range(2, len, 2):
        SerialNumber += "{0:c}".format(desc[i])
    desc = desc[len:]
	
    return {'Manufacturer': Manufacturer,
        'ProductDescription': ProductDescription,
        'SerialNumber': SerialNumber}

def DisplayChipConfiguration(cfg):

    print("Chip Configuration:")
    print("\tVendorID = %#06x" % cfg.VendorID)
    print("\tProductID = %#06x" % cfg.ProductID)
	
    print("\tStringDescriptors")
    STRDESC = GetInfoFromStringDescriptor(cfg.StringDescriptors)
    print("\t\tManufacturer = %s" % STRDESC['Manufacturer'])
    print("\t\tProductDescription = %s" % STRDESC['ProductDescription'])
    print("\t\tSerialNumber = %s" % STRDESC['SerialNumber'])
	
    print("\tInterruptInterval = %#04x" % cfg.bInterval)
	
    bSelfPowered = "Self-powered" if (cfg.PowerAttributes & _ft.FT_SELF_POWERED_MASK) else "Bus-powered"
    bRemoteWakeup = "Remote wakeup" if (cfg.PowerAttributes & _ft.FT_REMOTE_WAKEUP_MASK) else ""
    print("\tPowerAttributes = %#04x (%s %s)" % (cfg.PowerAttributes, bSelfPowered, bRemoteWakeup))
	
    print("\tPowerConsumption = %#04x" % cfg.PowerConsumption)
    print("\tReserved2 = %#04x" % cfg.Reserved2)

    fifoClock = ["100 MHz", "66 MHz"]	
    print("\tFIFOClock = %#04x (%s)" % (cfg.FIFOClock, fifoClock[cfg.FIFOClock]))
	
    fifoMode = ["245 Mode", "600 Mode"]
    print("\tFIFOMode = %#04x (%s)" % (cfg.FIFOMode, fifoMode[cfg.FIFOMode]))
	
    channelConfig = ["4 Channels", "2 Channels", "1 Channel", "1 OUT Pipe", "1 IN Pipe"]
    print("\tChannelConfig = %#04x (%s)" % (cfg.ChannelConfig, channelConfig[cfg.ChannelConfig]))
	
    print("\tOptionalFeatureSupport = %#06x" % cfg.OptionalFeatureSupport)
    print("\t\tBatteryChargingEnabled  : %d" % 
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLEBATTERYCHARGING) >> 0) )
    print("\t\tDisableCancelOnUnderrun : %d" % 
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_DISABLECANCELSESSIONUNDERRUN) >> 1) )
	
    print("\t\tNotificationEnabled     : %d %d %d %d" %
	    (((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLENOTIFICATIONMESSAGE_INCH1) >> 2),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLENOTIFICATIONMESSAGE_INCH2) >> 3),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLENOTIFICATIONMESSAGE_INCH3) >> 4),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLENOTIFICATIONMESSAGE_INCH4) >> 5) ))
		
    print("\t\tUnderrunEnabled         : %d %d %d %d" %
        (((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_DISABLEUNDERRUN_INCH1) >> 6),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_DISABLEUNDERRUN_INCH2) >> 7),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_DISABLEUNDERRUN_INCH3) >> 8),
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_DISABLEUNDERRUN_INCH4) >> 9) ))
		
    print("\t\tEnableFifoInSuspend     : %d" % 
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_SUPPORT_ENABLE_FIFO_IN_SUSPEND) >> 10) )
    print("\t\tDisableChipPowerdown    : %d" % 
        ((cfg.OptionalFeatureSupport & _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_SUPPORT_DISABLE_CHIP_POWERDOWN) >> 11) )
    print("\tBatteryChargingGPIOConfig = %#02x" % cfg.BatteryChargingGPIOConfig)
	
    print("\tFlashEEPROMDetection = %#02x (read-only)" % cfg.FlashEEPROMDetection)
    print("\t\tCustom Config Validity  : %s" % 
        ("Invalid" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_CUSTOMDATA_INVALID)) else "Valid") )
    print("\t\tCustom Config Checksum  : %s" % 
        ("Invalid" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_CUSTOMDATACHKSUM_INVALID)) else "Valid") )
    print("\t\tGPIO Input              : %s" % 
        ("Used" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_GPIO_INPUT)) else "Ignore") )
    if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_GPIO_INPUT)):
        print("\t\tGPIO 0                  : %s" % 
            ("High" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_GPIO_0)) else "Low") )
        print("\t\tGPIO 1                  : %s" % 
            ("High" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_GPIO_1)) else "Low") )
    print("\t\tConfig Used             : %s" % 
        ("Custom" if (cfg.FlashEEPROMDetection & (1<<_ft.FT_CONFIGURATION_FLASH_ROM_BIT_CUSTOM)) else "Default") )
		
    print("\tMSIO_Control = %#010x" % cfg.MSIO_Control)
    print("\tGPIO_Control = %#010x" % cfg.GPIO_Control)
    print("")


def enter_spi_update():

    global driver_port_ser

    # Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
    # driver_port_ser = serial.Serial('COM4', 3125000, timeout=0.1)      # Need to look this up manually
    # Check which port was REALLY used
    # print(driver_port_ser.name)

    # Wakeup + Enable Serial/SPI Update Commands
    write_me = bytes.fromhex('0106')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.01)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.01)
    # Print Results
    print("Waking up our SLM with external updates")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())

    # Keep the serial port open after this

    # Close the serial port
    # driver_port_ser.close()


def check_status():

    global driver_port_ser

    # Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
    # driver_port_ser = serial.Serial('COM4', 3125000, timeout=0.1)      # Need to look this up manually
    # Check which port was REALLY used
    # print(driver_port_ser.name)
    
    # Check our Status
    write_me = bytes.fromhex('8300')
    driver_port_ser.write(write_me)
    # Wait 1ms so we know value has been picked up
    time.sleep(0.001)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(10)
    # # Print Results
    # print("Status Registers:")
    # print("Sent: " + write_me.hex())
    # print("Recv: " + recv.hex())
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.001)


    # Close the serial port
    # driver_port_ser.close()


def spi_show_buf_A():

    global driver_port_ser

    # Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
    # driver_port_ser = serial.Serial('COM4', 3125000, timeout=0.1)      # Need to look this up manually
    # Check which port was REALLY used
    # print(driver_port_ser.name)

    # PanelUpdate with DestBuf = A, SrcBuf = B
    write_me = bytes.fromhex('0810')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.001)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    # time.sleep(0.01)
    # # Print Results
    # print("Waking up our SLM with external updates")
    # print("Sent: " + write_me.hex())
    # print("Recv: " + recv.hex())

    # Close the serial port
    # driver_port_ser.close()

def spi_show_buf_B():

    global driver_port_ser

    # Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
    # driver_port_ser = serial.Serial('COM4', 3125000, timeout=0.1)      # Need to look this up manually
    # Check which port was REALLY used
    # print(driver_port_ser.name)

    # PanelUpdate with DestBuf = A, SrcBuf = B
    write_me = bytes.fromhex('0820')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.001)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    # time.sleep(0.01)
    # Print Results
    # print("Waking up our SLM with external updates")
    # print("Sent: " + write_me.hex())
    # print("Recv: " + recv.hex())

    # Close the serial port
    # driver_port_ser.close()


def init():
    #################################################################
    ############################ Init ###############################
    #################################################################
    # First setup our Init such that:
    #  - it runs through the startup sequence
    #  - clock is set
    #  - SLM is put into external update mode


    global driver_port_ser

    # Open a serial PORT - Windows style (COM7) or Unix (/dev/tty/USB0)
    driver_port_ser = serial.Serial('COM11', 3125000, timeout=0.1)      # Need to look this up manually
    # Check which port was REALLY used
    print(driver_port_ser.name)


    # Look at the Hardware Configuration Register and the result you get, should be 'F8' ideally
    write_me = bytes.fromhex('F800')
    # write_me = bytes.fromhex('CCCC')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.1)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(10)
    # Print Results
    print("Checking Hardware ID Register - should be 'F8'")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.1)

    # Set the clock
    write_me = bytes.fromhex('0940')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.1)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.1)
    # Print Results
    print("Setting Clock Register to 62.5 MHz")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())
    # Readback the clock set
    write_me = bytes.fromhex('8900')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.1)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.1)
    # Print Results
    print("Reading Back Clock Register -should be 62.5 MHz")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())

    # Wakeup + Enable External Command Updates
    # write_me = bytes.fromhex('0102') # Regualr Option
    write_me = bytes.fromhex('0112') # Swizzled Option
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.1)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(2)
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.1)
    # Print Results
    print("Waking up our SLM with external updates")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())

    # Check our Status
    write_me = bytes.fromhex('8100')
    driver_port_ser.write(write_me)
    # Wait 10ms so we know value has been picked up
    time.sleep(0.1)
    # Read the Reply Byte - Should hopefully get 0xF8
    recv = driver_port_ser.read(10)
    # Print Results
    print("Status Registers:")
    print("Sent: " + write_me.hex())
    print("Recv: " + recv.hex())
    # Clear our buffers for the next operation
    driver_port_ser.reset_input_buffer()
    driver_port_ser.reset_output_buffer()
    time.sleep(0.1)

    # Close the serial port
    # driver_port_ser.close()

    # Now open up our USB3 Driver and leave it open
    # Use the global D3XX to achieve this
    global D3XX

    # Write out a single frame of half-on, half-off using main USB3 interface

    # First open our FTDI USB3 chip
    # print("INPUT: CHANNELS=%s, SIZE=%d, ITERATION=%d, WRITE=%s, READ=%s, STRESS=%s" % 
    #     (channelsToTest, transferSize, transferIteration, bWrite, bRead, bStressTest))
    # print("")
    # print("")
    # raise exception on error
    # ftd3xx.raiseExceptionOnError(True)
    # if sys.platform == 'linux2':
    #     DemoTurnOffPipeThreads()

    # check connected devices
    numDevices = ftd3xx.createDeviceInfoList()
    if (numDevices == 0):
        print("ERROR: Please check environment setup! No device is detected.")
        # return False
    print("Detected %d device(s) connected." % numDevices)
    devList = ftd3xx.getDeviceInfoList()	
    # DisplayDeviceList(numDevices, devList)
    devIndex = 0#SelectDevice(numDevices)

    # open the first device (index 0)
    D3XX = ftd3xx.create(devIndex, _ft.FT_OPEN_BY_INDEX)
    if (D3XX is None):
        print("ERROR: Please check if another D3XX application is open!")
        # return False
    # get the version numbers of driver and firmware
    # DisplayVersions(D3XX)
    # if (sys.platform == 'win32' and D3XX.getDriverVersion() < 0x01020006):
    #     print("ERROR: Old kernel driver version. Please update driver from Windows Update or FTDI website!")
    #     D3XX.close()
        # return False
    # check if USB3 or USB2		
    devDesc = D3XX.getDeviceDescriptor()
    bUSB3 = devDesc.bcdUSB >= 0x300
    # if (bUSB3 == False and transferSize==16*1024*1024):
    #     transferSize=4*1024*1024
    # if (bUSB3 == False):
    #     print("Warning: Device is connected using USB2 cable or through USB2 host controller!")
    # validate chip configuration
    cfg = D3XX.getChipConfiguration()
    DisplayChipConfiguration(cfg)
    numChannels = [4, 2, 1, 0, 0]
    numChannels = numChannels[cfg.ChannelConfig]
    if (numChannels == 0):
        numChannels = 1
        if (cfg.ChannelConfig == _ft.FT_CONFIGURATION_CHANNEL_CONFIG_1_OUTPIPE):
            bWrite = True
            bRead = False
        elif (cfg.ChannelConfig == _ft.FT_CONFIGURATION_CHANNEL_CONFIG_1_INPIPE):
            bWrite = False
            bRead = True
    if (cfg.OptionalFeatureSupport &
        _ft.FT_CONFIGURATION_OPTIONAL_FEATURE_ENABLENOTIFICATIONMESSAGE_INCHALL):
        print("invalid chip configuration: notification callback is set")	
        D3XX.close()
        # return False
    # delete invalid channels		
    # for channel in range(len(channelsToTest)-1, -1, -1):
    #     if (channelsToTest[channel] >= numChannels):
    #         del channelsToTest[channel]	
    # if (len(channelsToTest) == 0):
    #     D3XX.close()
        # return


def update_frame_buffer(frame_buffer):

    # Use the global D3XX to achieve this
    global D3XX
    
    # Load up a frame of data
    # Our buffer should already be a nice array of '1's and '0's by the time it gets here
    target_array = np.asarray(frame_buffer)
    # print(target_array)
    # binary_phase_packed_array = np.packbits(target_array, axis=None, bitorder='big')
    binary_phase_packed_array = np.packbits(target_array, axis=None, bitorder='little')
    # Reshape our array to be 1280 lines of 160 bytes long - this is the structure of the frame buffer that the SLM expects
    SLM_BYTES_PER_LINE = 160
    SLM_NUM_LINES      = 1280
    binary_phase_packed_2d = binary_phase_packed_array.reshape(SLM_BYTES_PER_LINE, SLM_NUM_LINES)
    buffwriteA = binary_phase_packed_2d.tobytes()
    # print(binary_phase_packed_2d)
    # print(buffwriteA)
    # print(len(buffwriteA))
    # img = Image.fromarray(binary_phase_packed_2d)
    # img.show()

    # Pump out our frame of data
    #//ftStatus = d3xxDevice.WritePipe(0x02, Frame1, (UInt32)total_bytes_per_frame, ref bytesWritten);
    #//Thread.Sleep(10);
    # Write A
    bytesWritten = D3XX.writePipe(0x02, buffwriteA, len(buffwriteA))
    # print(bytesWritten)
    # # time.sleep(2)
    # # # Write B
    # bytesWritten = D3XX.writePipe(0x02, buffwriteB, len(buffwriteB))
    # print(bytesWritten)
    # # time.sleep(10)
    

    # [OPTIONAL] - Read Results

    # Close
    # D3XX.close()
    # D3XX = 0	
	