
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv
import bisect

# My own code
import slm.driver as slm
import camera.driver as cam

################################################################
# Initialisation
################################################################

def draw_macro_pixel(row, col, size, ptr_left, ptr_top, value, image_np):
# row, col, size, ptr_left, ptr_top, value, image_np
    image_np[ptr_top + row*size:ptr_top + row*size + size ,\
            ptr_left + col*size:ptr_left + col*size + size] = value
    
    return image_np

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init(trigger=True)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('SES demo')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='Display', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='Image', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view, 0, 0)
layout.addWidget(camera_view, 0, 1)

# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()

# Set up Bluejay
ENABLE_BLUEJAY = True
if ENABLE_BLUEJAY == True:
    # Init our SLM
    slm.init()
    # Set our SLM to only update over SPI commands
    slm.enter_spi_update()
    #  ... Top ...
    # Left ... Right
    #     Bottom
    WIDTH_BJ = 10
    ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 230, 1070, 250, 1090 # initial position, 840x840
    pixel_size_BJ = ptr_right_BJ - ptr_left_BJ
    x_dim_BJ = 1280
    y_dim_BJ = 1280
    pix_val_BJ = 0 # Dim
    strip_val_BJ = 1 # Bright
    max_val_BJ = 0
    # Generating test patterns on the fly
    # Creating Guardian
    image_np_BJ = np.full((y_dim_BJ, x_dim_BJ), pix_val_BJ)
    image_np_BJ[: , ptr_left_BJ:ptr_left_BJ + WIDTH_BJ] = strip_val_BJ
    image_np_BJ[: , ptr_right_BJ - WIDTH_BJ: ptr_right_BJ] = strip_val_BJ
    image_np_BJ[ptr_top_BJ:ptr_top_BJ + WIDTH_BJ, :] = strip_val_BJ
    image_np_BJ[ptr_bottom_BJ - WIDTH_BJ: ptr_bottom_BJ, :] = strip_val_BJ
    # buffwriteA = image_np_BJ
    # buffwriteB = (buffwriteA + 1) % 2

count_trigger = 0

time.sleep(0.5)

#  ... Top ...
# Left ... Right
#     Bottom
WIDTH = 10
ptr_left, ptr_right, ptr_top, ptr_bottom = 670, 1270, 240, 840 # initial position, 600x600
pixel_size = ptr_right - ptr_left
x_dim = 1920
y_dim = 1080
pix_val = 220 # Dim
strip_val = 50 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

# Generating test patterns on the fly
# Creating Guardian
image_np = np.full((y_dim, x_dim), pix_val)
image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

# Creating macro pixels
value = [55, 85, 100,115,128,141,155,170,188]

gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for i in range(256):
    qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
img_widget.setPixmap(QPixmap(qImg))

# Set up the number of panels we want to slice n by n
num_panels = 3
num_pixels = 1023 # This is so that it is divisible by 3
# Set the void pixel number
num_void = 64
#   results are stored in a 2d array
res = [[0] * num_panels for _ in range(num_panels)]

res_stored = [0 for _ in range(num_panels*num_panels)]
res_temp = [0 for _ in range(num_panels*num_panels)]
res_hadamard = [[0] * num_panels for _ in range(num_panels)]

# some preprocessing of math
slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

i, j = -num_void, 0
x = (num_pixels - num_panels * 2 * num_void) / num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += x
    j += 1
    slice[j] = i
    j += 1

slice = [int(i) for i in slice]

error = [[0] * num_panels for _ in range(num_panels)]
total_iter = 0

matched_value = [40000, 16000, 4000, 1600, 400, 160, 40, 16, 4, 100000, 40000, 10000, 4000, 1000, 400, 100, 40, 10]

CALIBRATION = True
if CALIBRATION == True:
    CALIB_ITERATION = 36
    res_calib = np.zeros((num_panels, num_panels, 
                        len(matched_value), np.int32(CALIB_ITERATION / len(value))))
    row_cnt = 0

    # Creating a (num_panels by num_panels by 19) LUT value range
    # individual_limit = [[[0] * 19] * num_panels for _ in range(num_panels)] # this leads to a bug
    individual_limit = np.zeros((num_panels, num_panels, len(matched_value) + 1))
    # The head and tail of individual_limit can be pre-assigned
    for p in range(num_panels):
        for q in range(num_panels):
            individual_limit[p][q][0] = 0.1 # First element
            individual_limit[p][q][-1] = 120 # Last element

def find_individual_range(value, ind_limit):
    idx = bisect.bisect_left(ind_limit, value) - 1
    if idx == -1:
        idx = 0
    if idx >= len(matched_value):
        return np.nan
    return matched_value[idx]

# We'd rather use a simplified method at the moment
# This below calibration is based on Experiment2, 5d842fc7739f0c4239def96c291a2c538fe3e019 git commit
limit = [0.1, 1.90, 3.24, 4.76, 6.06, 7.30, 8.68, 9.92, 11.15, 11.89, 17.51, 27.34, 37.42, 46.41, 55.12, 65.90, 75.50, 84.17, 120] # This needs to be a sorted list

def find_range(value):
    idx = bisect.bisect_left(limit, value) - 1
    if idx == -1:
        idx = 0
    if idx >= len(matched_value):
        return np.nan
    return matched_value[idx]

TIME = 0.1

################################################################
# Garbage operation
################################################################

# We observe a delay in the process, this is to buy time for the system
for i in range(5):
    if ENABLE_BLUEJAY == True:
        matrix_BJ = np.ones((num_panels, num_panels), np.int32)
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                idx = matrix_BJ[row][col]
                image_np_BJ = draw_macro_pixel(row,col,np.int32(pixel_size_BJ / num_panels),
                                            ptr_left_BJ,ptr_top_BJ,idx,image_np_BJ)
        buffwriteA = image_np_BJ
        buffwriteB = (buffwriteA + 1) % 2
        # Update our SLM1 frame
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()
        slm.check_status()
        time.sleep(TIME)

    # idx = i % len(value)
    idx = 2
    for row in range(0,num_panels):
        for col in range(0,num_panels):
            image_np = draw_macro_pixel(row,col,np.int32(pixel_size / num_panels),ptr_left,ptr_top,value[idx],image_np)
            gray = np.require(image_np, np.uint8, 'C')
    qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
    qImg.ndarray = gray
    for i in range(256):
        qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
    img_widget.setPixmap(QPixmap(qImg))

    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now
    time.sleep(0.5)

    # Get our next camera picture
    camera_img = cam.trig_imag(2)
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)

    if ENABLE_BLUEJAY == True:
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()
        slm.check_status()
        time.sleep(TIME)
    else:
        # Give the hardware system some time
        time.sleep(0.5)

    # Store the values by slicing
    for p in range(num_panels):
        for q in range(num_panels):
            res[p][q] = np.average\
                (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])
            # res_calib[p][q][idx][row_cnt] = res[p][q]
    print(res)

################################################################
# The calibration steps
# TODO: I dont know why, but clearly there is a lag in terms of camera outputs
# if we print only the intensity as fixed values, starting from the second iteration
# the output becomes stable, adding a sleep in front of processEvents seems to have
# solved the issue.
################################################################
idx = 0 # reset parameter
row_cnt = 0 # reset just in case

time.sleep(1)

if CALIBRATION == True:
    for iter in range(CALIB_ITERATION):
        if ENABLE_BLUEJAY == True:
            matrix_BJ = np.ones((num_panels, num_panels), np.int32)
            for row in range(0,num_panels):
                for col in range(0,num_panels):
                    idx = matrix_BJ[row][col]
                    image_np_BJ = draw_macro_pixel(row, col, np.int32(pixel_size_BJ / num_panels),
                                                ptr_left_BJ, ptr_top_BJ, idx, image_np_BJ)
            buffwriteA = image_np_BJ
            buffwriteB = (buffwriteA + 1) % 2
            # Update our SLM1 frame
            slm.update_frame_buffer(buffwriteA)
            slm.spi_show_buf_A()
            slm.check_status()
            time.sleep(TIME)

        idx = iter % len(value)
        print(value[idx])
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                image_np = draw_macro_pixel(row, col, np.int32(pixel_size / num_panels), ptr_left, ptr_top, value[idx], image_np)
                gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for i in range(256):
            qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
        img_widget.setPixmap(QPixmap(qImg))

        loaded_view.show()
        loaded_view.setImage(image_np, levels=(0,255))

        # Housekeeping at the end of the loop
        QtGui.QApplication.processEvents()    # you MUST process the plot now
        time.sleep(0.5)

        # Get our next camera picture
        camera_img = cam.trig_imag(2)
        replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
        replay_field_image = replay_field_image.astype('int16')

        # time.sleep(0.5)
        # Update it on our QT GUI
        camera_view.show()
        camera_view.setImage(replay_field_image)

        if ENABLE_BLUEJAY == True:
            # Update our SLM1 frame for dc balancing
            slm.update_frame_buffer(buffwriteB)
            slm.spi_show_buf_B()
            slm.check_status()
            time.sleep(TIME + 0.5)
        else:
            # Give the hardware system some time
            time.sleep(0.5)

        if iter != 0 and iter % len(value) == 0:
            row_cnt += 1 # we only count down one row after a full set

        # Store the values by slicing
        for p in range(num_panels):
            for q in range(num_panels):
                res[p][q] = np.average\
                    (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])
                res_calib[p][q][idx][row_cnt] = res[p][q]



    row_cnt = 0 # reset
    # Do again, now with BJ at zero
    for iter in range(CALIB_ITERATION):
        if ENABLE_BLUEJAY == True:
            matrix_BJ = np.zeros((num_panels, num_panels), np.int32)
            for row in range(0,num_panels):
                for col in range(0,num_panels):
                    idx = matrix_BJ[row][col]
                    image_np_BJ = draw_macro_pixel(row,col,np.int32(pixel_size_BJ / num_panels),
                                                ptr_left_BJ,ptr_top_BJ,idx,image_np_BJ)
            buffwriteA = image_np_BJ
            buffwriteB = (buffwriteA + 1) % 2
            # Update our SLM1 frame
            slm.update_frame_buffer(buffwriteA)
            slm.spi_show_buf_A()
            slm.check_status()
            time.sleep(TIME)

        idx = iter % len(value)
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                image_np = draw_macro_pixel(row,col,np.int32(pixel_size / num_panels),ptr_left,ptr_top,value[idx],image_np)
                gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for i in range(256):
            qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
        img_widget.setPixmap(QPixmap(qImg))

        loaded_view.show()
        loaded_view.setImage(image_np, levels=(0,255))

        # Housekeeping at the end of the loop
        QtGui.QApplication.processEvents()    # you MUST process the plot now
        time.sleep(0.5)

        # Get our next camera picture
        camera_img = cam.trig_imag(2)
        replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
        replay_field_image = replay_field_image.astype('int16')

        # Update it on our QT GUI
        camera_view.show()
        camera_view.setImage(replay_field_image)

        if ENABLE_BLUEJAY == True:
            # Update our SLM1 frame for dc balancing
            slm.update_frame_buffer(buffwriteB)
            slm.spi_show_buf_B()
            slm.check_status()
            time.sleep(TIME + 0.5)
        else:
            # Give the hardware system some time
            time.sleep(0.5)

        if iter != 0 and iter % len(value) == 0:
            row_cnt += 1 # we only count down one row after a full set

        # Store the values by slicing
        for p in range(num_panels):
            for q in range(num_panels):
                res[p][q] = np.average\
                    (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])
                res_calib[p][q][idx + len(value)][row_cnt] = res[p][q]

    # Now we deal with the calibration matrix
    # res_calib [row][col][different intensity][no. measurement]
    for p in range(num_panels):
        for q in range(num_panels):
            # create a temporary storage
            temp_ele = np.zeros(len(matched_value) * 2)
            for ele in range(len(matched_value)):
                temp_ele[ele * 2] = np.max(res_calib[p][q][ele])
                temp_ele[ele * 2 + 1] = np.min(res_calib[p][q][ele])
            # Now as a sanity check, sort the array in descending order
            temp_ele[::-1].sort() # Sort in place
            # TODO: the construction of LUTs still need to be optimised !!!

            temp_el = np.zeros(len(matched_value) - 1)
            el_idx = 0
            for el in range(len(temp_ele) - 2):
                if el % 2 == 0: # Only process when el is even
                    temp_el[el_idx] = np.abs(temp_ele[el + 1] - temp_ele[el + 2])/2 \
                                        + temp_ele[el + 2]
                    el_idx += 1
            temp_el.sort() # Sort in place, ascending order
            
            for e in range(len(temp_el)):
                individual_limit[p][q][e + 1] = temp_el[e]

# Debug purpose
cnt = 0

################################################################
# Run random tests
################################################################
NUM_ITERATIONS = 50

for i in range(NUM_ITERATIONS):
    # Generate random matrix
    matrix_BJ_org = np.random.randint(0, 2, (num_panels, num_panels))
    matrix_AU_org = np.random.randint(0, 9, (num_panels, num_panels))
    # matrix_AU_org = np.array([[0,1,2], [3,4,5], [6,7,8]])
    # matrix_AU_org = np.array([[3,3,3], [3,3,3], [3,3,3]])
    # matrix_BJ_org = np.array([[0,0,1], [1,1,0], [1,0,1]])
    # Bluejay needs to be flipped lr and ud
    matrix_BJ = np.flip(matrix_BJ_org)
    # Aurora needs to be rotated 90 then flipped lr
    # matrix_AU = matrix_AU_org
    matrix_AU = np.fliplr(np.rot90(matrix_AU_org))

    if ENABLE_BLUEJAY == True:
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                idx = matrix_BJ[row][col]
                image_np_BJ = draw_macro_pixel(row,col,np.int32(pixel_size_BJ / num_panels),
                                                ptr_left_BJ,ptr_top_BJ,idx,image_np_BJ)
        buffwriteA = image_np_BJ
        buffwriteB = (buffwriteA + 1) % 2
        # Update our SLM1 frame
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()
        slm.check_status()
        time.sleep(TIME)


    for row in range(0,num_panels):
        for col in range(0,num_panels):
            idx = matrix_AU[row][col]
            image_np = draw_macro_pixel(row,col,np.int32(pixel_size / num_panels),
                                            ptr_left,ptr_top,value[idx],image_np)
            gray = np.require(image_np, np.uint8, 'C')
    qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
    qImg.ndarray = gray
    for i in range(256):
        qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
    img_widget.setPixmap(QPixmap(qImg))

    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now
    time.sleep(0.5)

    # Get our next camera picture
    camera_img = cam.trig_imag(2)
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)

    if ENABLE_BLUEJAY == True:
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()
        slm.check_status()
        time.sleep(TIME + 0.5)
    else:
        # Give the hardware system some time
        time.sleep(0.5)

    # Store the values by slicing
    for p in range(num_panels):
        for q in range(num_panels):
            res[p][q] = np.average\
                (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])
            res_temp[p*num_panels+q] = res[p][q]
    res = np.asarray(res)
    res_temp = np.asarray(res_temp)
    res_stored = np.vstack([res_stored, res_temp])

    # Reconstruct based on LUT
    # if not in range, set to np.nan
    for p in range(num_panels):
        for q in range(num_panels):
            if CALIBRATION == True:
                res_hadamard[p][q] = find_individual_range(res[p][q], individual_limit[p][q])
            else:
                res_hadamard[p][q] = find_range(res[p][q])

    # Compare the matrix
    numerical = [1,4,10,40,100,400,1000,4000,10000]
    matrix_BJ_org[matrix_BJ_org == 0] = 4
    matrix_BJ_org[matrix_BJ_org == 1] = 10
    for p in range(num_panels):
        for q in range(num_panels):
            matrix_AU_org[p][q] = numerical[matrix_AU_org[p][q]]
    # Hadamard product
    origin_hadamard = matrix_AU_org * matrix_BJ_org

    if cnt != 0 and cnt != 1:
        for p in range(num_panels):
            for q in range(num_panels):
                if origin_hadamard[p][q] != res_hadamard[p][q]:
                    error[p][q] += 1
        total_iter += 1
    cnt += 1


print("Total error counts:", error)
print("Total iterations:", total_iter, "Total elements:", total_iter*num_panels*num_panels)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("../data/canvasCalib/calibrationLUT"+time_str+".csv", 
           np.ravel(np.asarray(individual_limit)),
           delimiter =", ", 
           fmt ='% s')

np.savetxt("../data/canvasCalib/replayfield_image"+time_str+".csv", 
           np.asarray(replay_field_image),
           delimiter =", ", 
           fmt ='% s')

np.savetxt("../data/canvasCalib/image_np_AU"+time_str+".csv", 
           np.asarray(image_np),
           delimiter =", ", 
           fmt ='% s')

np.savetxt("../data/canvasCalib/image_np_BJ"+time_str+".csv", 
           np.asarray(image_np_BJ),
           delimiter =", ", 
           fmt ='% s')



print("Write Complete")