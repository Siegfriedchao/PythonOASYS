
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

def draw_macro_pixel(row, col, size, ptr_left, ptr_top, value, image_np):
# row, col, size, ptr_left, ptr_top, value, image_np
    image_np[ptr_top + row*size:ptr_top + row*size + size ,\
            ptr_left + col*size:ptr_left + col*size + size] = value
    
    return image_np

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init(trigger=True)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view, 0, 0)
layout.addWidget(camera_view, 0, 1)

# # Adding another widget to display a table
# class TableView(QTableWidget):
#     def __init__(self, data, *args):
#         QTableWidget.__init__(self, *args)
#         self.data = data
#         self.setData()
#         self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
#         self.resizeColumnsToContents()
#         self.resizeRowsToContents()

#     def setData(self): 
#         for n in range(0,3):
#             for m in range(0,3):
#                 newitem = QTableWidgetItem(str(self.data[n][m]))
#                 self.setItem(m, n, newitem)

# data = [[1,2,3],
#         [4,5,6],
#         [7,8,9]]
# table_view = TableView(data, 3, 3)
# layout.addWidget(table_view, 1, 2)

# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()

# Set up Bluejay
ENABLE_BLUEJAY = True
if ENABLE_BLUEJAY == True:
    # Init our SLM
    slm.init()
    # Set our SLM to only update over SPI commands
    slm.enter_spi_update()
    #  ... Top ...
    # Left ... Right
    #     Bottom
    WIDTH_BJ = 10
    ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 230, 1070, 250, 1090 # initial position
    x_dim_BJ = 1280
    y_dim_BJ = 1280
    pix_val_BJ = 1 # Dim
    strip_val_BJ = 1 # Bright
    max_val_BJ = 0
    # Generating test patterns on the fly
    # Creating Guardian
    image_np_BJ = np.full((y_dim_BJ, x_dim_BJ), pix_val_BJ)
    image_np_BJ[: , ptr_left_BJ:ptr_left_BJ + WIDTH_BJ] = strip_val_BJ
    image_np_BJ[: , ptr_right_BJ - WIDTH_BJ: ptr_right_BJ] = strip_val_BJ
    image_np_BJ[ptr_top_BJ:ptr_top_BJ + WIDTH_BJ, :] = strip_val_BJ
    image_np_BJ[ptr_bottom_BJ - WIDTH_BJ: ptr_bottom_BJ, :] = strip_val_BJ
    buffwriteA = image_np_BJ
    buffwriteB = (buffwriteA + 1) % 2

count_trigger = 0

time.sleep(0.5)

#  ... Top ...
# Left ... Right
#     Bottom
WIDTH = 10
ptr_left, ptr_right, ptr_top, ptr_bottom = 670, 1270, 240, 840 # initial position
x_dim = 1920
y_dim = 1080
pix_val = 220 # Dim
strip_val = 50 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

# Generating test patterns on the fly
# Creating Guardian
image_np = np.full((y_dim, x_dim), pix_val)
image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

# Creating macro pixels
PIXELS = 3 # select 1 or 2
if PIXELS == 1:
    # Using the full set
    value = [50, 70, 95,100,120,140,155,180,200]
    for row in range(0,3):
        for col in range(0,3):
            pix_value = value[row*3 + col]
            image_np = draw_macro_pixel(row,col,200,ptr_left,ptr_top,pix_value,image_np)
elif PIXELS == 2:
    # Using only one macro pixel instead of the whole
    value = [50, 70, 95,105,120,140,155,180,200]
    image_np = draw_macro_pixel(1,1,200,ptr_left,ptr_top,value[0],image_np)
elif PIXELS == 3:
    # Using the full set
    value = [55, 85, 100,115,128,141,155,170,188]

gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for i in range(256):
    qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
img_widget.setPixmap(QPixmap(qImg))

# Set up the number of panels we want to slice n by n
num_panels = 3
num_pixels = 1023
# Set the void pixel number
num_void = 64
#   results are stored in a 2d array
res = [[0] * num_panels for _ in range(num_panels)]

# some preprocessing of math
slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

i, j = -num_void, 0
x = (num_pixels - num_panels * 2 * num_void) / num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += x
    j += 1
    slice[j] = i
    j += 1

slice = [int(i) for i in slice]

res_stored = [0 for _ in range(num_panels*num_panels)]
res_temp = [0 for _ in range(num_panels*num_panels)]

NUM_ITERATIONS = 455
TIME = 0.1
for i in range(NUM_ITERATIONS):
    if ENABLE_BLUEJAY == True:
        # Update our SLM1 frame
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()
        slm.check_status()
        time.sleep(TIME)

    if PIXELS == 2:
        idx = i % (num_panels * num_panels)
        image_np = draw_macro_pixel(1,1,200,ptr_left,ptr_top,value[idx],image_np)
        gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for i in range(256):
            qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
        img_widget.setPixmap(QPixmap(qImg))
    elif PIXELS == 3:
        for row in range(0,3):
            for col in range(0,3):
                idx = i % (num_panels * num_panels)
                image_np = draw_macro_pixel(row,col,200,ptr_left,ptr_top,value[idx],image_np)
                gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for i in range(256):
            qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
        img_widget.setPixmap(QPixmap(qImg))

    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    # Get our next camera picture
    camera_img = cam.trig_imag(2)
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)

    if ENABLE_BLUEJAY == True:
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()
        slm.check_status()
        time.sleep(TIME)
    else:
        # Give the hardware system some time
        time.sleep(0.5)

    # Store the values by slicing
    for p in range(num_panels):
        for q in range(num_panels):
            res[p][q] = np.average\
                (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])
            res_temp[p*num_panels+q] = res[p][q]
    res = np.asarray(res)
    res_temp = np.asarray(res_temp)
    res_stored = np.vstack([res_stored, res_temp])

    the_table = plt.table(cellText=res)

    plt.pause(0.05)
    plt.clf()
    plt.cla()

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("../data/canvasCalib/StabilityAurora"+time_str+".csv", 
           np.asarray(res_stored),
           delimiter =", ", 
           fmt ='% s')

print("Write Complete")