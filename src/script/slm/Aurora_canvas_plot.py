
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
# set the trigger delay to 1000us=1ms, this gives less fluctuation
cam.set_trigger_delay(1000)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()


NUM_CITL_ITERATIONS = 256
count_trigger = 0

time.sleep(0.5)

#  ... Top ...
# Left ... Right
#     Bottom
WIDTH = 10
ptr_left, ptr_right, ptr_top, ptr_bottom = 690, 1330, 256, 876 # initial position
x_dim = 1920
y_dim = 1080
pix_val = 220 # Dim
strip_val = 50 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

# Generating test patterns on the fly
# Creating Guardian
image_np = np.full((y_dim, x_dim), pix_val)
image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

# Creating macro pixels
PIXELS = 1
if PIXELS == 1:
    image_np[ptr_top:ptr_top + 200 , ptr_left:ptr_left + 200] = strip_val
    image_np[ptr_top:ptr_top + 200 , ptr_left + 400:ptr_left + 600] = strip_val
    image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
    image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
    image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for i in range(256):
    qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
img_widget.setPixmap(QPixmap(qImg))

while True:
    # Get our next camera picture
    camera_img = cam.trig_imag(2)
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # Store the values
    new_val = np.average(replay_field_image[1000:1024,:])
    res_1.append(new_val)
    ptr_1.append(ptr_left)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

# Display the widget as a new window
window.show()

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

print("Write Complete")