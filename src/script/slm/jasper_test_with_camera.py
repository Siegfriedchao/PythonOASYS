
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init(trigger=True)

#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

# Plot on the SLM screen
# Create window
window2 = QtGui.QMainWindow()
# Setup our Window
parent_widget2 = QtGui.QWidget()
window2.setCentralWidget(parent_widget2)
# Choose which monitor we are going to display on
display_monitor = 1 # the number of the monitor you want to display your widget
monitor2 = QDesktopWidget().screenGeometry(display_monitor)
window2.move(monitor2.left(), monitor2.top())
# window2.resize(640,640)
# window2.show()
window2.showFullScreen()
# window2.showMaximized()
window2.setWindowTitle('pyqtgraph example: GraphicsLayout')
layout2 = QtGui.QGridLayout()
parent_widget2.setLayout(layout2)

slm_plot = pg.PlotItem(title='BufA', invertY=True)
slm_view = pg.ImageView(view=slm_plot)
layout2.addWidget(slm_view,    0,      0)

# Load up a frame of data into our buffers to display
# Just some test code here to load up a simple image
# image_location = "./input/" + "horizontal_grating.bmp"
# image_location = "../input/" + "checkerboard_16_pixel.bmp"
# image_location = "../input/" + "checkerboard_64_pixel_half.bmp"
# image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
image_location = "../input/" + "smiley_face_inv.png"
# image_location = "../input/" + "romeo_b.jpg"
# image_location = "../input/" + "Saturn.JPG"
# image_location = "../input/" + "einstein.png"

img_raw = Image.open(image_location)
image_np = np.asarray(img_raw)

x_dim = 3000
y_dim = 3000
pix_val = 0
image_np = np.full((x_dim, y_dim), pix_val)

# Big loop runs until:
#   - terminated with an 'X' command
#   - reaches prescribed number of iterations
NUM_CITL_ITERATIONS = 256
count_trigger = 0

res = []

time.sleep(0.5)
for i in range(NUM_CITL_ITERATIONS):
    # Get our next camera picture
    # camera_img = cam.grab_imag()
    camera_img = cam.trig_imag(1)
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))

    # Generating test patterns on the fly
    x_dim = 3000
    y_dim = 3000
    pix_val = i
    image_np = np.full((y_dim, x_dim), pix_val)

    # Store the values
    res.append(np.average(replay_field_image))

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    slm_view.show()
    slm_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    # time.sleep(0.5)

# Display the widget as a new window
window.show()
window2.show()

# Now save the measured result to file
plt.plot(res)
plt.savefig("test.png",format='png')
plt.show()
np.savetxt("test.csv", 
           np.asarray(res),
           delimiter =", ", 
           fmt ='% s')
print("Write Complete")

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()