
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=False, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

WIDTH = 10
ptr_left, ptr_right, ptr_top, ptr_bottom = 100, 1150, 0, 1150
x_dim = 1280
y_dim = 1280
pix_val = 0 # Dim
strip_val = 1 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

TIME = 0.1

#left SLM (left in camera)
while condition_flg == False:
    # Generating test patterns on the fly
    image_np = np.full((y_dim, x_dim), pix_val)
    image_np[: , ptr_left:ptr_left + WIDTH] = strip_val

    buffwriteA = image_np
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    time.sleep(TIME)

    # Get our next camera picture
    camera_img = cam.grab_imag()
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # The other buffer is just inverted
    buffwriteB = (buffwriteA + 1) % 2
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    time.sleep(TIME)

    # Store the values
    new_val = np.average(replay_field_image[:, 0:24])
    res_1.append(new_val)
    ptr_1.append(ptr_left)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image, levels=(0,255))
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,1))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    ptr_left += 2
    # fig = plt.plot(ptr_1, res_1)
    # plt.pause(0.05)
    # plt.clf()
    # plt.cla()

    if ptr_left > 230:
        # Terminate the calibration
        condition_flg = True

    time.sleep(0.05)

condition_flg = False
res_2 = []
ptr_2 = []
#right SLM (right in camera)
while condition_flg == False:
    # Generating test patterns on the fly
    image_np = np.full((y_dim, x_dim), pix_val)
    image_np[: , ptr_right - WIDTH: ptr_right] = strip_val

    buffwriteA = image_np
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    time.sleep(TIME)

    # Get our next camera picture
    camera_img = cam.grab_imag()
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # The other buffer is just inverted
    buffwriteB = (buffwriteA + 1) % 2
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    time.sleep(TIME)

    # Store the values
    new_val = np.average(replay_field_image[:,1000:1024])
    res_2.append(new_val)
    ptr_2.append(ptr_right)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image, levels=(0,255))
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,1))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    ptr_right -= 2
    # fig = plt.plot(ptr_2, res_2)
    # plt.pause(0.03)
    # plt.clf()
    # plt.cla()

    if ptr_right < 1050:
        # Terminate the calibration
        condition_flg = True

    time.sleep(0.05)

condition_flg = False
res_3 = []
ptr_3 = []
#top SLM (bottom in camera)
while condition_flg == False:
    # Generating test patterns on the fly
    image_np = np.full((y_dim, x_dim), pix_val)
    image_np[ptr_top:ptr_top + WIDTH, :] = strip_val

    buffwriteA = image_np
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    time.sleep(TIME)

    # Get our next camera picture
    camera_img = cam.grab_imag()
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # The other buffer is just inverted
    buffwriteB = (buffwriteA + 1) % 2
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    time.sleep(TIME)

    # Store the values
    new_val = np.average(replay_field_image[1000:1024,:])
    res_3.append(new_val)
    ptr_3.append(ptr_top)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image, levels=(0,255))
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,1))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    ptr_top += 2
    # fig = plt.plot(ptr_3, res_3)
    # plt.pause(0.03)
    # plt.clf()
    # plt.cla()

    if ptr_top > 220:
        # Terminate the calibration
        condition_flg = True

    time.sleep(0.05)

condition_flg = False
res_4 = []
ptr_4 = []
#bottom SLM (top in camera)
while condition_flg == False:
    # Generating test patterns on the fly
    image_np = np.full((y_dim, x_dim), pix_val)
    image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

    buffwriteA = image_np
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    time.sleep(TIME)

    # Get our next camera picture
    camera_img = cam.grab_imag()
    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # The other buffer is just inverted
    buffwriteB = (buffwriteA + 1) % 2
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    time.sleep(TIME)

    # Store the values
    new_val = np.average(replay_field_image[0:24,:])
    res_4.append(new_val)
    ptr_4.append(ptr_bottom)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image, levels=(0,255))
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,1))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

    ptr_bottom -= 2
    # fig = plt.plot(ptr_4, res_4)
    # plt.pause(0.03)
    # plt.clf()
    # plt.cla()

    if ptr_bottom < 900:
        # Terminate the calibration
        condition_flg = True

    time.sleep(0.05)

# As we dont currently have it turned to sleep mode, keep the board running
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
hologram_img_raw = Image.open(image_location)
target_img_raw = hologram_img_raw.resize((1280, 1280))
target_img_gray = target_img_raw.convert("1")
buffwriteA = np.asarray(target_img_gray)
buffwriteB = (buffwriteA + 1) % 2

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

file_path_name = "E:/GitLab/ExperimentData/20230124/CanvasCalib/"

np.savetxt(file_path_name+"BJLeft"+time_str+".csv",
           np.c_[np.asarray(res_1),np.asarray(ptr_1)],
           delimiter =", ",
           fmt ='% s')
np.savetxt(file_path_name+"BJRight"+time_str+".csv",
           np.c_[np.asarray(res_2),np.asarray(ptr_2)],
           delimiter =", ",
           fmt ='% s')
np.savetxt(file_path_name+"BJTop"+time_str+".csv",
           np.c_[np.asarray(res_3),np.asarray(ptr_3)],
           delimiter =", ",
           fmt ='% s')
np.savetxt(file_path_name+"BJBottom"+time_str+".csv",
           np.c_[np.asarray(res_4),np.asarray(ptr_4)],
           delimiter =", ",
           fmt ='% s')

plt.plot(res_1,label='Left')
plt.plot(res_2,label='Right')
plt.plot(res_3,label='Top')
plt.plot(res_4,label='Bottom')
plt.legend()
plt.savefig(file_path_name+"BJtest"+time_str+".png",format='png')
plt.show()

print("Write Complete")

for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)