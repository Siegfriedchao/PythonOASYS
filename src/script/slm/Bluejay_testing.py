
# Imports
# Libraries
import time
import numpy as np
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init()

#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# # Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 1 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
# window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(1280,1024)
# window.show()
# Different Fullscreen options
# window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# # Init our QTGUI + QTWindow
# app = QtGui.QApplication([])
# # Interpret image data as row-major instead of col-major
# pg.setConfigOptions(imageAxisOrder='row-major')
# # Create window with ImageView widget
# win = QtGui.QMainWindow()
# win.resize(800,800)
# # imv = pg.ImageView()
# # win.setCentralWidget(imv)
# win.show()

# win.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
bufA_plot = pg.PlotItem(title='BufA', invertY=True)
bufA_plot.hideAxis('left')
bufA_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
bufA_view = pg.ImageView(view=bufA_plot) 
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
bufB_plot = pg.PlotItem(title='BufB', invertY=True)
bufB_plot.hideAxis('left')
bufB_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
bufB_view = pg.ImageView(view=bufB_plot)
layout.addWidget(bufA_view,    0,      0)
layout.addWidget(bufB_view,    0,      1)
# window.show()

# Load up a frame of data into our buffers to display
# Just some test code here to load up a simple image
# image_location = "../input/" + "USAF_half.bmp"
# image_location = "../input/" + "checkerboard_16_pixel.bmp"
# image_location = "../input/" + "checkerboard_64_pixel_half.bmp"
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
# image_location = "../input/" + "smiley_face_inv.png"
# image_location = "../input/" + "smiley_face_inv_target.png"
# image_location = "../input/" + "checkerboard_512.bmp"
# image_location = "../input/" + "dot.png"
# image_location = "../input/" + "romeo_b.jpg"
# image_location = "../input/" + "Saturn.JPG"
# image_location = "../input/" + "einstein.png"
# image_location = "E:/GitLab/ExperimentData/20230108/Code/UsedInExperiment/FLC/FLC0-255width40.bmp"

hologram_img_raw = Image.open(image_location)
# Resize for our SLM
target_img_raw = hologram_img_raw.resize((1280, 1280))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
target_img_gray = target_img_raw.convert("1")
# target_img_gray = target_img_raw
# Convert our Image into an array
buffwriteA = np.asarray(target_img_gray)
# The other buffer is just inverted
buffwriteB = (buffwriteA + 1) % 2

# # Swap for endianess test
# raw_array = np.asarray(target_img_raw)
# swapped = raw_array.byteswap()
# Image.fromarray(raw_array).show()
# Image.fromarray(swapped).show()

# Big loop runs until:
#   - terminated with an 'X' command
#   - reaches prescribed number of iterations
NUM_CITL_ITERATIONS = 1000000
TIME = 0.5


slm.update_frame_buffer(buffwriteB)

for i in range(NUM_CITL_ITERATIONS):

    # # # Update Frame
    slm.update_frame_buffer(buffwriteA)

    # Buffer Swap
    slm.spi_show_buf_A()

    print("State A")
    # Print out our SLM status
    # slm.check_status()

    # Update our SLM frame
    
    time.sleep(TIME)
    # Buffer Swap

    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    print("State B")
    # Print out our SLM status
    # slm.check_status()

    time.sleep(TIME)
    # time.sleep(0.1)