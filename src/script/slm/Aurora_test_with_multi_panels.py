
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import time

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init(trigger=True, width=1024, height=1024, offsetX=0, offsetY=190, exposure=8000)
# cam.init(False, 1024, 1024, 0, 190, 8000)

# offset_location = "../input/" + "BladeOffAuroraDisplayingBlack2.bmp"
# offset_raw = Image.open(offset_location)
# offset = np.asarray(offset_raw, dtype=np.int16) - 40
# offset[offset < 0] = 0


#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

# Plot on the SLM screen
# Create window
window2 = QtGui.QMainWindow()
# Setup our Window
parent_widget2 = QtGui.QWidget()
window2.setCentralWidget(parent_widget2)
# Choose which monitor we are going to display on
display_monitor = 1 # the number of the monitor you want to display your widget
monitor2 = QDesktopWidget().screenGeometry(display_monitor)
window2.move(monitor2.left(), monitor2.top())
# window2.resize(640,640)
# window2.show()
window2.showFullScreen()
# window2.showMaximized()
window2.setWindowTitle('pyqtgraph example: GraphicsLayout')
layout2 = QtGui.QGridLayout()
parent_widget2.setLayout(layout2)

slm_plot = pg.PlotItem(title='BufA', invertY=True)
slm_view = pg.ImageView(view=slm_plot)
layout2.addWidget(slm_view,    0,      0)


# Big loop runs until:
#   - terminated with an 'X' command
#   - reaches prescribed number of iterations
NUM_ITERATIONS = 256
count_trigger = 0

# Set up the number of panels we want to slice n by n
num_panels = 4
num_pixels = 1024
# Set the void pixel number
num_void = 64
#   results are stored in a 2d array
res = [[0]*NUM_ITERATIONS for _ in range(num_panels*num_panels)]

# some preprocessing of math
slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

i, j = -num_void, 0
x = (num_pixels - num_panels * 2 * num_void) / num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += x
    j += 1
    slice[j] = i
    j += 1

slice = [int(i) for i in slice]

# Give the hardware system some time
time.sleep(0.5)

for i in range(NUM_ITERATIONS):
    # Generating test patterns on the fly
    x_dim = 2000
    y_dim = 2000
    pix_val = i
    image_np = np.full((y_dim, x_dim), pix_val)
    slm_view.show()
    slm_view.setImage(image_np, levels=(0,255))

    # Get our next camera picture
    # camera_img = cam.grab_imag()

    # For trigger mode use this:
    camera_img = cam.trig_imag(2)

    replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    replay_field_image = replay_field_image.astype('int16')

    # cam_img = Image.fromarray(camera_img, 'L')
    # cam_img.save("../data/rawdata/test" + str(i) + ".bmp")

    # replay_field_image -= offset
    # replay_field_image[replay_field_image < 0] = 0

    # Store the values by slicing
    for p in range(num_panels):
        for q in range(num_panels):
            res[p * num_panels + q][i] = np.average\
                (replay_field_image[slice[2*p]:slice[2*p+1],slice[2*q]:slice[2*q+1]])

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(replay_field_image)
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now

# Display the widget as a new window
window.show()
window2.show()

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

# np.savetxt("../data/fourpanelpure/test"+time_str+".csv", 
# # np.savetxt("../data/rawdata/test"+time_str+".csv", 
#            np.c_[np.asarray(res_1),np.asarray(res_2),np.asarray(res_3),np.asarray(res_4)],
#            delimiter =", ",
#            fmt ='% s')

for j in range(num_panels):
    for k in range(num_panels):
        plt.plot(res[j * num_panels + k],label=str(j)+','+str(k))
# plt.legend()
plt.savefig("../data/NewSetup/MultiPanels/test"+time_str+".png",format='png')
# plt.savefig("../data/rawdata/test"+time_str+".png",format='png')
plt.show()


# result_camera.save("../data/fourpanel/offset_test.bmp")
# result_camera.save("../data/rawdata/offset_test.bmp")

print("Write Complete")

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()