
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
cam.set_trigger_delay(1000)
#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

# Plot on the SLM screen
# Create window
window2 = QtGui.QMainWindow()
# Setup our Window
parent_widget2 = QtGui.QWidget()
window2.setCentralWidget(parent_widget2)
# Choose which monitor we are going to display on
display_monitor = 1 # the number of the monitor you want to display your widget
monitor2 = QDesktopWidget().screenGeometry(display_monitor)
window2.move(monitor2.left(), monitor2.top())
# window2.resize(640,640)
# window2.show()
window2.showFullScreen()
# window2.showMaximized()
window2.setWindowTitle('pyqtgraph example: GraphicsLayout')
layout2 = QtGui.QGridLayout()
parent_widget2.setLayout(layout2)

slm_plot = pg.PlotItem(title='BufA', invertY=True)
slm_view = pg.ImageView(view=slm_plot)
layout2.addWidget(slm_view,    0,      0)

# Load up a frame of data into our buffers to display
# Just some test code here to load up a simple image
# image_location = "../input/" + "USAF_half.bmp"
# image_location = "../input/" + "checkerboard_16_pixel.bmp"
# image_location = "../input/" + "checkerboard_64_pixel_half.bmp"
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
# image_location = "../input/" + "smiley_face_inv.png"
# image_location = "../input/" + "smiley_face_inv_target.png"
# image_location = "../input/" + "dot.png"
# image_location = "../input/" + "romeo_b.jpg"
# image_location = "../input/" + "Saturn.JPG"
# image_location = "../input/" + "einstein.png"

hologram_img_raw = Image.open(image_location)
# Resize for our SLM
target_img_raw = hologram_img_raw.resize((1280, 1280))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
target_img_gray = target_img_raw.convert("1")
# target_img_gray = target_img_raw
# Convert our Image into an array
buffwriteA = np.asarray(target_img_gray)
# The other buffer is just inverted
buffwriteB = (buffwriteA + 1) % 2

x_dim = 2000
y_dim = 2000
pix_val = 0
image_np = np.full((x_dim, y_dim), pix_val)

# Big loop runs until:
#   - terminated with an 'X' command
#   - reaches prescribed number of iterations
NUM_CITL_ITERATIONS = 256

TIME = 0
PAUSE = 0.02

count_trigger = 0
res = []
res2 = []

for i in range(NUM_CITL_ITERATIONS):
# for i in range(25,225,25):
    # Update our SLM2 frame
    x_dim = 2000
    y_dim = 2000
    pix_val = i
    image_np = np.full((y_dim, x_dim), pix_val)

    slm_view.show()
    slm_view.setImage(image_np, levels=(0,255))

    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()

    # Give the SLM sometime to update
    time.sleep(PAUSE)

    # Now grab the image
    camera_img = cam.trig_imag(2)
    result_camera = np.copy(Image.fromarray(camera_img, 'L'))

    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(result_camera)
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))
    QtGui.QApplication.processEvents()
    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()

    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Now grab the image
    camera_img2 = cam.trig_imag(2)
    result_camera2 = np.copy(Image.fromarray(camera_img2, 'L'))

    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(result_camera2)

    # Store the values
    res.append(np.average(result_camera))
    res2.append(np.average(result_camera2))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now


# Display the widget as a new window
window.show()
window2.show()

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

# np.savetxt("../data/together"+time_str+".csv", 
#            np.asarray(res),
#            delimiter =", ", 
#            fmt ='% s')

np.savetxt("E:/GitLab/ExperimentData/20230118/Data-90-90-90-90-"+time_str+".csv", 
           np.c_[np.asarray(res), np.asarray(res2)],
           delimiter =", ", 
           fmt ='% s')

plt.plot(res)
plt.plot(res2)
plt.savefig("E:/GitLab/ExperimentData/20230118/Data-90-90-90-90-"+time_str+".png",format='png')

print("Write Complete")

# As we dont currently have it turned to sleep mode, keep the board running
for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)
    time.sleep(PAUSE)
