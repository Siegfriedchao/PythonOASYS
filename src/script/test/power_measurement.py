
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
# from PyQt5.QtWidgets import QDesktopWidget
# from pyqtgraph.Qt import QtCore, QtGui
# import pyqtgraph as pg

import csv

# My own code
# import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm

import winsound
frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second
winsound.Beep(frequency, duration)

from multiprocessing.pool import ThreadPool

# # Init our SLM
# slm.init()
# # Set our SLM to only update over SPI commands
# slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=False, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

res = [] # camera
res2 = [] # power meter

start = time.time()
for i in range(6):
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(pwm.grab_power)

    # do some other stuff in the main process
    # camera_img_A = cam.grab_imag()
    # camera_img_B = cam.grab_imag() # 1.5ms each
    camera_img = cam.grab_imag() # 1.5ms each
    power_data = async_result.get()  # get the return value from your function.
    pool.close()
    # result_camera_A = np.copy(Image.fromarray(camera_img_A, 'L'))
    # result_camera_B = np.copy(Image.fromarray(camera_img_B, 'L'))
    # result_camera = (result_camera_A + result_camera_B) / 2

    result_camera = np.copy(Image.fromarray(camera_img, 'L'))
    # Store the values
    res.append(np.average(result_camera))
    res2.append(power_data)

    time.sleep(0.2) # wait for 0.2second
end = time.time()
print("Elapsed time (second):", end - start)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

# np.savetxt("E:/GitLab/ExperimentData/20230106/Data"+time_str+".csv", 
#            np.c_[np.asarray(res), np.asarray(res2)],
#            delimiter =", ", 
#            fmt ='% s')

# for i in range(10):
#     pwm.return_sound()

print("Write Complete")