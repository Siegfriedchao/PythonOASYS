
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import camera.driver as cam
import p100usb.driver as pwm

from multiprocessing.pool import ThreadPool

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1 # why did my screen change today? 2023/01/06
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()

x_dim = 1920
y_dim = 1080
pix_val = 0
# image_location = "../input/" + "USAF_half.bmp"
# hologram_img_raw = Image.open(image_location)
image_np = np.full((y_dim, x_dim), pix_val)

res = [] # camera
res2 = [] # power meter

TIME = 0.2

NUM_ITERATIONS = 5
count_trigger = 0

# Initilise the display
gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for aa in range(256):
    qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
img_widget.setPixmap(QPixmap(qImg))
QtGui.QApplication.processEvents()
time.sleep(.5) # wait to refresh, wait longer

start = time.time()
for i in range(0,3000,200):
    pix_val = 127

    cam.set_trigger_delay(i)

    print('displayed value:', i)
    for j in range(20):
        image_np = np.full((y_dim, x_dim), pix_val)
        gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for aa in range(256):
            qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
        img_widget.setPixmap(QPixmap(qImg))

        QtGui.QApplication.processEvents()    # you MUST process the plot now
        time.sleep(TIME) # wait to refresh

        # Obtain measurements
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img = cam.trig_imag(1)
        power_data = async_result.get()  # get the return value from your function.
        pool.close()
        result_camera = np.copy(Image.fromarray(camera_img, 'L'))

        # Store the values
        res.append(np.average(result_camera))
        res2.append(power_data)

        # time.sleep(TIME) # slow down the process

end = time.time()
print("Elapsed time (second):", end - start)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("E:/GitLab/ExperimentData/20230118/Data"+time_str+".csv", 
           np.c_[np.asarray(res), np.asarray(res2)],
           delimiter =", ", 
           fmt ='% s')

print("Write Complete")
