
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam

# Global flag
saveImg = 0

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
cam.init(trigger=True)

# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='BufA', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='BufB', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view,    0,      0)
layout.addWidget(camera_view,    0,      1)

# Plot on the SLM screen
# Create window
window2 = QtGui.QMainWindow()
# Setup our Window
parent_widget2 = QtGui.QWidget()
window2.setCentralWidget(parent_widget2)
# Choose which monitor we are going to display on
display_monitor = 1 # the number of the monitor you want to display your widget
monitor2 = QDesktopWidget().screenGeometry(display_monitor)
window2.move(monitor2.left(), monitor2.top())
# window2.resize(640,640)
# window2.show()
window2.showFullScreen()
# window2.showMaximized()
window2.setWindowTitle('pyqtgraph example: GraphicsLayout')
layout2 = QtGui.QGridLayout()
parent_widget2.setLayout(layout2)

#TODO: this needs to be updated as there are still white lines around
slm_plot = pg.PlotItem(invertY=True)
slm_view = pg.ImageView(view=slm_plot)
slm_view.ui.histogram.hide()
slm_view.ui.roiBtn.hide()
slm_view.ui.menuBtn.hide()
slm_plot.hideAxis(axis="left")
slm_plot.hideAxis(axis="bottom")
layout2.addWidget(slm_view,    0,      0)


# Calibration
offset_location = "../input/" + "offset.bmp"
offset_raw = Image.open(offset_location)
offset = np.asarray(offset_raw) - 20
offset[offset < 0] = 0

# Now the main operations

# Load up a frame of data into our buffers to display
# Just some test code here to load up a simple image
# image_location = "../input/" + "USAF_half.bmp"
# image_location = "../input/" + "checkerboard_16_pixel.bmp"
# image_location = "../input/" + "checkerboard_64_pixel_half.bmp"
image_location = "../input/" + "checkerboard_512.bmp"
# image_location = "../input/" + "smiley_face_inv.png"
# image_location = "../input/" + "smiley_face_inv_target.png"
# image_location = "../input/" + "dot.png"
# image_location = "../input/" + "romeo_b.jpg"
# image_location = "../input/" + "Saturn.JPG"
# image_location = "../input/" + "einstein.png"

hologram_img_raw = Image.open(image_location)
# Resize for our SLM
target_img_raw = hologram_img_raw.resize((1280, 1280))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
target_img_gray = target_img_raw.convert("1")
# target_img_gray = target_img_raw
# Convert our Image into an array
buffwriteA = np.asarray(target_img_gray)
# The other buffer is just inverted
buffwriteB = (buffwriteA + 1) % 2

x_dim = 2000
y_dim = 2000
pix_val = 0
image_np = np.full((x_dim, y_dim), pix_val)

# Big loop runs until:
#   - terminated with an 'X' command
#   - reaches prescribed number of iterations
NUM_CITL_ITERATIONS = 256

TIME = 0
PAUSE = 0.02

count_trigger = 0
res_1 = []
res_2 = []
res_3 = []
res_4 = []
res2_1 = []
res2_2 = []
res2_3 = []
res2_4 = [] # need a better way

a = 128
b = 384
c = 640
d = 896

for i in range(NUM_CITL_ITERATIONS):
# for i in range(25,250,25):
    # Update our SLM2 frame
    x_dim = 512
    y_dim = 512
    pix_val = i

    # now we create 4 different regions
    image_np_1 = np.full((y_dim, x_dim), pix_val)
    image_np_2 = np.full((y_dim, x_dim), 255 - pix_val)

    image_np_row_1 = np.concatenate((image_np_1,image_np_2),axis=1)
    image_np_row_2 = np.concatenate((image_np_2,image_np_1),axis=1)

    image_np = np.concatenate((image_np_row_1,image_np_row_2),axis=0)

    slm_view.show()
    slm_view.setImage(image_np, levels=(0,255))

    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()

    # Give the SLM sometime to update
    time.sleep(PAUSE)

    # Now grab the image
    camera_img = cam.trig_imag(2)
    result_camera = np.copy(Image.fromarray(camera_img, 'L'))

    if saveImg == 1:
        cam_img = Image.fromarray(camera_img, 'L')
        cam_img.save("../data/rawdataFull/test1_" + str(i) + ".bmp")

    # Sleep for the rest of the period
    time.sleep(TIME)

    # result_camera -= offset
    # result_camera[result_camera < 0] = 0

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(result_camera)
    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))
    QtGui.QApplication.processEvents()
    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()

    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Now grab the image
    camera_img2 = cam.trig_imag(2)
    result_camera2 = np.copy(Image.fromarray(camera_img2, 'L'))

    if saveImg == 1:
        cam_img2 = Image.fromarray(camera_img2, 'L')
        cam_img2.save("../data/rawdataFull/test2_" + str(i) + ".bmp")

    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(result_camera2)

    # Store the values

    # Apply calibration values
    # result_camera2 -= offset
    # result_camera2[result_camera2 < 0] = 0

    res_1.append(np.average(result_camera[a:b,a:b]))
    res_2.append(np.average(result_camera[c:d,a:b]))
    res_3.append(np.average(result_camera[a:b,c:d]))
    res_4.append(np.average(result_camera[c:d,c:d]))
    res2_1.append(np.average(result_camera2[a:b,a:b]))
    res2_2.append(np.average(result_camera2[c:d,a:b]))
    res2_3.append(np.average(result_camera2[a:b,c:d]))
    res2_4.append(np.average(result_camera2[c:d,c:d]))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now


# Display the widget as a new window
window.show()
window2.show()

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

# np.savetxt("../data/together"+time_str+".csv", 
#            np.asarray(res),
#            delimiter =", ", 
#            fmt ='% s')

np.savetxt("../data/rawdataFull/test_"+time_str+".csv",
           np.c_[np.asarray(res_1),np.asarray(res_2),np.asarray(res_3),np.asarray(res_4),np.asarray(res2_1),np.asarray(res2_2),np.asarray(res2_3),np.asarray(res2_4)],
           delimiter =", ",
           fmt ='% s')

plt.plot(res_1)
plt.plot(res_2)
plt.plot(res_3)
plt.plot(res_4)
plt.plot(res2_1)
plt.plot(res2_2)
plt.plot(res2_3)
plt.plot(res2_4)
plt.savefig("../data/rawdataFull/together"+time_str+".png",format='png')
# plt.show()

# offset_img = Image.fromarray(offset, mode='L')
# offset_img.save("../data/fourpanel/offset.bmp")

print("Write Complete")

# As we dont currently have it turned to sleep mode, keep the board running
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
hologram_img_raw = Image.open(image_location)
target_img_raw = hologram_img_raw.resize((1280, 1280))
target_img_gray = target_img_raw.convert("1")
buffwriteA = np.asarray(target_img_gray)
buffwriteB = (buffwriteA + 1) % 2

for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)
    time.sleep(PAUSE)
