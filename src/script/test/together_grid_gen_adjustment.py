### For online calibration to fine-tune the alignment
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

import csv
import bisect

# My own code
import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm

from multiprocessing.pool import ThreadPool

def draw_macro_pixel(row, col, size, ptr_left, ptr_top, value, image_np):
# row, col, size, ptr_left, ptr_top, value, image_np
    image_np[ptr_top + row*size:ptr_top + row*size + size ,\
            ptr_left + col*size:ptr_left + col*size + size] = value
    
    return image_np

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
# set the trigger delay to 1000us=1ms, this gives less fluctuation
cam.set_trigger_delay(1000)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)
# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 2 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(640,640)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('SES demo')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)

# window.setWindowTitle('pyqtgraph example: ImageView')
# Hologram Display Box Defintion
# For showing BufA, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
loaded_plot = pg.PlotItem(title='Display', invertY=True)
loaded_plot.hideAxis('left')
loaded_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
loaded_view = pg.ImageView(view=loaded_plot)
# Replay Field Display Box Definition
# For showing BufB, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
camera_plot = pg.PlotItem(title='Image', invertY=True)
camera_plot.hideAxis('left')
camera_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
camera_view = pg.ImageView(view=camera_plot)
layout.addWidget(loaded_view, 0, 0)
layout.addWidget(camera_view, 0, 1)

# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()


NUM_TILES = 4

# Set up Bluejay
ENABLE_BLUEJAY = True
if ENABLE_BLUEJAY == True:
    # Init our SLM
    slm.init()
    # Set our SLM to only update over SPI commands
    slm.enter_spi_update()
    #  ... Top ...
    # Left ... Right
    #     Bottom
    WIDTH_BJ = 6
    # ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 193, 1063, 92, 972 # initial position, 880*880
    ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 175, 1055, 60, 940
    pixel_size_BJ = ptr_right_BJ - ptr_left_BJ
    x_dim_BJ = 1280
    y_dim_BJ = 1280
    pix_val_BJ = 0 # Dim
    strip_val_BJ = 1 # Bright
    max_val_BJ = 0
    # Generating test patterns on the fly
    # Creating Guardian
    image_np_BJ = np.full((y_dim_BJ, x_dim_BJ), pix_val_BJ)
    image_np_BJ[: , ptr_left_BJ:ptr_left_BJ + WIDTH_BJ] = strip_val_BJ
    image_np_BJ[: , ptr_right_BJ - WIDTH_BJ: ptr_right_BJ] = strip_val_BJ
    image_np_BJ[ptr_top_BJ:ptr_top_BJ + WIDTH_BJ, :] = strip_val_BJ
    image_np_BJ[ptr_bottom_BJ - WIDTH_BJ: ptr_bottom_BJ, :] = strip_val_BJ

    # Creating grid lines
    for tCnt in range(NUM_TILES):
        image_np_BJ[:, ptr_left_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)-WIDTH_BJ/2):
                ptr_left_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)+WIDTH_BJ/2)] = strip_val_BJ
        image_np_BJ[ptr_top_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)-WIDTH_BJ/2):
                ptr_top_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)+WIDTH_BJ/2), :] = strip_val_BJ


    # buffwriteA = image_np_BJ
    # buffwriteB = (buffwriteA + 1) % 2

count_trigger = 0

time.sleep(0.5)

#  ... Top ...
# Left ... Right
#     Bottom
WIDTH = 6
ptr_left, ptr_right, ptr_top, ptr_bottom = 690, 1330, 256, 876 # initial position, 640x620
pixel_size = ptr_right - ptr_left
x_dim = 1920
y_dim = 1080
pix_val = 220 # Dim
strip_val = 50 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

# Generating test patterns on the fly
# Creating Guardian
image_np = np.full((y_dim, x_dim), pix_val)
image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

for tCnt in range(NUM_TILES):
    image_np[:, ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)-WIDTH/2):ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)+WIDTH/2)] = strip_val
    image_np[ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)-WIDTH/2):ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)+WIDTH/2), :] = strip_val

# # Creating macro pixels
# PIXELS = 3 # select 1 or 2 or 3
# if PIXELS == 3:
#     # Using the full set
#     value = [55, 85, 100,115,128,141,155,170,188]

gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for i in range(256):
    qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
img_widget.setPixmap(QPixmap(qImg))

# Set up the number of panels we want to slice n by n
num_panels = NUM_TILES
num_pixels = 1024
# Set the void pixel number
num_void = 64
#   results are stored in a 2d array
res = [[0] * num_panels for _ in range(num_panels)]

res_stored = [0 for _ in range(num_panels*num_panels)]
res_temp = [0 for _ in range(num_panels*num_panels)]
res_hadamard = [[0] * num_panels for _ in range(num_panels)]

# some preprocessing of math
slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

i, j = -num_void, 0
x = (num_pixels - num_panels * 2 * num_void) / num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += x
    j += 1
    slice[j] = i
    j += 1

slice = [int(i) for i in slice]

error = [[0] * num_panels for _ in range(num_panels)]
total_iter = 0

# Debug purpose
cnt = 0
res = [] # power meter
res2 = [] # power meter2

NUM_ITERATIONS = 100
saveImg = 0
TIME = 0.1
while True:
    if ENABLE_BLUEJAY == True:
        buffwriteA = image_np_BJ
        buffwriteB = (buffwriteA + 1) % 2
        # Update our SLM1 frame
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()
        slm.check_status()
        time.sleep(TIME)

    start = time.time()
    # Generating test patterns on the fly
    # array = [80, 90, 104, 115, 127]
    # array = [80, 90, 98, 106, 116]
    # array = [18,30,38,48,180]
    array = [50,58,64,70,154]#[25,29,32,35,77]
    # pix_val = 64
    pix_val = array[4]
    # Creating Guardian
    image_np = np.full((y_dim, x_dim), pix_val)
    image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
    image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
    image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
    image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

    for tCnt in range(NUM_TILES):
        image_np[:, ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)-WIDTH/2):ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)+WIDTH/2)] = strip_val
        image_np[ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)-WIDTH/2):ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)+WIDTH/2), :] = strip_val

    gray = np.require(image_np, np.uint8, 'C')
    qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
    qImg.ndarray = gray
    for i in range(256):
        qImg.setColor(i, QtGui.QColor(i, i, i).rgb())
    img_widget.setPixmap(QPixmap(qImg))

    loaded_view.show()
    loaded_view.setImage(image_np, levels=(0,255))

    # Housekeeping at the end of the loop
    QtGui.QApplication.processEvents()    # you MUST process the plot now
    end = time.time()
    time.sleep(0.5-(end-start)) #make sure it is 0.5second in total

    # Obtain measurements
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(pwm.grab_power)
    # do some other stuff in the main process
    camera_img = cam.trig_imag(1)
    power_data = async_result.get()  # get the return value from your function.
    pool.close()
    result_camera = np.copy(Image.fromarray(camera_img, 'L'))

    # Now save the measured result to file
    time_str = time.strftime("%Y%m%d-%H%M%S")
    if saveImg == 1:
        cam_img = Image.fromarray(camera_img, 'L')
        cam_img.save("E:/RawExperimentData/CanvasGrid/S0_" + "Grey" + str(pix_val) + "_" + time_str + ".bmp")
    res.append(power_data)

    # Update it on our QT GUI
    camera_view.show()
    camera_view.setImage(camera_img)

    if ENABLE_BLUEJAY == True:
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()
        slm.check_status()
        time.sleep(TIME + 0.5)

        # Obtain measurements
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img2 = cam.trig_imag(1)
        power_data2 = async_result.get()  # get the return value from your function.
        pool.close()
        result_camera = np.copy(Image.fromarray(camera_img2, 'L'))

        # Update it on our QT GUI
        camera_view.show()
        camera_view.setImage(camera_img2)

        if saveImg == 1:
            cam_img = Image.fromarray(camera_img2, 'L')
            cam_img.save("E:/RawExperimentData/CanvasGrid/S1_" + "Grey" + str(pix_val) + "_" + time_str + ".bmp")
        res2.append(power_data2)

        QtGui.QApplication.processEvents()    # you MUST process the plot now
    else:
        # Give the hardware system some time
        time.sleep(0.5)
        QtGui.QApplication.processEvents()    # you MUST process the plot now

############# Below never reaches
### just in case

# As we dont currently have it turned to sleep mode, keep the board running
for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)