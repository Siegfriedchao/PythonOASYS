
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
# from PyQt5.QtWidgets import QDesktopWidget
# from pyqtgraph.Qt import QtCore, QtGui
# import pyqtgraph as pg

import csv

# My own code
import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm

from multiprocessing.pool import ThreadPool

# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=False, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

x_dim = 1280
y_dim = 1280
pix_val = 1 # set to 'on'
# image_location = "../input/" + "USAF_half.bmp"
# hologram_img_raw = Image.open(image_location)
image_np = np.full((y_dim, x_dim), pix_val)
buffwriteA = image_np
buffwriteB = (buffwriteA + 1) % 2

res = [] # camera
res2 = [] # power meter
res3 = [] # camera
res4 = [] # power meter

TIME = 0.1

start = time.time()

# initialise the FLC without recording
slm.update_frame_buffer(buffwriteA)
slm.spi_show_buf_A()
time.sleep(TIME)
slm.update_frame_buffer(buffwriteB)
slm.spi_show_buf_B()
time.sleep(TIME)

for i in range(300):

    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    time.sleep(TIME)

    # Obtain measurements
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(pwm.grab_power)
    # do some other stuff in the main process
    camera_img = cam.grab_imag() # 28-30ms each
    power_data = async_result.get()  # get the return value from your function.
    pool.close()
    result_camera = np.copy(Image.fromarray(camera_img, 'L'))

    # The other buffer is just inverted
    # buffwriteB = (buffwriteA + 1) % 2
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    time.sleep(TIME)

    # Obtain second set of measurements
    pool = ThreadPool(processes=1)
    async_result = pool.apply_async(pwm.grab_power)
    # do some other stuff in the main process
    camera_img2 = cam.grab_imag() # 28-30ms each
    power_data2 = async_result.get()  # get the return value from your function.
    pool.close()
    result_camera2 = np.copy(Image.fromarray(camera_img2, 'L'))

    # Store the values
    res.append(np.average(result_camera))
    res2.append(power_data)
    res3.append(np.average(result_camera2))
    res4.append(power_data2)

end = time.time()
print("Elapsed time (second):", end - start)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("E:/GitLab/ExperimentData/20230106/Data"+time_str+".csv", 
           np.c_[np.asarray(res), np.asarray(res2), np.asarray(res3), np.asarray(res4)],
           delimiter =", ", 
           fmt ='% s')

print("Write Complete")

# As we dont currently have it turned to sleep mode, keep the board running
for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    time.sleep(0.1)
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)
    time.sleep(0.1)
