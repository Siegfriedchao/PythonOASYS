
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from os.path import dirname, join as pjoin
import scipy

# My own code
import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm
import util.load_mat as lmat

import winsound
frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second

from multiprocessing.pool import ThreadPool

# Global flag
saveImg = 1

# Global variable
ptr_left, ptr_right, ptr_top, ptr_bottom = 690, 1330, 256, 876 # initial position, 640x620
pixel_size_lr = ptr_right - ptr_left
pixel_size_ud = ptr_bottom - ptr_top



# Init our SLM
slm.init()
# Set our SLM to only update over SPI commands
slm.enter_spi_update()

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
# set the trigger delay to 1000us=1ms, this gives less fluctuation
cam.set_trigger_delay(1000)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1 # why did my screen change today? 2023/01/06
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()

x_dim = 1920
y_dim = 1080
pix_val = 0
# image_location = "../input/" + "USAF_half.bmp"
# hologram_img_raw = Image.open(image_location)
image_np = np.full((y_dim, x_dim), pix_val)

# Initialise the display
gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for aa in range(256):
    qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
img_widget.setPixmap(QPixmap(qImg))
QtGui.QApplication.processEvents()


# Initialise FLC
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
hologram_img_raw = Image.open(image_location)
# Resize for our SLM
target_img_raw = hologram_img_raw.resize((1280, 1280))
# Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
target_img_gray = target_img_raw.convert("1")
# target_img_gray = target_img_raw
# Convert our Image into an array
buffwriteA = np.asarray(target_img_gray)
# The other buffer is just inverted
buffwriteB = (buffwriteA + 1) % 2

QtGui.QApplication.processEvents()
# Obtain dummy measurements to avoid invalid first data point
pool = ThreadPool(processes=1)
async_result = pool.apply_async(pwm.grab_power)
# do some other stuff in the main process
camera_img = cam.trig_imag(1)
power_data = async_result.get()  # get the return value from your function.
pool.close()
result_camera = np.copy(Image.fromarray(camera_img, 'L'))

time.sleep(1) # wait to refresh, wait longer
################################################
################################################
NUM_ITERATIONS = 256

TIME = 0.3
PAUSE = 0.02

count_trigger = 0
res_1 = []
res_2 = []
res_3 = []
res_4 = []
res2_1 = []
res2_2 = []
res2_3 = []
res2_4 = [] # need a better way
res = [] # power meter
res2 = [] # power meter2

a = 128
b = 384
c = 640
d = 896

x_dim = 1920
y_dim = 1080

APPLYOFFSET = 1

for i in range(NUM_ITERATIONS):
    pix_val = i
    print('displayed value:', i)
    for ii in range(0,1):
# for i in range(25,250,25):
        # Update our SLM2 frame
        # x_dim = 512
        # y_dim = 512

        image_np = np.full((y_dim, x_dim), pix_val)

        # Apply global offset map
        if APPLYOFFSET == 1:
            data_dir = 'E:/GitLab/python_scripts/CGH/src/matlab/'
            mat_fname = pjoin(data_dir, 'run19Matrix2.mat')
            image_np_corrected = lmat.apply_offset(mat_fname, ptr_top, ptr_left, pixel_size_ud, pixel_size_lr, image_np)
            gray = np.require(image_np_corrected, np.uint8, 'C')
        else:
            gray = np.require(image_np, np.uint8, 'C')

        # gray = np.require(image_np, np.uint8, 'C')
        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for aa in range(256):
            qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
        img_widget.setPixmap(QPixmap(qImg))

        QtGui.QApplication.processEvents()    # you MUST process the plot now
        time.sleep(PAUSE) # wait to refresh
        # Update our SLM1 frame
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()

        time.sleep(TIME) # wait to refresh

        # Obtain measurements
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img = cam.trig_imag(1)
        power_data = async_result.get()  # get the return value from your function.
        pool.close()
        result_camera = np.copy(Image.fromarray(camera_img, 'L'))

        if saveImg == 1:
            file_path_name = "E:/RawExperimentData/MultiPanel/Run25/"
            cam_img = Image.fromarray(camera_img, 'L')
            cam_img.save(file_path_name + "1/test1_" +str(ii) + "Grey" + str(i) + ".bmp")

        ################################################
        ################################################
        time.sleep(PAUSE) # wait to refresh
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()

        time.sleep(TIME) # wait to refresh

        # Obtain measurements
        pool2 = ThreadPool(processes=1)
        async_result2 = pool2.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img2 = cam.trig_imag(1)
        power_data2 = async_result2.get()  # get the return value from your function.
        pool2.close()
        result_camera2 = np.copy(Image.fromarray(camera_img2, 'L'))

        if saveImg == 1:
            cam_img2 = Image.fromarray(camera_img2, 'L')
            cam_img2.save(file_path_name + "2/test2_" +str(ii) + "Grey" + str(i) + ".bmp")

        # Store the values
        res_1.append(np.average(result_camera[a:b,a:b]))
        res_2.append(np.average(result_camera[c:d,a:b]))
        res_3.append(np.average(result_camera[a:b,c:d]))
        res_4.append(np.average(result_camera[c:d,c:d]))
        res.append(power_data)

        res2_1.append(np.average(result_camera2[a:b,a:b]))
        res2_2.append(np.average(result_camera2[c:d,a:b]))
        res2_3.append(np.average(result_camera2[a:b,c:d]))
        res2_4.append(np.average(result_camera2[c:d,c:d]))
        res2.append(power_data2)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

file_path_name = "E:/GitLab/ExperimentData/20230226/MultiPanel-265-201-292-"
np.savetxt(file_path_name + time_str + ".csv",
           np.c_[np.asarray(res_1),np.asarray(res_2),
           np.asarray(res_3),np.asarray(res_4),
           np.asarray(res),
           np.asarray(res2_1),np.asarray(res2_2),
           np.asarray(res2_3),np.asarray(res2_4),
           np.asarray(res2)],
           delimiter =", ",
           fmt ='% s')

plt.plot(res_1)
plt.plot(res_2)
plt.plot(res_3)
plt.plot(res_4)
plt.plot(res2_1)
plt.plot(res2_2)
plt.plot(res2_3)
plt.plot(res2_4)
plt.savefig(file_path_name+time_str+".png",format='png')
# plt.show()

print("Write Complete")
winsound.Beep(frequency, duration)

# As we dont currently have it turned to sleep mode, keep the board running
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
hologram_img_raw = Image.open(image_location)
target_img_raw = hologram_img_raw.resize((1280, 1280))
target_img_gray = target_img_raw.convert("1")
buffwriteA = np.asarray(target_img_gray)
buffwriteB = (buffwriteA + 1) % 2

for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)
    time.sleep(PAUSE)
