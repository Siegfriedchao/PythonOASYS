
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
from os.path import dirname, join as pjoin
import scipy

# My own code
import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm
import util.load_mat as lmat

import winsound
frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second

from multiprocessing.pool import ThreadPool

# Global flag
saveImg = 1
ENABLE_BLUEJAY = True
NUM_TILES = 5

def draw_macro_pixel(row, col, size_ud, size_lr, ptr_left, ptr_top, value, image_np):
# row, col, size, ptr_left, ptr_top, value, image_np
    image_np[ptr_top + row*size_ud:ptr_top + row*size_ud + size_ud ,\
            ptr_left + col*size_lr:ptr_left + col*size_lr + size_lr] = value
    
    return image_np

# Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
# cam.init(trigger=True)
cam.init(trigger=True, width=1024, height=1024, offsetX=480, offsetY=120, exposure=19774)
# set the trigger delay to 1000us=1ms, this gives less fluctuation
cam.set_trigger_delay(1000)

# Init our power meter
pwm.init(beamDiameter = 9, averageCount = 50, wavelength = 658)

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# Plot on the SLM screen
#https://gist.github.com/pjessesco/fce92a1c81c2cd564ddec876fc14d0fd
#https://github.com/sjara/brainmix/blob/master/brainmix/gui/numpy2qimage.py
selected_screen = 1 # why did my screen change today? 2023/01/06
screens_available = app.screens()
screen = screens_available[selected_screen]
screen_width = screen.size().width()
screen_height = screen.size().height()
# Create a black image for init 
pixmap = QPixmap(1920, 1080)
pixmap.fill(QColor('black'))
# Create QLabel object
img_widget = QLabel()
img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
# # Hide mouse cursor 
# img_widget.setCursor(Qt.BlankCursor)
img_widget.setStyleSheet("background-color: black;")
img_widget.setGeometry(0, 0, screen_width, screen_height)
img_widget.setWindowTitle('myImageDisplayApp')
img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop) #https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
img_widget.setPixmap(pixmap)
img_widget.show()
img_widget.windowHandle().setScreen(screen)

img_widget.showFullScreen()
img_widget.clear()

# Set up Bluejay
if ENABLE_BLUEJAY == True:
    # Init our SLM
    slm.init()
    # Set our SLM to only update over SPI commands
    slm.enter_spi_update()
    #  ... Top ...
    # Left ... Right
    #     Bottom
    WIDTH_BJ = 6
    # ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 193, 1063, 92, 972 # initial position, 880*880
    ptr_left_BJ, ptr_right_BJ, ptr_top_BJ, ptr_bottom_BJ = 175, 1055, 60, 940
    pixel_size_BJ = ptr_right_BJ - ptr_left_BJ
    x_dim_BJ = 1280
    y_dim_BJ = 1280
    pix_val_BJ = 0 # Dim
    strip_val_BJ = 1 # Bright
    max_val_BJ = 0
    # Generating test patterns on the fly
    # Creating Guardian
    image_np_BJ = np.full((y_dim_BJ, x_dim_BJ), pix_val_BJ)
    # image_np_BJ[: , ptr_left_BJ:ptr_left_BJ + WIDTH_BJ] = strip_val_BJ
    # image_np_BJ[: , ptr_right_BJ - WIDTH_BJ: ptr_right_BJ] = strip_val_BJ
    # image_np_BJ[ptr_top_BJ:ptr_top_BJ + WIDTH_BJ, :] = strip_val_BJ
    # image_np_BJ[ptr_bottom_BJ - WIDTH_BJ: ptr_bottom_BJ, :] = strip_val_BJ

    # # Creating grid lines
    # for tCnt in range(NUM_TILES):
    #     image_np_BJ[:, ptr_left_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)-WIDTH_BJ/2):
    #             ptr_left_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)+WIDTH_BJ/2)] = strip_val_BJ
    #     image_np_BJ[ptr_top_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)-WIDTH_BJ/2):
    #             ptr_top_BJ+np.uint16((tCnt + 1)*(880/NUM_TILES)+WIDTH_BJ/2), :] = strip_val_BJ

#  ... Top ...
# Left ... Right
#     Bottom
WIDTH = 6
ptr_left, ptr_right, ptr_top, ptr_bottom = 690, 1330, 256, 876 # initial position, 640x620
pixel_size_lr = ptr_right - ptr_left
pixel_size_ud = ptr_bottom - ptr_top
x_dim = 1920
y_dim = 1080
pix_val = 220 # Dim
strip_val = 50 # Bright
condition_flg = False
res_1 = []
ptr_1 = []
max_val = 0

# Generating test patterns on the fly
# Creating Guardian
image_np = np.full((y_dim, x_dim), pix_val)
# image_np[: , ptr_left:ptr_left + WIDTH] = strip_val
# image_np[: , ptr_right - WIDTH: ptr_right] = strip_val
# image_np[ptr_top:ptr_top + WIDTH, :] = strip_val
# image_np[ptr_bottom - WIDTH: ptr_bottom, :] = strip_val

# for tCnt in range(NUM_TILES):
#     image_np[:, ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)-WIDTH/2):ptr_left+np.uint16((tCnt + 1)*(640/NUM_TILES)+WIDTH/2)] = strip_val
#     image_np[ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)-WIDTH/2):ptr_top+np.uint16((tCnt + 1)*(620/NUM_TILES)+WIDTH/2), :] = strip_val

# Initialise the display
gray = np.require(image_np, np.uint8, 'C')
qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
qImg.ndarray = gray
for aa in range(256):
    qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
img_widget.setPixmap(QPixmap(qImg))
QtGui.QApplication.processEvents()

if ENABLE_BLUEJAY == True:
    # Initialise FLC
    buffwriteA = image_np_BJ
    # The other buffer is just inverted
    buffwriteB = (buffwriteA + 1) % 2
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    time.sleep(0.5)

QtGui.QApplication.processEvents()
# Obtain dummy measurements to avoid invalid first data point
pool = ThreadPool(processes=1)
async_result = pool.apply_async(pwm.grab_power)
# do some other stuff in the main process
camera_img = cam.trig_imag(1)
power_data = async_result.get()  # get the return value from your function.
pool.close()
result_camera = np.copy(Image.fromarray(camera_img, 'L'))

time.sleep(1) # wait to refresh, wait longer

# Set up the number of panels we want to slice n by n
num_panels = NUM_TILES
num_pixels = 1025
# Set the void pixel number
num_void = 64
#   results are stored in a 2d array
res_panel = [[0] * num_panels for _ in range(num_panels)]

res_stored = [0 for _ in range(num_panels*num_panels)]
res_temp = [0 for _ in range(num_panels*num_panels)]
res_hadamard = [[0] * num_panels for _ in range(num_panels)]

# some preprocessing of math
slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

i, j = -num_void, 0
x = (num_pixels - num_panels * 2 * num_void) / num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += x
    j += 1
    slice[j] = i
    j += 1

slice = [int(i) for i in slice]

################################################
################################################
NUM_ITERATIONS = 101
NUM_SUBFRAMES = 4

TIME = 0.3
PAUSE = 0.02

count_trigger = 0
res = [] # power meter
res2 = [] # power meter2

value = [50,154] # 50 for neg, 154 for pos

a = 128
b = 384
c = 640
d = 896

x_dim = 1920
y_dim = 1080

for i in range(NUM_ITERATIONS):
    print('iteration:', i)

    matrix_BJ_org = np.random.randint(0, 2, (num_panels, num_panels))
    matrix_AU_org = np.random.randint(0, 2, (num_panels, num_panels))

    # Bluejay needs to be flipped ud
    matrix_BJ = np.flipud(matrix_BJ_org)
    # Aurora needs to be flipped lr and ud
    # matrix_AU = matrix_AU_org
    matrix_AU = np.flip(matrix_AU_org)

    if saveImg == 1:
        file_path_name = "E:/RawExperimentData/RandomSigns/Run3/"
        np.savetxt(file_path_name + "BJ" + str(i) +".csv",
                    np.c_[matrix_BJ_org],
                    delimiter =", ",
                    fmt ='% s')
        np.savetxt(file_path_name + "AU" + str(i) +".csv",
                    np.c_[matrix_AU_org],
                    delimiter =", ",
                    fmt ='% s')

    for subFrame in range(NUM_SUBFRAMES):
        start = time.time()
        # SLM1 matrix needs some pre-processing
        order = np.array([[0,0,0,0],[1,1,1,1]]) # 0 for neg, 1 for pos

        # SLM1 image
        load_BJ = np.zeros((num_panels, num_panels))
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                # Determine whether to apply 0 or 1 depending on which sub-frame
                # and which base element to represent
                element_idx = matrix_BJ[row][col]
                idx = order[element_idx][subFrame]
                image_np_BJ = draw_macro_pixel(row,col,np.int32(pixel_size_BJ / num_panels),
                                                np.int32(pixel_size_BJ / num_panels),
                                                ptr_left_BJ,ptr_top_BJ,idx,image_np_BJ)
                # for debug purpose
                load_BJ[row][col] = idx
        buffwriteA = image_np_BJ
        buffwriteB = (buffwriteA + 1) % 2

        # SLM2 image
        for row in range(0,num_panels):
            for col in range(0,num_panels):
                idx = matrix_AU[row][col]
                # width and height are not the same
                # 640x620
                image_np = draw_macro_pixel(row,col,np.int32(pixel_size_ud / num_panels),
                                                np.int32(pixel_size_lr / num_panels),
                                                ptr_left,ptr_top,value[idx],image_np)
                gray = np.require(image_np, np.uint8, 'C')
        # Apply global offset map
        data_dir = 'E:/GitLab/python_scripts/CGH/src/matlab/'
        mat_fname = pjoin(data_dir, 'run19Matrix2.mat')
        image_np_corrected = lmat.apply_offset(mat_fname, ptr_top, ptr_left, pixel_size_ud, pixel_size_lr, image_np)
        gray = np.require(image_np_corrected, np.uint8, 'C')

        qImg = QtGui.QImage(gray.data, screen_width, screen_height, QtGui.QImage.Format_Indexed8)
        qImg.ndarray = gray
        for aa in range(256):
            qImg.setColor(aa, QtGui.QColor(aa, aa, aa).rgb())
        img_widget.setPixmap(QPixmap(qImg))

        QtGui.QApplication.processEvents()    # you MUST process the plot now
        # time.sleep(PAUSE) # wait to refresh
        # Update our SLM1 frame
        end = time.time()
        slm.update_frame_buffer(buffwriteA)
        slm.spi_show_buf_A()

        time.sleep(TIME) # wait to refresh

        # Obtain measurements
        pool = ThreadPool(processes=1)
        async_result = pool.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img = cam.trig_imag(1)
        power_data = async_result.get()  # get the return value from your function.
        pool.close()
        result_camera = np.copy(Image.fromarray(camera_img, 'L'))

        if saveImg == 1:
            cam_img = Image.fromarray(camera_img, 'L')
            cam_img.save(file_path_name+"Iter" +str(i) + "Sub" + str(subFrame) + ".bmp")
            # for debug purpose
            np.savetxt(file_path_name+"Debug_Iter"+ str(i) + "Sub" + str(subFrame) +"BJ.csv",
                        np.c_[load_BJ],
                        delimiter =", ",
                        fmt ='% s')
        ################################################
        ################################################
        time.sleep(end-start) # wait to refresh

        # Execute but skip the storage of the DC balancing state
        # Update our SLM1 frame for dc balancing
        slm.update_frame_buffer(buffwriteB)
        slm.spi_show_buf_B()

        time.sleep(TIME) # wait to refresh

        # Obtain measurements
        pool2 = ThreadPool(processes=1)
        async_result2 = pool2.apply_async(pwm.grab_power)
        # do some other stuff in the main process
        camera_img2 = cam.trig_imag(1)
        power_data2 = async_result2.get()  # get the return value from your function.
        pool2.close()
        result_camera2 = np.copy(Image.fromarray(camera_img2, 'L'))

        # if saveImg == 1:
        #     cam_img2 = Image.fromarray(camera_img2, 'L')
        #     cam_img2.save(file_path_name+"Inv_Iter" +str(i) + "Sub" + str(subFrame) + ".bmp")

        # Store the values
        res.append(power_data)
        # res2.append(power_data2)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("E:/GitLab/ExperimentData/20230225/PowerMeter"+time_str+".csv", 
           np.c_[np.asarray(res)],
           delimiter =", ",
           fmt ='% s')

print("Write Complete")
winsound.Beep(frequency, duration)

# As we dont currently have it turned to sleep mode, keep the board running
image_location = "../input/" + "checkerboard_128_pixel_off.bmp"
hologram_img_raw = Image.open(image_location)
target_img_raw = hologram_img_raw.resize((1280, 1280))
target_img_gray = target_img_raw.convert("1")
buffwriteA = np.asarray(target_img_gray)
buffwriteB = (buffwriteA + 1) % 2

for i in range(10000):
    # Update our SLM1 frame
    slm.update_frame_buffer(buffwriteA)
    slm.spi_show_buf_A()
    slm.check_status()
    # Give the SLM sometime to update
    time.sleep(PAUSE)
    # Sleep for the rest of the period
    time.sleep(TIME)

    # Update our SLM1 frame for dc balancing
    slm.update_frame_buffer(buffwriteB)
    slm.spi_show_buf_B()
    slm.check_status()

    time.sleep(TIME)
    time.sleep(PAUSE)
