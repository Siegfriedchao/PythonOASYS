# -*- coding: utf-8 -*-
"""
Test Application to get a feel for how hard it is to use PyQTGraph
"""

# import initExample ## Add path to library (just for examples; you do not need this)
from enum import Enum
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import matplotlib.pyplot as plt
# from scipy.fft import fft2, fftshift
import scipy as sp
import pyqtgraph as pg
import pyqtgraph.opengl as gl
from PIL import Image

# My own code
from alg import dbs
from alg import gs
from alg import ben
# from alg import sgd
from alg import soft_citl
from util import gen_header



# import pyqtgraph.examples
# pyqtgraph.examples.run()




# Selectable MACRO Options

#############################################
######### CGH Algorithm Choice ##############
#############################################
# Which algorithm, are we using? Options are:
#    - GS
#    - DBS
#    - User-Defined Algorithm
class ALG_TYP(Enum):
    GS   = 0
    DBS  = 1
    USER = 2
# G-S is the default
SELECTED_ALG = ALG_TYP.GS

#############################################
############## Program Mode #################
#############################################
# We can run our program in one of two modes:
#  - CGH Mode. Here the program takes Target Replay Field, replay field that we will run the selected CGH algorithm on. This is the main 'mode' of operation.
#  - Hologram Mode. Here we just load a static hologram and don't run CGH, just simulate what we expect to see. Hologram needs to be a binary-phase image. This let's us see what we expect for given hologram.
# Note on formats:
# In CGH Mode, Pillow is pretty flexible and can handle most image types and formats, but to keep things easy, use png of jpg and try to have the image square.
# Note that the script will add padding to handle the conjugate image
# If we load a Hologram, use a 8-bit, monochrome, 'png' image with following pixel value to phase-delay mapping:
#     - A pixel value of   0 maps to  +pi/2 (ie:  90 deg) phase-delay
#     - A pixel value of 255 maps to +3pi/2 (ie: 270 deg) phase-delay
class PROG_MODE(Enum):
    CGH = 0
    HOL = 1
# Default mode is CGH
SELECTED_MODE = PROG_MODE.CGH










#############################################
########### Configure our GUI ###############
#############################################
# Uses a combination of:
#  - QT5:       Gui Toolkit
#  - PySide:    Official QT-supported python bindings for QT
#  - PyQtPlot:  Open-GL accelerated Plotting Package for Python with QT - see http://www.pyqtgraph.org/

# Setup Options   
# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')
# Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])
# # Create window
window = QtGui.QMainWindow()
# Setup our Window
parent_widget = QtGui.QWidget()
window.setCentralWidget(parent_widget)

# Code to select where our GUI starts up - super useful for development with multiple screens
# Choose which monitor we are going to display on
# From https://stackoverflow.com/questions/6854947/how-to-display-a-window-on-a-secondary-display-in-pyqt
display_monitor = 1 # the number of the monitor you want to display your widget
monitor = QDesktopWidget().screenGeometry(display_monitor)
window.move(monitor.left(), monitor.top())
window.showFullScreen()
# Resize the Window if you, need to get rid of lower options if you want to keep
# window.resize(1280,1024)
# window.show()
# Different Fullscreen options
window.showMaximized()
# window.showFullScreen()
window.setWindowTitle('pyqtgraph example: GraphicsLayout')
# Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
parent_widget.setLayout(layout)











#############################################
################ GUI Layout #################
#############################################

# Hologram Display Box Defintion
# For showing our Hologram, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
hologram_plot = pg.PlotItem(title='Hologram Phase - Radians', invertY=True)
hologram_plot.hideAxis('left')
hologram_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
hologram_view = pg.ImageView(view=hologram_plot) 

# Replay Field Display Box Definition
# For showing our Replay Field, we take a set of axes and put an ImageView inside of it. Gives us a nice Title and spacing
replay_field_plot = pg.PlotItem(title='Replay Field Magnitude - Log10 Scale', invertY=True)
replay_field_plot.hideAxis('left')
replay_field_plot.hideAxis('bottom')
 # Set the plot to the ImageView we want to show
replay_field_view = pg.ImageView(view=replay_field_plot)

# Image Displays for Expected Magnitude and Phase from simulation
# These both go in raw images
# We want to show a raw image, so we have a graphics view on the outside
sim_mag_graphics_view   = pg.GraphicsLayoutWidget()
sim_phase_graphics_view = pg.GraphicsLayoutWidget()
# Inside the graphics view, we place a viewbox - this will contain the image later on
sim_mag_graphics_viewbox   = pg.ViewBox(invertY=True)
sim_phase_graphics_viewbox = pg.ViewBox(invertY=True)
sim_mag_graphics_viewbox.setAspectLocked()
sim_phase_graphics_viewbox.setAspectLocked()

# We use an openGL-accelerated Surface View to show our replay field
replay_surface_view = gl.GLViewWidget()
replay_surface_view.setCameraPosition(distance=50)
replay_surface_view.show
## Add a grid to the view
surf_grid = gl.GLGridItem()
# Draw grid after surfaces since they may be translucent
surf_grid.setDepthValue(10)
replay_surface_view.addItem(surf_grid)

# Now that we have defined all of our views, specify where they are going to sit in the application
# We use the PyQtGraph built-in Grid infrastructure to achieve this
# Specific row, col positions and row, col spans
# There is probably a more space-efficient way to do this, but this works just fine
hol_row_pos     = 0
hol_col_pos     = 0
hol_row_span    = 4
hol_col_span    = 3
replay_row_pos  = 0
replay_col_pos  = 3
replay_row_span = 4
replay_col_span = 3
mag_row_pos     = 4
mag_col_pos     = 0
mag_row_span    = 2
mag_col_span    = 2
phase_row_pos   = 4
phase_col_pos   = 2
phase_row_span  = 2
phase_col_span  = 2
surf_row_pos    = 4
surf_col_pos    = 4
surf_row_span   = 2
surf_col_span   = 2
# Now add all our widgets
layout.addWidget(hologram_view,           hol_row_pos,      hol_col_pos,    hol_row_span,    hol_col_span)
layout.addWidget(replay_field_view,       replay_row_pos,   replay_col_pos, replay_row_span, replay_col_span)
layout.addWidget(sim_mag_graphics_view,   mag_row_pos,      mag_col_pos,    mag_row_span,    mag_col_span)
layout.addWidget(sim_phase_graphics_view, phase_row_pos,    phase_col_pos,  phase_row_span,  phase_col_span)
layout.addWidget(replay_surface_view,     surf_row_pos,     surf_col_pos,   surf_row_span,   surf_col_span)








#############################################
################## Run CGH ##################
#############################################

# OPTION 1: Run our CGH to give us holograms to plot
# GS
# [hologram_array, replay_array] = gs.run_cgh_gs("einstein.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("smiley_face.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("mandrill.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("clown_test.gif")
# [hologram_array, replay_array] = gs.run_cgh_gs("romeo_b.jpg")
# [hologram_array, replay_array] = gs.run_cgh_gs("smiley_face_inv.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("smiley_face_inv.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("flat.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("smiley_face_mini.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("smiley_face_tiled.png")
# [hologram_array, replay_array] = gs.run_cgh_gs("vertical_grating.bmp")
# [hologram_array, replay_array] = gs.run_cgh_gs("checkerboard.bmp")
# [hologram_array, replay_array] = gs.run_cgh_gs("dot.png")

# DBS
# [hologram_array, replay_array] = dbs.run_cgh_dbs("einstein.png")
# [hologram_array, replay_array] = dbs.run_cgh_dbs("smiley_face_inv.png")
# [hologram_array, replay_array] = dbs.run_cgh_dbs("circle_inv.png")
# Ben
# [hologram_array, replay_array] = ben.run_cgh_ben("einstein.png")
# Soft CITL
# [hologram_array, replay_array] = soft_citl.run_cgh_soft_citl("romeo_b.jpg")
# [hologram_array, replay_array] = soft_citl.run_cgh_soft_citl("smiley_face_mini.png")
# [hologram_array, replay_array] = soft_citl.run_cgh_soft_citl("vertical_grating.png")
# [hologram_array, replay_array] = soft_citl.run_cgh_soft_citl("vertical_grating.png")

# SGD
# [hologram_array, replay_array] = sgd.run_cgh_sgd("smiley_face_inv.png")

# Save our Hologram if we run CGH
# gen_header.gen_binary_phase_bitmap(hologram_array)


# Temp step - should be in run_cgh function
# hologram_array = hologram_array + 1j*np.pi


# OPTION 2: Directly compute our replay field from the specified hologram
[hologram_array, replay_array] = gs.calc_replay_field("hologram/generated_hologram_CM128.bmp")
# [hologram_array, replay_array] = gs.calc_replay_field("checkerboard.bmp")
# [hologram_array, replay_array] = gs.calc_replay_field("romeo_holo.png")
# [hologram_array, replay_array] = gs.calc_replay_field("../output/generated_hologram.bmp")










#############################################
##### Load Outputs onto our GUI Widgets #####
#############################################

# We map our outputs to 8-bit images
MAX_IMG_VALUE = 255.0 
# Convert our replay field magnitude and phase into images, scaled such that the maximum values corresponds to 255
replay_field_mag_rescaled    = np.abs(replay_array)   * MAX_IMG_VALUE/np.max(np.abs(replay_array))
replay_field_phase_rescaled  = np.angle(replay_array) * MAX_IMG_VALUE/np.max(np.angle(replay_array))
# Take the log_base_10 as otherwise we get range issues
# Also add a tiny little amount so we avoid log(0) = infinity issues
tiny_offset = 10**-20
# replay_field_amplitude_log = np.log10(replay_field_mag_rescaled+tiny_offset)
replay_field_amplitude_log = replay_field_mag_rescaled

# Display our Hologram
# Optional Colour-map
# hologram_view.setPredefinedGradient('viridis') 
# Our binary-phase hologram array is complex numbers of {1j, -1j}
# In order to display these as an image where we have {pi/2, 2pi/2}, we find the angle and then add pi
hologram_binary_to_plot = np.angle(hologram_array)+np.pi
# Plot
hologram_view.setImage(hologram_binary_to_plot)

# Display our Replay Field
# Optional Colour-map
replay_field_view.setPredefinedGradient('viridis') 
replay_field_view.setImage(replay_field_amplitude_log)

# Display our Abs Magnitude and Phase Images
# Load up our images through their viewboxes
# Labels for our Hologram Image
sim_mag_graphics_view.addLabel("Magnitude", size='12pt')
sim_phase_graphics_view.addLabel("Phase", size='12pt')
sim_mag_graphics_view.nextRow()
sim_phase_graphics_view.nextRow()
# Now add the arrays, note they are is in the viewbox
sim_mag_graphics_view.addItem(sim_mag_graphics_viewbox)
sim_phase_graphics_view.addItem(sim_phase_graphics_viewbox)
replay_field_mag_graphics_img   = pg.ImageItem(replay_field_mag_rescaled, border='b')
replay_field_phase_graphics_img = pg.ImageItem(replay_field_phase_rescaled, border='b')
replay_field_mag_graphics_img.setLevels([0.0, MAX_IMG_VALUE])
replay_field_phase_graphics_img.setLevels([0.0, MAX_IMG_VALUE])
sim_mag_graphics_viewbox.addItem(replay_field_mag_graphics_img)
sim_phase_graphics_viewbox.addItem(replay_field_phase_graphics_img)

# Load up our surface plots for our Replay Field
# Colour-Map Stuff
minZ=np.min(replay_field_amplitude_log)
maxZ=np.max(replay_field_amplitude_log)
cmap = plt.get_cmap('jet')
rgba_img = cmap((replay_field_amplitude_log-minZ)/(maxZ -minZ))
# Plotting
(x_pixels, y_pixels) = np.shape(replay_field_amplitude_log)
surf_plot = gl.GLSurfacePlotItem(z=replay_field_amplitude_log, colors=rgba_img, computeNormals=False)
surf_plot.scale(2.0*10/x_pixels, 2.0*10/y_pixels, 1.0/255.0)
# p1.translate(0.0, 0.0, 20)
surf_plot.translate(-10.0, -10.0, 0)
# p1.scale(2*10./x_pixels, 2*10./y_pixels, 1.0)
replay_surface_view.addItem(surf_plot)









# Display the widget as a new window
window.show()

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()