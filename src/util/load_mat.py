from os.path import dirname, join as pjoin
import scipy
import numpy as np
from matplotlib import pyplot as plt

def apply_offset(mat_fname, ptr_top, ptr_left, pixel_size_ud, pixel_size_lr, image_np):
    mat_contents = scipy.io.loadmat(mat_fname)
    offset = np.asarray(mat_contents['offset'])

    # Aurora needs to be flipped lr and ud
    offset = np.flip(offset)
    # we need to interpolate the offset map to the size of canvas
    y = np.linspace(0, 1, len(offset))
    x = np.linspace(0, 1, len(offset[0]))

    f = scipy.interpolate.interp2d(x, y, offset, kind='cubic')
    # f = scipy.interpolate.interp2d(x, y, offset, kind='nearest')

    y2 = np.linspace(0, 1, pixel_size_ud)
    x2 = np.linspace(0, 1, pixel_size_lr)
    offset_interp = f(x2, y2)

    offset_map = np.full((len(image_np),len(image_np[0])), 0)

    offset_map[ptr_top:ptr_top + pixel_size_ud,\
               ptr_left:ptr_left + pixel_size_lr] = offset_interp

    image_np_corrected = image_np - offset_map
    # make sure we create the safety boundary
    image_np_corrected[image_np_corrected < 0] = 0
    image_np_corrected[image_np_corrected > 255] = 255

    return image_np_corrected

# data_dir = '/Users/youchaowang/Documents/GitLab/python_scripts/CGH/src/matlab/'
# mat_fname = pjoin(data_dir, 'test_matrix.mat')
# mat_contents = scipy.io.loadmat(mat_fname)

# offsetMap = np.asarray(mat_contents['offset'])
# y_dim = 1080
# x_dim = 1920
# pix_val = 32

# ptr_left, ptr_right, ptr_top, ptr_bottom = 690, 1330, 256, 876 # initial position, 640x620
# pixel_size_lr = ptr_right - ptr_left
# pixel_size_ud = ptr_bottom - ptr_top

# image_np = np.full((y_dim, x_dim), pix_val)

# mat_contents = scipy.io.loadmat(mat_fname)
# offset = np.asarray(mat_contents['offset'])
# # we need to interpolate the offset map to the size of canvas
# y = np.linspace(0, 1, len(offset))
# x = np.linspace(0, 1, len(offset[0]))

# f = scipy.interpolate.interp2d(x, y, offset, kind='cubic')

# y2 = np.linspace(0, 1, pixel_size_ud)
# x2 = np.linspace(0, 1, pixel_size_lr)
# offset_interp = f(x2, y2)

# offset_map = np.full((len(image_np),len(image_np[0])), pix_val)

# offset_map[ptr_top:ptr_top + pixel_size_ud, ptr_left:ptr_left + pixel_size_lr] = offset_interp

# image_np_corrected = image_np + offset_map
# # make sure we create the safety boundary
# image_np_corrected[image_np_corrected < 0] = 0
# image_np_corrected[image_np_corrected > 255] = 255

# plt.imshow(image_np_corrected)
# plt.show()


# arr = scipy.misc.face(gray=True)

# y = np.linspace(0, 1, arr.shape[0])
# x = np.linspace(0, 1, arr.shape[1])
# f = scipy.interpolate.interp2d(x, y, arr, kind='cubic')

# x2 = np.linspace(0, 1, 1000)
# y2 = np.linspace(0, 1, 1600)
# arr2 = f(x2, y2)

# arr.shape # (768, 1024)
# arr2.shape # (1000, 1600)

# plt.figure()
# plt.imshow(arr)
# plt.figure()
# plt.imshow(arr2)
# plt.show()