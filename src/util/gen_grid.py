from matplotlib import image
import numpy as np
import matplotlib.pyplot as plt

# to read the image stored in the working directory
# data = image.imread('E:/GitLab/python_scripts/CGH/data/fourpanel/offset_test.bmp')
data = image.imread('E:/GitLab/python_scripts/CGH/analysis/replayfield_image20220607-141658.bmp')

# # Set up the number of panels we want to slice n by n
# num_panels = 3
# num_pixels = 1023
# # Set the void pixel number
# num_void = 64
# #   results are stored in a 2d array
# res = [[0] * num_panels for _ in range(num_panels)]

# # some preprocessing of math
# slice = [ 0 for _ in range(2*num_panels)] # 2 for start and end

# i, j = -num_void, 0
# x = (num_pixels - num_panels * 2 * num_void) / num_panels
# #TODO: sanity check for x here!

# while i + num_void < num_pixels:
#     i += 2 * num_void
#     slice[j] = i
#     i += x
#     j += 1
#     slice[j] = i
#     j += 1

# slice = [int(i) for i in slice]

# TODO: write up the code such that we can generate lines automatically

num_panels = 3
num_pixels = 1024
num_void = 64
y = np.zeros((num_panels * 2, 2))
x = np.zeros((num_panels * 2, 2))
# in fact x and y's are the same

y_guardian = [0, num_pixels]
x_guardian = [0, num_pixels]

# some preprocessing of math
slice = np.zeros(num_panels * 2) # 2 for start and end

i, j = -num_void, 0
xx = (num_pixels - num_panels * 2 * num_void) / num_panels
# num_pixels might not be divisible by num_panels
#TODO: sanity check for x here!

while i + num_void < num_pixels:
    i += 2 * num_void
    slice[j] = i
    i += xx
    j += 1
    slice[j] = i
    j += 1

for i in range(num_panels * 2):
	y[i] = slice[i]
	x[i] = slice[i]
	plt.plot(x[i], y_guardian, color="white", linewidth=1)
	plt.plot(y[i], x_guardian, color="white", linewidth=1)

plt.axis('off')
plt.imshow(data, cmap=plt.get_cmap('gray'))
plt.show()


y0 = [0, 1024]
x1 = [64, 64]
x2 = [277, 277]
x3 = [405, 405]
x4 = [618, 618]
x5 = [746, 746]
x6 = [959, 959]

x0 = [0, 1024]
y1 = [64, 64]
y2 = [277, 277]
y3 = [405, 405]
y4 = [618, 618]
y5 = [746, 746]
y6 = [959, 959]

plt.plot(x1, y0, x2, y0, x3, y0, x4, y0, x5, y0, x6, y0, color="white", linewidth=1)
plt.plot(x0, y1, x0, y2, x0, y3, x0, y4, x0, y5, x0, y6, color="white", linewidth=1)
plt.axis('off')
plt.imshow(data, cmap=plt.get_cmap('gray'))
plt.show()