
# -*- coding: utf-8 -*-
"""
Python Script to generate the c# headers of an appropriate array
"""

# import initExample ## Add path to library (just for examples; you do not need this)

# from PyQt5.QtWidgets import QDesktopWidget
# from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
# from scipy.fft import fft2, fftshift
# import scipy as sp
# import pyqtgraph as pg
# import pyqtgraph.opengl as gl
from PIL import Image



# Generate a 1-bit binary header file and save as output from a passed hologram
def gen_binary_phase_bitmap(convert_me):

    # First thing we need to do is to take our phase-hologram and convert it to a 1-bit data structure
    # Quanitise these phases to give us our updated hologram
    # For pi/2, 3pi/2, we firstly put each point into 2 bins, 0 and 1, where:
    #    { [0    ,  pi[   }  ->    0
    #    { [pi   ,  2*pi] }  ->    1
    # Note that the sign for pi is just the default right=False for np.digitize()
    binary_phase_single_bit_array = np.digitize( convert_me.imag, bins=[0] )

    # Optional to rotate our image by 90 degrees because our SLM is mounted at 90 Deg
    # binary_phase_single_bit_rot_array = np.rot90(binary_phase_single_bit_array)
    binary_phase_single_bit_rot_array = binary_phase_single_bit_array#np.rot90(binary_phase_single_bit_array)

    # Now we 'pack' up our numpy array by grouping up every 8 entries into a single byte
    # Do this over the flattened array
    # Do this big-endian
    binary_phase_packed_array = np.packbits(binary_phase_single_bit_rot_array, axis=None, bitorder='big')
    # Reshape our array to be 1280 lines of 160 bytes long - this is the structure of the frame buffer that the SLM expects
    SLM_BYTES_PER_LINE = 160
    SLM_NUM_LINES      = 1280
    binary_phase_packed_2d = binary_phase_packed_array.reshape(SLM_BYTES_PER_LINE, SLM_NUM_LINES)
    # binary_phase_packed_2d = binary_phase_packed_array.reshape(64, 8)

    # Now that we have our 2-D array, save it as a csv
    header_text = "namespace HoloRelay{   class PythonImages{     public static byte[] generated_frame_buffer = new byte[] {"
    footer_text = "        };     }}"
    np.savetxt("output/generated_frame_buffer.cs", binary_phase_packed_2d, delimiter=",", fmt=' 0x%02X', header=header_text, footer=footer_text, comments='', newline=',\n')
    # TODO: Add code to delete the commas at the end of the header and footer, .cs doesn't like them

    # We also save our hologram as a bmp where 0 -> pi/2 and 255 -> 3pi/2
    # Need to convert to 8-bit single channel greyscale or get bug, see - https://stackoverflow.com/questions/32159076/python-pil-bitmap-png-from-array-with-mode-1
    binary_phase_8_bit_array = np.array(binary_phase_single_bit_array * 255, dtype="uint8")
    binary_image = Image.fromarray(binary_phase_8_bit_array)
    # binary_image.show()
    binary_image.save("output/generated_hologram.bmp")
    print(binary_phase_packed_2d)

    # Now we iterate through this array, saving every 160 bytes of data to disk - this corresponds to 1 line


    # Now convert to 1-bit image
    # binary_phase_single_bit_image = Image.fromarray(binary_phase_single_bit_array, '1')

    # print(binary_phase_single_bit_image)
    # binary_phase_single_bit_image.show()
    # print(hologram_array_binned)

        # # Note that the sign for 1 is just the default right=False for np.digitize()
        # hologram_array_binned = np.digitize( np.angle(hologram_array), bins=[0] )
        # # These bins are then mapped to -pi and +pi in turn
        # hologram_array_quantised = (hologram_array_binned-0.5)*2*np.pi
        # hologram_array = np.ones( target_array.shape ) * np.exp( 1j*np.angle(hologram_array_quantised) )



# Convert a complex phase bitmap to a numpy array scaled from 0 to 255
def conv_complex_phase_bitmap_to_8_bit_binary_array(convert_me):

    # First thing we need to do is to take our phase-hologram and convert it to a 1-bit data structure
    # Quanitise these phases to give us our updated hologram
    # For pi/2, 3pi/2, we firstly put each point into 2 bins, 0 and 1, where:
    #    { [0    ,  pi[   }  ->    0
    #    { [pi   ,  2*pi] }  ->    1
    # Note that the sign for pi is just the default right=False for np.digitize()
    binary_phase_single_bit_array = np.digitize( convert_me.imag, bins=[0] )

    # We also save our hologram as a bmp where 0 -> pi/2 and 255 -> 3pi/2
    # Need to convert to 8-bit single channel greyscale or get bug, see - https://stackoverflow.com/questions/32159076/python-pil-bitmap-png-from-array-with-mode-1
    binary_phase_8_bit_array = np.array(binary_phase_single_bit_array * 255, dtype="uint8")
    return binary_phase_8_bit_array
