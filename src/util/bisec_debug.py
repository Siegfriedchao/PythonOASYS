import bisect
import numpy as np

limit = [0.1, 1.90, 3.24, 4.76, 6.06, 7.30, 8.68, 9.92, 11.15, 11.89, 17.51, 27.34, 37.42, 46.41, 55.12, 65.90, 75.50, 84.17, 120]  # This needs to be a sorted list
matched_value = [40000, 16000, 4000, 1600, 400, 160, 40, 16, 4, 100000, 40000, 10000, 4000, 1000, 400, 100, 40, 10]

def find_range(value):
    idx = bisect.bisect_left(limit, value) - 1
    if idx == -1:
        idx = 0
    if idx >= len(matched_value):
        return np.nan
    return matched_value[idx], idx

# Debug purpose
cnt = 0

a = 6
b = find_range(a)
print(b)