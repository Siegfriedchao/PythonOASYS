    
# -*- coding: utf-8 -*-
"""
Python Functions to provide Generic Utility
"""

# import initExample ## Add path to library (just for examples; you do not need this)

# from PyQt5.QtWidgets import QDesktopWidget
# from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
# from scipy.fft import fft2, fftshift
# import scipy as sp
# import pyqtgraph as pg
# import pyqtgraph.opengl as gl
import matplotlib.pyplot as plt
from PIL import Image
import torch
import torchvision.transforms as transforms
    

# Load an image as a tensor of shape (1, 1, H, W) with padding
def load_img_as_tensorwith_padding(target_image_filename: str, hol_num_pixels_x, hol_num_pixels_y):

    # Load our Target Image (Target Amplitude)
    # Load up our Target Image
    # target_img_raw = Image.open("./input/mandrill.png")
    image_location = "../input/" + target_image_filename
    target_img_raw = Image.open(image_location)
    # Pad it so we can handle conjugate symmetry
    img_width, img_height = target_img_raw.size
    padding_left  = int(img_width/2.0)
    padding_right = int(img_width/2.0)
    padding_top    = 0
    padding_bottom = int(img_height)
    SINGLE_CHANNEL_ZERO = (0)
    # RGB_CHANNEL_ZERO    = (0, 0, 0)
    target_img_cropped = Image.new(target_img_raw.mode, (2*img_width, 2*img_height), SINGLE_CHANNEL_ZERO )
    target_img_cropped.paste(target_img_raw, (padding_left, padding_top))
    # Resize it to our Hologram dimension
    target_img_raw = target_img_cropped.resize((hol_num_pixels_x, hol_num_pixels_y))
    # Make-sure that the image is grayscale, scaled to be betweeon 0.0 and 255.0
    target_img_gray = target_img_raw.convert("F")
    # Print out what format we are in to make sure
    print("Loaded Target Image Format is: ", target_img_gray.format, target_img_gray.size, target_img_gray.mode)
    # Convert our Image into an array
    # target_array = np.asarray(target_img_gray)
    # Print a line + plot as a sense-check
    # print(target_array[200::])
    # Image.fromarray(target_array).show()
    # target_img_gray.show()

    # Convert our image into a Tensor, PyTorch gives us functions to do this
    pil2tensor = transforms.ToTensor()
    target_img_gray_tensor = pil2tensor(target_img_gray)

    # If you want to plot the tensor
    # plt.figure()
    # # imshow needs a numpy array with the channel dimension
    # # as the the last dimension so we have to transpose things.
    # plt.imshow(target_img_gray_tensor.numpy().transpose(1, 2, 0))
    # plt.show()

    # Now return our tensor
    return target_img_gray_tensor






# Helper function to convert imaginary number tensor from polar to cartesian coords
#  Takes the float tensors (mag, ang) pair as parameters - ie: don't put-in complex tensors
def conv_polar_to_cartesian_tensor(mag, ang):
    real = mag * torch.cos(ang)
    imag = mag * torch.sin(ang)
    return real, imag



# Helper function to convert imaginary number tensor from cartesian to polar coords
#  Takes the float (real, imag) pair as parameters (so get them with complex.real and complex.imag)
def conv_cartesian_to_polar_tensor(real, imag):

    # for i in real[0]:
    #     for j in i:
    #         if torch.isnan(j):
    #             print("Why!!")

    # for i in imag[0]:
    #     for j in i:
    #         if torch.isnan(j):
    #             print("Why Imag!!")

    # print(f"Largest Values: Real:{torch.min(real)} Imag:{torch.min(imag)}")
    # print()
    # print()
    # print()

    mag = torch.pow(real**2 + imag**2, 0.5)
    ang = torch.atan2(imag, real)

    # complex = torch.polar(real, imag)
    # mag = complex.abs()
    # ang = complex.angle()

    return(mag, ang)




# Helper function to plot a complex tensor as a 2x2 subplot in both co-ordinate systems
fig_existing, ax_existing = plt.subplots(2, 2)
def plot_complex_tensor_both_cords(plot_me, plot_title: str, plot_mag_log_10=False, update_existing_fig=False):

    plt.ion()

    # Cartesian First
    # Create a clone because we cannot call numpy() if our Tensor requires grad
    # Have to use tensor.detach().numpy() instead.
    plot_me_cloned = plot_me.clone()
    plot_me_detached = plot_me_cloned.detach()
    # Convert our tensor to a numpy array to plot
    plot_me_real = plot_me_detached[0,:,:].real.numpy()
    plot_me_imag = plot_me_detached[0,:,:].imag.numpy()
    # Polar co-ordinates too
    (plot_me_mag, plot_me_ang) = conv_cartesian_to_polar_tensor(plot_me_detached.real, plot_me_detached.imag)
    plot_me_mag = plot_me_mag[0,:,:]
    plot_me_ang = plot_me_ang[0,:,:]
    # Convert mag to log10 basis if flag is set
    if plot_mag_log_10:
        plot_me_mag = torch.log10(plot_me_mag)

    # Plot on a 2x2 subplot unless we have set the update_existing_fig flag
    fig = 0
    ax  = 0
    if update_existing_fig==True:
        fig = fig_existing
        ax =  ax_existing
        ax[0][0].clear()
        ax[1][0].clear()
        ax[0][1].clear()
        ax[1][1].clear()
    else:
        fig, ax = plt.subplots(2, 2)
    fig.suptitle(plot_title)
    #   Top-Left
    ax_id = ax[0][0]
    curr_plot = plot_me_real
    ax_id.set_title('Cartesian - Real')
    img_id = ax_id.imshow(curr_plot, cmap='plasma', interpolation='None')
    # plt.colorbar(img_id, ax=ax_id)
    #   Bot-Left
    ax_id = ax[1][0]
    curr_plot = plot_me_imag
    ax_id.set_title('Cartesian - Imag')
    img_id = ax_id.imshow(curr_plot, cmap='plasma', interpolation='None')
    # plt.colorbar(img_id, ax=ax_id)
    # Top-Right
    ax_id = ax[0][1]
    curr_plot = plot_me_mag
     # Ternary operator to change title to Log10 if flag set
    ax_id.set_title('Polar - log10(Mag)') if plot_mag_log_10==True else ax_id.set_title('Polar - Mag')
    img_id = ax_id.imshow(curr_plot, cmap='plasma', interpolation='None')
    # plt.colorbar(img_id, ax=ax_id)
    # Bot-Right
    ax_id = ax[1][1]
    curr_plot = plot_me_ang
    ax_id.set_title('Polar - Angle')
    img_id = ax_id.imshow(curr_plot, cmap='plasma', interpolation='None')
    # plt.colorbar(img_id, ax=ax_id)
    plt.show()
    plt.pause(0.005)

    plt.ioff()



# Helper function to show an array of images
#  Eg: show([dog1, dog2])
#  From https://pytorch.org/vision/stable/auto_examples/plot_scripted_tensor_transforms.html#sphx-glr-auto-examples-plot-scripted-tensor-transforms-py
def show_img(imgs):

    plt.ion()
    fix, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = transforms.ToPILImage()(img.to('cpu'))
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    plt.show()
    plt.ioff()



# Helper function to continuously update a single image
#  Eg: show([dog1, dog2])
#  From https://pytorch.org/vision/stable/auto_examples/plot_scripted_tensor_transforms.html#sphx-glr-auto-examples-plot-scripted-tensor-transforms-py
img_fig_existing, img_ax_existing = plt.subplots(ncols=1)
def update_img(imgs):

    plt.ion()
    # fix, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    # for i, img in enumerate(imgs):
    img = transforms.ToPILImage()(imgs.to('cpu'))
    img_ax_existing.imshow(np.asarray(img))
    img_ax_existing.set(xticklabels=[], yticklabels=[], xticks=[], yticks=[])
    plt.show()
    plt.ioff()