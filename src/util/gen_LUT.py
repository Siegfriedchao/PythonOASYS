import numpy as np

def gen_LUT(intRange = 383, arr=[1]):

    arr = np.asarray(arr)
    arrLen = len(arr)
    arr[::-1].sort()
    
    # Create the LUT container
    container = np.zeros([intRange + 1, arrLen])
    for number in range(0,intRange+1):
        currentNum = number
        for i in range(arrLen):
            container[number][i] = np.floor(currentNum/arr[i])
            currentNum -= container[number][i]*arr[i]

    return container

def load_SLM(baseElement):
    # Generate a pair of integers
    intRange = 383
    intLUT = gen_LUT(intRange, baseElement)
    intA = np.random.randint(0,intRange)
    intB = np.random.randint(0,intRange)
    # intA = 148# 4, 47, 160
    # intB = 274# 1,4,14,47,160
    # intA = 232#1,4,14,47,160
    # intB = 291#1,4,14,47,160

    lenA = len(np.nonzero(intLUT[intA])[0])
    lenB = len(np.nonzero(intLUT[intB])[0])
    elementA = baseElement[np.nonzero(intLUT[intA])]
    elementB = baseElement[np.nonzero(intLUT[intB])]

    # Unload the numbers onto SLM
    SLM1Load = []
    SLM2Load = []
    for iB in range(lenB):
        for iA in range(lenA):
            SLM1Load.append(elementA[iA])
            SLM2Load.append(elementB[iB])

    integersLoad = [intA, intB]
    loadLength = len(SLM1Load)

    # Reshape the matrix to fit the 5x5 panel
    if len(SLM1Load) != 25:
        # SLM1Load = np.append(np.asarray(SLM1Load), np.zeros(25 - len(SLM1Load)))
        # SLM2Load = np.append(np.asarray(SLM2Load), np.zeros(25 - len(SLM2Load)))

        # we need to fill the unused tiles with some random values, instead of null
        SLM1Load = np.append(np.asarray(SLM1Load), np.random.choice(baseElement, 25 - len(SLM1Load)))
        SLM2Load = np.append(np.asarray(SLM2Load), np.random.choice(baseElement, 25 - len(SLM2Load)))

    print(SLM1Load)
    print(SLM2Load)

    return SLM1Load, SLM2Load, integersLoad, loadLength


baseElement = [1,4,14,47,160]
baseElement = np.asarray(baseElement)
NUM_TILES = 5

SLM1Load, SLM2Load, integers_load, load_length = load_SLM(baseElement)

Temp1Load = np.copy(SLM1Load)
Temp2Load = np.copy(SLM2Load)
# matrix_BJ_org = np.asarray(Temp1Load).reshape((NUM_TILES,NUM_TILES))
# matrix_AU_org = np.asarray(Temp2Load).reshape((NUM_TILES,NUM_TILES))
# # Bluejay needs to be flipped ud
# matrix_BJ = np.flipud(matrix_BJ_org)
# # Aurora needs to be flipped lr and ud
# # matrix_AU = matrix_AU_org
# matrix_AU = np.flip(matrix_AU_org)

# print(matrix_BJ_org)
# print(matrix_BJ)
# print(matrix_AU_org)
# print(matrix_AU)

# # Need to determine which order
# matrix_AU[matrix_AU == 4] = 0
# matrix_AU[matrix_AU == 1] = 3
# matrix_AU[matrix_AU == 160] = 1
# matrix_AU[matrix_AU == 47] = 2
# matrix_AU[matrix_AU == 14] = 4

# matrix_BJ[matrix_BJ == 160] = 0
# matrix_BJ[matrix_BJ == 14] = 2
# matrix_BJ[matrix_BJ == 4] = 3
# matrix_BJ[matrix_BJ == 1] = 4
# matrix_BJ[matrix_BJ == 47] = 1

# print(matrix_BJ)
# print(matrix_AU)
# print(SLM1Load)
# print(SLM2Load)