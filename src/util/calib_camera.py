import cv2
import numpy as np
import pathlib

#https://longervision.github.io/2017/03/18/ComputerVision/OpenCV/opencv-internal-calibration-circle-grid/
#https://opencv24-python-tutorials.readthedocs.io/en/stable/py_tutorials/py_calib3d/py_calibration/py_calibration.html
def calibrate_chessboard(dir_path, image_format, square_size, width, height):
    '''Calibrate a camera using chessboard images.'''
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(8,6,0)
    objp = np.zeros((height*width, 3), np.float32)
    objp[:, :2] = np.mgrid[0:width, 0:height].T.reshape(-1, 2)
    objp = objp * square_size

    # Original blob coordinates, supposing all blobs are of z-coordinates 0
    # And, the distance between every two neighbour blob circle centers is 100 milimetres
    # In fact, any number can be used to replace 100.
    # Namely, the real size of the circle is pointless while calculating camera calibration parameters.
    objp = np.zeros((44, 3), np.float32)
    objp[0]  = (0  , 0  , 0)
    objp[1]  = (0  , 72 , 0)
    objp[2]  = (0  , 144, 0)
    objp[3]  = (0  , 216, 0)
    objp[4]  = (36 , 36 , 0)
    objp[5]  = (36 , 108, 0)
    objp[6]  = (36 , 180, 0)
    objp[7]  = (36 , 252, 0)
    objp[8]  = (72 , 0  , 0)
    objp[9]  = (72 , 72 , 0)
    objp[10] = (72 , 144, 0)
    objp[11] = (72 , 216, 0)
    objp[12] = (108, 36,  0)
    objp[13] = (108, 108, 0)
    objp[14] = (108, 180, 0)
    objp[15] = (108, 252, 0)
    objp[16] = (144, 0  , 0)
    objp[17] = (144, 72 , 0)
    objp[18] = (144, 144, 0)
    objp[19] = (144, 216, 0)
    objp[20] = (180, 36 , 0)
    objp[21] = (180, 108, 0)
    objp[22] = (180, 180, 0)
    objp[23] = (180, 252, 0)
    objp[24] = (216, 0  , 0)
    objp[25] = (216, 72 , 0)
    objp[26] = (216, 144, 0)
    objp[27] = (216, 216, 0)
    objp[28] = (252, 36 , 0)
    objp[29] = (252, 108, 0)
    objp[30] = (252, 180, 0)
    objp[31] = (252, 252, 0)
    objp[32] = (288, 0  , 0)
    objp[33] = (288, 72 , 0)
    objp[34] = (288, 144, 0)
    objp[35] = (288, 216, 0)
    objp[36] = (324, 36 , 0)
    objp[37] = (324, 108, 0)
    objp[38] = (324, 180, 0)
    objp[39] = (324, 252, 0)
    objp[40] = (360, 0  , 0)
    objp[41] = (360, 72 , 0)
    objp[42] = (360, 144, 0)
    objp[43] = (360, 216, 0)


    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    images = pathlib.Path(dir_path).glob(f'*.bmp')
    # Iterate through all images
    for fname in images:
        img = cv2.imread(str(fname))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        # ret, corners = cv2.findChessboardCorners(gray, (width, height), None)
        ret, corners = cv2.findCirclesGrid(gray, (width, height), flags = cv2.CALIB_CB_ASYMMETRIC_GRID)
        # If found, add object points, image points (after refining them)
        if ret:
            corners2 = corners
            # corners2 = cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001))
            cv2.drawChessboardCorners(img, (4,11), corners2, ret)
            cv2.imwrite('output.jpg', img)
            # cv2.imshow('img', img)
            # cv2.waitKey(0)
            objpoints.append(objp)

            # corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            imgpoints.append(corners2)

    # Calibrate camera
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    return [ret, mtx, dist, rvecs, tvecs]

def save_coefficients(mtx, dist, path):
    '''Save the camera matrix and the distortion coefficients to given path/file.'''
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_WRITE)
    cv_file.write('K', mtx)
    cv_file.write('D', dist)
    # note you *release* you don't close() a FileStorage object
    cv_file.release()

def load_coefficients(path):
    '''Loads camera matrix and distortion coefficients.'''
    # FILE_STORAGE_READ
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)

    # note we also have to specify the type to retrieve other wise we only get a
    # FileNode object back instead of a matrix
    camera_matrix = cv_file.getNode('K').mat()
    dist_matrix = cv_file.getNode('D').mat()

    cv_file.release()
    return [camera_matrix, dist_matrix]

# Parameters
IMAGES_DIR = 'E:\GitLab\python_scripts\CGH\check'
IMAGES_FORMAT = '.bmp'
SQUARE_SIZE = 0.02
WIDTH = 4
HEIGHT = 11

# Calibrate 
ret, mtx, dist, rvecs, tvecs = calibrate_chessboard(
    IMAGES_DIR, 
    IMAGES_FORMAT, 
    SQUARE_SIZE, 
    WIDTH, 
    HEIGHT
)
# Save coefficients into a file
save_coefficients(mtx, dist, "calibration_chessboard.yml")

# Load coefficients
mtx, dist = load_coefficients('calibration_chessboard.yml')
original = cv2.imread('E:\GitLab\python_scripts\CGH\check\DisplayingGridShowingPincusionDistortion.bmp')
dst = cv2.undistort(original, mtx, dist, None, None)
cv2.imwrite('undist.jpg', dst)