
# -*- coding: utf-8 -*-
"""
Python Script to compute the MSE from a target and a measured image
"""

# import initExample ## Add path to library (just for examples; you do not need this)

# from PyQt5.QtWidgets import QDesktopWidget
# from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
# from scipy.fft import fft2, fftshift
# import scipy as sp
# import pyqtgraph as pg
# import pyqtgraph.opengl as gl
from PIL import Image



# Computes a scalar MSE value from a target and a measured image/array
# Both should be numpy arrays of the same size
def calc_mse_whole_image(measured_arr, target_arr):

    # Basic MSE calculation
    number_elements = measured_arr.size
    candidate_mean_squared_error = np.sum( (target_arr - measured_arr)**2)   * (1.0/number_elements)
    return candidate_mean_squared_error



# Computes a scalar MSE value from a target and a measured image/array with a given ROI
# Both images should be numpy arrays of the same size
# ROI entry is the same as the ImageDraw.rectangle SPI (https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html#functions)
# Second point is specified just outside the drawn rectangle - which is great as we can just use normal python slice notation
#    eg: xy = [ x0, y0, x1, x2]
def calc_mse_with_roi(measured_arr, target_arr, xy):

    # First we build out Mask for our ROI
    # True is invalid, False is valid for a masked array - https://numpy.org/doc/stable/reference/maskedarray.generic.html#what-is-a-masked-array
    VALID   = False
    # Get size from our target array
    x_pixels = target_arr.shape[0]
    y_pixels = target_arr.shape[1]
    # Create out mask - all is False by default
    roi_mask = np.ones( [x_pixels, y_pixels], dtype=np.bool )
    # Set Valid bits in our ROI Mask - we can just use normal python slice notation as the second element is non-inclusive
    roi_mask[ xy[0]:xy[2], xy[1]:xy[3] ] = VALID

    # Create Masked Arrays
    measured_arr_masked = np.ma.masked_array(measured_arr, mask=roi_mask)
    target_arr_masked   = np.ma.array(target_arr,          mask=roi_mask)

    # Run Basic MSE calculation with Masked Arrays
    number_elements = target_arr_masked.count()
    candidate_mean_squared_error = np.sum( (target_arr_masked - measured_arr_masked)**2)   * (1.0/number_elements)
    return candidate_mean_squared_error
