
import numpy as np
import cv2, PIL
# Use the aruco library to give us nice 2D barcodes for calibration - might need to do pip install opencv-contrib
from cv2 import aruco, findChessboardCorners



# import cv2, PIL
# from cv2 import aruco
import matplotlib.pyplot as plt
# import matplotlib as mpl
# import pandas as pd

# My own code
import alg.gs as gs
import control.hardware as hardware
import util.gen_header as gen_header

# We are going to use a 640x640 test image
REPLAY_PIXELS_X      = 640
REPLAY_PIXELS_Y      = 640
SINGLE_CHANNEL_COLOUR = 1
# Our test images shall be 16 pixels wide in total, so 4 per pixel
BARCODE_2D_PIXEL_WIDTH = 64
# We don't detect our barcodes if we are right at the edges, so need to add an offset
EDGE_OFFSET            = 4
# SLM Pixels
SLM_NATIVE_PIXELS_X   = 1280
SLM_NATIVE_PIXELS_Y   = 1280






###############################################################################
############################ Helper Functions #################################
###############################################################################

# Generate our Test Grid using the ARUCO OpenCV Library
def generate_aruco_test_grid():

    # #########################################################################
    # ############################ Single ARUCO Images ########################
    # #########################################################################
    # # First thing we do is build up our test image to be used for calibration
    # # Test image shall be populated with 
    # test_me_img = np.ones( shape=(REPLAY_PIXELS_X, REPLAY_PIXELS_Y), dtype=np.uint8 )
    # test_me_img = test_me_img*255
    # # We only use 4 images, hence keep things simple and use the 4x4 pixel dataset with only 50 images - https://docs.opencv.org/4.5.2/d9/d6a/group__aruco.html
    # aruco_dict = aruco.Dictionary_get( aruco.DICT_4X4_100 )
    # # Generate our markers, we use the first 4
    # marker_top_left  = aruco.drawMarker(aruco_dict, 0, BARCODE_2D_PIXEL_WIDTH)
    # marker_top_right = aruco.drawMarker(aruco_dict, 1, BARCODE_2D_PIXEL_WIDTH)
    # marker_bot_left  = aruco.drawMarker(aruco_dict, 2, BARCODE_2D_PIXEL_WIDTH)
    # marker_bot_right = aruco.drawMarker(aruco_dict, 3, BARCODE_2D_PIXEL_WIDTH)
    # # Copy them into our test image
    # test_me_img[                         EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET,                            EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET ] = marker_top_left
    # test_me_img[ -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET,                                                  EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET ] = marker_top_right
    # test_me_img[                         EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET,    -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET                       ] = marker_bot_left
    # test_me_img[ -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET,                          -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET                       ] = marker_bot_right
    # cv2.imshow("test_output", test_me_img)
    # # cv2.waitKey(0) 
    # # Now detect our markers
    # # gray = cv2.cvtColor(test_me_img, cv2.COLOR_BGR2GRAY)
    # parameters =  aruco.DetectorParameters_create()
    # corners, ids, rejectedImgPoints = aruco.detectMarkers(test_me_img, aruco_dict, parameters=parameters)
    # # Now draw them back
    # test_me_img_colour = cv2.cvtColor(test_me_img, cv2.COLOR_GRAY2BGR)
    # frame_markers = aruco.drawDetectedMarkers(test_me_img_colour.copy(), corners, ids)
    # cv2.imshow("marked_up", frame_markers)
    # # cv2.waitKey(0) 
    # # plt.figure()
    # # plt.imshow(frame_markers)
    # # plt.legend()
    # # plt.show()
    # plt.figure()
    # plt.imshow(frame_markers, origin = "upper")
    # if ids is not None:
    #     for i in range(len(ids)):
    #         c = corners[i][0]
    #         plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "+", label = "id={0}".format(ids[i]))
    # for points in rejectedImgPoints:
    #     y = points[:, 0]
    #     x = points[:, 1]
    #     plt.plot(x, y, ".m-", linewidth = 1.)
    # plt.legend()
    # plt.show()




    #########################################################################
    ############################### Entire Board ############################
    #########################################################################
    # First thing we do is build up our test image to be used for calibration
    # Test image shall be populated with 
    test_me_img = np.ones( shape=(REPLAY_PIXELS_X, REPLAY_PIXELS_Y), dtype=np.uint8 )
    test_me_img = test_me_img*255
    # # We only use 4 images, hence keep things simple and use the 4x4 pixel dataset with only 50 images - https://docs.opencv.org/4.5.2/d9/d6a/group__aruco.html
    # aruco_dict = aruco.Dictionary_get( aruco.DICT_4X4_100 )
    # # Generate our markers, we use the first 4
    # marker_top_left  = aruco.drawMarker(aruco_dict, 0, BARCODE_2D_PIXEL_WIDTH)
    # marker_top_right = aruco.drawMarker(aruco_dict, 1, BARCODE_2D_PIXEL_WIDTH)
    # marker_bot_left  = aruco.drawMarker(aruco_dict, 2, BARCODE_2D_PIXEL_WIDTH)
    # marker_bot_right = aruco.drawMarker(aruco_dict, 3, BARCODE_2D_PIXEL_WIDTH)
    # # Copy them into our test image
    # test_me_img[                         EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET,                            EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET ] = marker_top_left
    # test_me_img[ -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET,                                                  EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET ] = marker_top_right
    # test_me_img[                         EDGE_OFFSET:BARCODE_2D_PIXEL_WIDTH+EDGE_OFFSET,    -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET                       ] = marker_bot_left
    # test_me_img[ -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET,                          -BARCODE_2D_PIXEL_WIDTH-EDGE_OFFSET:-EDGE_OFFSET                       ] = marker_bot_right
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    # board = aruco.CharucoBoard_create(7, 5, 1, .8, aruco_dict)
    # board = aruco.CharucoBoard_create(5, 5, 1, .8, aruco_dict)
    board = aruco.CharucoBoard_create(4, 4, 1, .8, aruco_dict)
    test_me_img = board.draw((REPLAY_PIXELS_X, REPLAY_PIXELS_Y))

    # cv2.imshow("test_output", test_me_img)
    # cv2.waitKey(0) 
    # Now detect our markers
    # gray = cv2.cvtColor(test_me_img, cv2.COLOR_BGR2GRAY)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(test_me_img, aruco_dict, parameters=parameters)
    # Now draw them back
    test_me_img_colour = cv2.cvtColor(test_me_img, cv2.COLOR_GRAY2BGR)
    frame_markers = aruco.drawDetectedMarkers(test_me_img_colour.copy(), corners, ids)
    # cv2.imshow("marked_up", frame_markers)
    # cv2.waitKey(0) 
    # plt.figure()
    # plt.imshow(frame_markers)
    # plt.legend()
    # plt.show()
    # plt.figure()
    # plt.imshow(frame_markers, origin = "upper")
    # if ids is not None:
    #     for i in range(len(ids)):
    #         c = corners[i][0]
    #         plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "+", label = "id={0}".format(ids[i]))
    # for points in rejectedImgPoints:
    #     y = points[:, 0]
    #     x = points[:, 1]
    #     plt.plot(x, y, ".m-", linewidth = 1.)
    # plt.legend()
    # plt.show()

    return ~test_me_img, aruco_dict

    # # markerLength = 40   # Here, our measurement unit is centimetre.
    # # markerSeparation = 8   # Here, our measurement unit is centimetre.
    # # board = aruco.GridBoard_create(5, 7, markerLength, markerSeparation, aruco_dict)

    # arucoParams = aruco.DetectorParameters_create()


    

    # # aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)

    # fig = plt.figure()
    # nx = 4
    # ny = 3
    # # for i in range(1, nx*ny+1):
    # ax = fig.add_subplot( 2, 2, 1)
    # img = aruco.drawMarker(aruco_dict, 0, 2D_BARCODE_PER_PIXEL_WIDTH)
    # plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
    # ax = fig.add_subplot( 2, 2, 2)
    # img = aruco.drawMarker(aruco_dict, 1, 1280)
    # plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
    # ax = fig.add_subplot( 2, 2, 3)
    # img = aruco.drawMarker(aruco_dict, 2, 1280)
    # plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
    # ax = fig.add_subplot( 2, 2, 4)
    # img = aruco.drawMarker(aruco_dict, 3, 1280)
    # plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
    # ax.axis("off")

    # # plt.savefig("_data/markers.pdf")
    # plt.show()

    # # convert to X,Y,Z
    # new_corners = np.zeros(shape=(len(corners),4,3))
    # for cnt,corner in enumerate(corners):
    #     new_corners[cnt,:,:-1] = corner

    # # try to create a board via Board_create
    # aruco.Board_create(new_corners,aruco_dict,ids)















def run_startup_calibration():

    # Tell the user we are starting a calibration
    print("Running Start-Up Calibration....")

    # Generate our Grid
    target_buf_uint8 = cv2.imread("./input/calib/culled_chessboard_7x7.png", cv2.IMREAD_GRAYSCALE)
    target_buf_uint8 = cv2.resize(target_buf_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_CUBIC) # Use Cubic interpolation as okay to have intemediate values


    # [target_buf_uint8, aruco_dict] = generate_aruco_test_grid()
    # target_buf_uint8 = cv2.resize(target_buf_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_CUBIC) # Use Cubic interpolation as okay to have intemediate values

    # Generate a CGH for our AR Code Grid
    [hologram_array_complex, replay_array_complex, target_img_padded_uint8] = gs.run_cgh_gs_passin_buffer(target_buf_uint8) 
    # Our binary-phase hologram array is complex numbers of {1j, -1j}
    # We convert it to a binary 8-bit bitmap to show and display
    slm_display_buf_uint8 = gen_header.conv_complex_phase_bitmap_to_8_bit_binary_array(hologram_array_complex)
    # Ensure that our bitmap is the same resolution as our SLM
    slm_display_buf_uint8 = cv2.resize(slm_display_buf_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_NEAREST) # Use Cubic interpolation as okay to have intemediate values

    # Display it on our SLM and Capture the Result
    camera_output_intensity_buf_uint8 = hardware.update_slm_and_capture_output(slm_display_buf_uint8)    

    # Show our results
    cv2.imshow("RawCameraOutput", camera_output_intensity_buf_uint8)


    # Clean up our captured image
    # camera_output_intensity_inv_buf_uint8 = ~camera_output_intensity_buf_uint8
    gaussian_blurred_replay = cv2.GaussianBlur(camera_output_intensity_buf_uint8, (31, 31), cv2.BORDER_DEFAULT)
    cv2.imshow("GaussianBlurred", gaussian_blurred_replay)

    # Try to detect our AR Code Grid
    corners = [0]
    did_we_find_our_grid = findChessboardCorners(target_buf_uint8, (6, 6), cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

    did_we = did_we_find_our_grid


    # # cv2.imshow("test_output", test_me_img)
    # # cv2.waitKey(0) 
    # # Now detect our markers
    # # gray = cv2.cvtColor(test_me_img, cv2.COLOR_BGR2GRAY)
    # parameters =  aruco.DetectorParameters_create()
    # corners, ids, rejectedImgPoints = aruco.detectMarkers(gaussian_blurred_replay, aruco_dict, parameters=parameters)
    # # Now draw them back
    # test_me_img_colour = cv2.cvtColor(camera_output_intensity_buf_uint8, cv2.COLOR_GRAY2BGR)
    # frame_markers = aruco.drawDetectedMarkers(test_me_img_colour.copy(), corners, ids)
    # cv2.imshow("marked_up", frame_markers)
    # # cv2.waitKey(0) 
    # # plt.figure()
    # # plt.imshow(frame_markers)
    # # plt.legend()
    # # plt.show()
    # plt.figure()
    # plt.imshow(frame_markers, origin = "upper")
    # if ids is not None:
    #     for i in range(len(ids)):
    #         c = corners[i][0]
    #         plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "+", label = "id={0}".format(ids[i]))
    # for points in rejectedImgPoints:
    #     y = points[:, 0]
    #     x = points[:, 1]
    #     plt.plot(x, y, ".m-", linewidth = 1.)
    # plt.legend()
    # plt.show()








