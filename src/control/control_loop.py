# File for main control loop for binary-camera-in-the-loop feedback system


# Imports
# Libraries
import time
import numpy as np
# from PIL import Image, ImageOps
import cv2
from PyQt5.QtWidgets import QDesktopWidget
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

# My own code
import control.simple_controller as controller
import control.fourier_system as system
import control.hardware as hardware
import control.logger as logger
import control.calibration as calibration
# import slm.driver as slm
# Special syntax to import from 'cousin' directories
# from ..slm.driver import GetInfoFromStringDescriptor
# from ..camera.driver import driver as cam
# import os
# cwd = os.getcwd()

# Set our Target
target_image_location = "./input/" + "smiley_face_inv.png"







# Run-Forever Control Loop
def run_forever_loop():


    # Constants
    SLM_NATIVE_PIXELS_X  = 1280
    SLM_NATIVE_PIXELS_Y  = 1280
    SLM_MACRO_PIXELS_X   = 1280
    SLM_MACRO_PIXELS_Y   = 1280
    REPLAY_PIXELS_X      = 256
    REPLAY_PIXELS_Y      = 256
    SINGLE_CHANNEL_COLOUR = 1
    # How many elements have we got?
    element_count_slm    = SLM_MACRO_PIXELS_X*SLM_MACRO_PIXELS_Y
    element_count_replay = REPLAY_PIXELS_X*REPLAY_PIXELS_Y


    # All of our various data structures which shall be pased around between functions
    # Store them as numpy arrays to keep things simple to keep track of (only 1 copy of the truth)
    # Use the util functions to generate images as required
    target_buf_uint8                         = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    target_buf_padded_uint8                  = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    error_buf_uint8                          = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    # error_corr_mask_img_uint8                = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), dtype=np.uint8 ) 
    # compensated_target_img_uint8             = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), dtype=np.uint8 )
    slm_display_buf_uint8                    = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    camera_output_intensity_buf_uint8        = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    simulated_output_intensity_buf_uint8     = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )
    simulated_output_phase_buf_uint8         = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y, SINGLE_CHANNEL_COLOUR), dtype=np.uint8 )

    # Load our target
    target_img_raw_uint8 = cv2.imread(target_image_location, cv2.IMREAD_GRAYSCALE )
    # Resize for our SLM
    target_buf_uint8 = cv2.resize(target_img_raw_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_CUBIC) # Use Cubic interpolation as okay to have intemediate values
    # target_img_float32 = target_img_float32.resize((SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y))
    # Convert our Image into an array
    # target_buf_float32 = np.asarray(target_img_float32)







    # Initialisation - call as required before main-loop
    # Call our Hardware first as its the most likely thing to fail
    hardware.init()

    # Next we need to run our calibration
    target_buf_uint8 = calibration.run_startup_calibration()
    target_buf_uint8 = cv2.resize(target_buf_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_CUBIC) # Use Cubic interpolation as okay to have intemediate values


    # Need to intialise the logging to delete old data + create new directories - call it last in-case any of the above failed 
    # Don't want to wipe data prematurely
    logger.init()





    # Big loop runs until:
    #   - terminated with an 'X' command
    #   - reaches prescribed number of iterations
    NUM_CITL_ITERATIONS = 1000000
    for i in range(NUM_CITL_ITERATIONS):
    # while True:

        # First step is to calculate the error signal
        #   ie: negative feedback output term from the target_img and 
        error_buf_uint8 = target_buf_uint8 - camera_output_intensity_buf_uint8 

        # Now, feed these control signals into my controller 
        [slm_display_buf_uint8, target_buf_padded_uint8] = controller.run_controller_iteration(target_buf_uint8, error_buf_uint8)

        # Now, feed the output into the system simulation to caluculate 
        system.run_system_iteration(slm_display_buf_uint8, simulated_output_intensity_buf_uint8, simulated_output_phase_buf_uint8)

        # Finally, actuate the system by updating the SLM and capturing an image
        camera_output_intensity_buf_uint8 = hardware.update_slm_and_capture_output(slm_display_buf_uint8)

        # Save our images to disk from each loop
        logger.save_images_to_disk(i, target_buf_uint8, target_buf_padded_uint8, error_buf_uint8, slm_display_buf_uint8, camera_output_intensity_buf_uint8, simulated_output_intensity_buf_uint8, simulated_output_phase_buf_uint8)



    # ##### BENCHMARK #####
    # point_start = time.time()


    # # First we try to flip some bits to compute a candidate improved solution
    # # How many pixels are we going to flip?
    # number_of_pixels_to_flip = number_of_pixels_to_change
    # # Convert out array to 1D temporarily for flipping
    # candid_hologram_array    = np.copy(hologram_array)
    # candid_hologram_array_1D = candid_hologram_array.reshape(-1)
    # # Generate a mask of pixels indices to modify - this is just a simple 1D vector
    # # mask = np.random.randint(0, element_count, size=number_of_pixels_to_flip)
    # mask = np.random.choice(index_lookup_array, number_of_pixels_to_flip, replace=False)
    # # print(mask)
    # # Use the mask to flip our bits
    # # candid_hologram_array_1D[mask] = np.logical_not(candid_hologram_array_1D[mask])
    # candid_hologram_array_1D[mask] = (candid_hologram_array_1D[mask] + 1) % 2
    # # selected_values[:] = 3
    # # Convert back to 2D and update for next iteration
    # candid_hologram_array = candid_hologram_array_1D.reshape(SLM_MACRO_PIXELS_X, SLM_MACRO_PIXELS_Y)

    # ##### BENCHMARK #####
    # point_A = time.time()







    # # # Update our SLM frame
    # # # slm.update_frame("smiley_face_mini_test.png")
    # # # slm.update_frame_image("romeo_gs_2.bmp")
    # # # slm.update_frame_image("smiley_face_gs_200.bmp")
    # # # If we are using SLM macropixels, then need to rescale for this
    # # candid_hologram_array_img = Image.fromarray(candid_hologram_array)
    # # candid_hologram_array_img = Image.fromarray(hologram_array)
    # # candid_hologram_array_img_resized = candid_hologram_array_img.resize((SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y))
    # # candid_hologram_array_native = np.asarray(candid_hologram_array_img_resized)
    # # # # Update Frame
    # # slm.update_frame_buffer(candid_hologram_array_native)








    # # # # Get our next camera picture
    # # camera_img = cam.grab_imag()
    # # replay_field_image = np.copy(Image.fromarray(camera_img, 'L'))
    # # # replay_field_image = 0
    # # # np.copy(
    # # # replay_field_image.show()









    # # Update our SLM frame
    # # slm.update_frame_image("smiley_face_gs_200.bmp")
    # # If we are using SLM macropixels, then need to rescale for this
    # candid_hologram_array_img = Image.fromarray(candid_hologram_array)
    # # candid_hologram_array_img = Image.fromarray(hologram_array)
    # candid_hologram_array_img_resized = candid_hologram_array_img.resize((SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y))
    # candid_hologram_array_native = np.asarray(candid_hologram_array_img_resized)
    # # # Update Frame
    # slm.update_frame_buffer(candid_hologram_array_native)

    # # Show what we sent
    # holA_view.show()
    # holA_view.setImage(candid_hologram_array)

    # # Buffer Swap
    # slm.spi_show_buf_A()

    # # Print out our SLM status
    # # slm.check_status()

    # # Get our next camera picture
    # camera_imgA = cam.grab_imag()
    # replay_field_imageA = np.copy(Image.fromarray(camera_imgA, 'L'))
    # # Update it on our QT GUI
    # bufA_view.show()
    # bufA_view.setImage(replay_field_imageA)

    # ##### BENCHMARK #####
    # point_B = time.time()







    # # Update our SLM frame
    # # slm.update_frame("smiley_face_mini_test.png")
    # # slm.update_frame_image("romeo_gs_2.bmp")
    # # # Update Frame
    # inverted_candid_hologram_array_native = (candid_hologram_array_native + 1) % 2
    # slm.update_frame_buffer(inverted_candid_hologram_array_native)

    # # Buffer Swap
    # slm.spi_show_buf_B()

    # # Print out our SLM status
    # # slm.check_status()

    # # Get our next camera picture
    # camera_imgB = cam.grab_imag()
    # replay_field_imageB = np.copy(Image.fromarray(camera_imgB, 'L'))
    # # Update it on our QT GUI
    # bufB_view.show()
    # bufB_view.setImage(replay_field_imageB)

    # ##### BENCHMARK #####
    # point_C = time.time()










    # # Before we set our MSE, set set any pixel values below our noise floor to 0
    # # NOISE_FLOOR_THRESHOLD = 80#15#30 # Note that this is determined experimentally and depends on ambient light conditions
    # # replay_field_image_filtered = np.where( replay_field_imageA<NOISE_FLOOR_THRESHOLD, 0, replay_field_imageA)
    # # # Normalise our replay field to the power input
    # # replay_field_power = np.sum(replay_field_imageA)
    # # power_scaling_factor = target_array_power/replay_field_power
    # # # Now scale our results with this power
    # # replay_field_image_filtered = replay_field_imageA * power_scaling_factor
    # # # Clip any value above 255 - effectively saturated
    # # replay_field_image_filtered = np.where( replay_field_image_filtered>255, 255, replay_field_imageA)
    # # after_target = np.sum(target_array)
    # # after_replay = np.sum(replay_field_image_filtered)
    # # ratio = after_target/after_replay

    # # Equalize our Image
    # # replay_field_imageA_Img     = Image.fromarray(replay_field_imageA)
    # # replay_field_imageA_Img_Eg  = ImageOps.equalize(replay_field_imageA_Img)
    # # replay_field_image_filtered = np.asarray(replay_field_imageA_Img_Eg)
    # replay_field_image_filtered = replay_field_imageA


    # # Show our filtered value
    # holB_view.show()
    # holB_view.setImage(replay_field_image_filtered)

    # # Compute our MSE
    # candidate_mean_squared_error = np.sum( (target_array - replay_field_image_filtered)**2)   * (1.0/element_count_replay)
    # time_update = (time.time() - start)
    # # print("Iter Number: ", i, " Candidate MSE: ", candidate_mean_squared_error, "Runtime: ", time_update, " Candidate MSE: ", candidate_mean_squared_error)
    # # print(candidate_mean_squared_error)


    # ##### BENCHMARK #####
    # point_D = time.time()



    # # If we have an imporovement, then update
    # if( candidate_mean_squared_error < hologram_mse):
    #     hologram_array      = candid_hologram_array
    #     hologram_mse        = candidate_mean_squared_error
    #     replay_array        = replay_field_image_filtered
    #     # Got an improvement, so print this
    #     time_update = (time.time() - start)
    #     print("IMPROVEMENT - Iter Number: ", i, " Updated MSE: ", hologram_mse, "Runtime: ", time_update)



    # # Every 500th iteration, we check if we need to reduce the number of pixels we are flipping
    # if( (i%5000)==0 ):
    #     # Drop to 90 percent of what we were doing rpeviously
    #     number_of_pixels_to_change = (np.int)(number_of_pixels_to_change * 0.9)
    #     print("Updating Number Pixels Flipped: ", number_of_pixels_to_change, " pixels")
    #     # If the MSE hasn't changed in 500 attempts, then we probably ain't getting too many more improvements at this level, flip less pixels
    #     # if(hologram_mse==sliding_scale_mse):
    #     #     # Drop to 80 percent of what we were doing rpeviously
    #     #     number_of_pixels_to_change = (np.int)(number_of_pixels_to_change * 0.95)
    #     #     print("Updating Number Pixels Flipped: ", number_of_pixels_to_change, " pixels")
    #     #     # Never let it go below 1 pixels
    #     #     if( number_of_pixels_to_change<=1 ):
    #     #         number_of_pixels_to_change = 1
    #     # # Update for subsequent check
    #     # sliding_scale_mse = hologram_mse


    # # Print our Replay Field every 1000th iteration
    # if( (i%100)==0 ):
    # # if( True ):
    #     # Image.fromarray(candid_replay_array).show()
    #     # Rescale such that our max value corresponds to 255.0
    #     # replay_field = np.abs(replay_field_image)
    #     # # replay_field = np.abs(target_array)
    #     # replay_field_rescaled = replay_field * 255.0/np.max(replay_field)
    #     # # Now plot the image
    #     # Image.fromarray(replay_field_rescaled).show()
    #     # Also give an update
    #     time_update = (time.time() - start)
    #     print("HEARTBEAT   - Iter Number: ", i, " Updated MSE: ", candidate_mean_squared_error, "Runtime: ", time_update)



    # # Housekeeping at the end of the loop
    # QtGui.QApplication.processEvents()    # you MUST process the plot now

    # # Timing stuff
    # ##### BENCHMARK #####
    # point_end = time.time()
    # total_iter_time_ms  = point_end - point_start
    # part_a_iter_time_ms = point_A   - point_start
    # part_b_iter_time_ms = point_B   - point_A
    # part_c_iter_time_ms = point_C   - point_B
    # part_d_iter_time_ms = point_D   - point_C
    # end_iter_time_ms    = point_end - point_D
    # # print("Total Time: ", total_iter_time_ms, " Part A: ", part_a_iter_time_ms, "Part B: ", part_b_iter_time_ms, "Part C: ", part_c_iter_time_ms, "Part D: ", part_d_iter_time_ms, "End: ", end_iter_time_ms)


    # # Housekeeping at the end of the loop
    # # print("Still Alive...")
    # # time.sleep(2.0)