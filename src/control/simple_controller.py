# Simple Controller that takes a target image as an input, runs G-S and spits returns a SLM bitmap to send to the SLM

# For camera
from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np
import copy
import cv2

# Our own Code
# .alg.gs import gs
import alg.gs as gs
import util.gen_header as gen_header

# Constants
MAX_IMG_VALUE = 255.0
SLM_NATIVE_PIXELS_X = 1280
SLM_NATIVE_PIXELS_Y = 1280

# Controller init Function
def init():
    # Init code goes here
    do_nothing = 0

def run_controller_iteration(target_buf_uint8, error_buf_uint8):
    
    # Zero the data structure to return
    slm_display_buf_uint8 = np.zeros( shape=(SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), dtype=np.uint8 )

    # Take our target and perform CGH
    [hologram_array_complex, replay_array_complex, target_img_padded_uint8] = gs.run_cgh_gs_passin_buffer(target_buf_uint8)

    # Convert our simulation-deduced replay field magnitude and phase into images, scaled such that the maximum values corresponds to 255
    replay_field_mag_rescaled    = np.abs(replay_array_complex)   * MAX_IMG_VALUE/np.max(np.abs(replay_array_complex))
    replay_field_phase_rescaled  = np.angle(replay_array_complex) * MAX_IMG_VALUE/np.max(np.angle(replay_array_complex))

    # Our binary-phase hologram array is complex numbers of {1j, -1j}
    # We convert it to a binary 8-bit bitmap to show and display
    slm_display_buf_uint8 = gen_header.conv_complex_phase_bitmap_to_8_bit_binary_array(hologram_array_complex)
    # Ensure that our bitmap is the same resolution as our SLM
    slm_display_buf_uint8 = cv2.resize(slm_display_buf_uint8, (SLM_NATIVE_PIXELS_X, SLM_NATIVE_PIXELS_Y), interpolation=cv2.INTER_NEAREST) # Use Cubic interpolation as okay to have intemediate values
    # And return
    return slm_display_buf_uint8, target_img_padded_uint8


