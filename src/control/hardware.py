# Function which actuates the system by updating the SLM and capturing an image

# For camera
from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np
import copy
import cv2
import slm.driver as slm
import camera.driver as camera

# Hardware init Function
def init():
    # Init code for Hardware goes here
    # Init our SLM
    slm.init()
    # Set our SLM to only update over SPI commands
    slm.enter_spi_update()
    # Init our Camera - this sets our parameters explicity such as ROI, exposure time, etc...
    camera.init()

def update_slm_and_capture_output(slm_display_buf_uint8):

    # Setup out Data Structures
    # We have two buffers to output, Buffer A and Buffer B
    slm_display_bufA_uint8 = np.zeros( shape=(1280, 1280), dtype=np.uint8 )
    slm_display_bufB_uint8 = np.zeros( shape=(1280, 1280), dtype=np.uint8 )
    # We also take 2 images 
    # Note that we only ever use 1 but take 2 to ensure we get DC Balance
    img_capture1_uint8 = np.zeros( shape=(1280, 1280), dtype=np.uint8 )
    img_capture2_uint8 = np.zeros( shape=(1280, 1280), dtype=np.uint8 )

    # Now, load-up our buffers, we use deep-copy to avoid any corruption issues
    slm_display_bufA_uint8   = copy.deepcopy(slm_display_buf_uint8)
    slm_display_bufB_uint8   = ~slm_display_bufA_uint8 # Buffer B is complement for DC-Balance of FLC crystals

    # Perform our First Frame Update + Capture
    # Update our SLM frame
    slm.update_frame_buffer(slm_display_bufA_uint8)
    # Get our next camera picture
    img_capture1_uint8 = camera.grab_imag()
    # replay_field_A_image = Image.fromarray(camera_img_A, 'L')
    # replay_field_A_array = np.copy(replay_field_A_image)
    # Buffer Swap to prime the next iamge
    slm.spi_show_buf_A()

    # Perform our Second Frame Update + Capture
    # Update our SLM frame
    slm.update_frame_buffer(slm_display_bufB_uint8)
    # Get our next camera picture
    img_capture2_uint8 = camera.grab_imag()
    # Scale our camera image to be same resolution as our SLM - not stricly necessary but we do it anyway
    img_capture2_uint8 = cv2.resize(img_capture2_uint8, (1280, 1280), interpolation=cv2.INTER_CUBIC) # Use nearest-neighbour to preserve values
    # replay_field_A_image = Image.fromarray(camera_img_A, 'L')
    # replay_field_A_array = np.copy(replay_field_A_image)
    # Buffer Swap to prime the next iamge
    slm.spi_show_buf_B()

    # Now, we return our second captured image
    # This gives us the image associated with the A buffer (they are offset by 1 because of the BufferSwitch)
    return img_capture2_uint8
