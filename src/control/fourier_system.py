# Fourier System that takes a SLM bitmap as an input and calculates the simulated output of phase and intensity
# Just models the output as a simple Fourier System

# For camera
# from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np

# Controller init Function
def init():
    # Init code goes here
    do_nothing = 0

def run_system_iteration(slm_display_buf_uint8, simulated_output_intensity_buf_uint8, simulated_output_phase_buf_uint8):
    # Data structure to return
    simulated_output_intensity_buf_uint8 = np.zeros( shape=(1280, 1280), dtype=np.uint8 )
    simulated_output_phase_buf_uint8     = np.zeros( shape=(1280, 1280), dtype=np.uint8 )
