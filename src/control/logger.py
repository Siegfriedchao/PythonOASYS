# Function which saves our images to disk after each loop

import cv2
import os
import shutil

# Locations to save to
# Root Dir
OUTPUT_ROOT_DIR                 = "./output/control_loop_output/"
# Separate folders for each output type
TARGET_OUTPUT_PATH              = OUTPUT_ROOT_DIR + "/target"
TARGET_PADDED_OUTPUT_PATH       = OUTPUT_ROOT_DIR + "/target_padded"
ERROR_OUTPUT_PATH               = OUTPUT_ROOT_DIR + "/error"
SLM_DISPLAY_OUTPUT_PATH         = OUTPUT_ROOT_DIR + "/slm_display"
CAMERA_OUTPUT_PATH              = OUTPUT_ROOT_DIR + "/camera_output"
SIMULATED_INTENSITY_OUTPUT_PATH = OUTPUT_ROOT_DIR + "/simulated_intensity"
SIMULATED_PHASE_OUTPUT_PATH     = OUTPUT_ROOT_DIR + "/simulated_phase"

    # # Create a temporary file on disk and then use our existing GS code
    # target_image_float = Image.fromarray(target_buf_uint8, 'L')
    # temp_image_parameter_path = "temp/temp_GS_input.bmp"
    # temp_image_full_path      = "../input/" + temp_image_parameter_path
    # # Remove any existing temp file first
    # os.remove(temp_image_full_path)
    # # Now save our Image
    # target_image_float.save(temp_image_full_path)

def init():
    # Delete the folders
    shutil.rmtree(OUTPUT_ROOT_DIR)
    # Create new/fresh folders for output
    os.makedirs(TARGET_OUTPUT_PATH)
    os.makedirs(TARGET_PADDED_OUTPUT_PATH)
    os.makedirs(ERROR_OUTPUT_PATH)
    os.makedirs(SLM_DISPLAY_OUTPUT_PATH)
    os.makedirs(CAMERA_OUTPUT_PATH)
    os.makedirs(SIMULATED_INTENSITY_OUTPUT_PATH)
    os.makedirs(SIMULATED_PHASE_OUTPUT_PATH)

def save_images_to_disk(i, target_buf_uint8, target_buf_padded_uint8, error_buf_uint8, slm_display_buf_uint8, camera_output_intensity_buf_uint8, simulated_output_intensity_buf_uint8, simulated_output_phase_buf_uint8):
    # Buildup our output paths to incorporate the index count
    target_output_file = TARGET_OUTPUT_PATH                           + "/target_"              + str(i) + ".bmp"
    target_padded_output_file = TARGET_PADDED_OUTPUT_PATH             + "/target_padded_"       + str(i) + ".bmp"
    error_output_file = ERROR_OUTPUT_PATH                             + "/error_"               + str(i) + ".bmp"
    slm_display_output_file = SLM_DISPLAY_OUTPUT_PATH                 + "/slm_display_"         + str(i) + ".bmp"
    camera_output_file = CAMERA_OUTPUT_PATH                           + "/camera_output_"       + str(i) + ".bmp"
    simulated_intensity_output_file = SIMULATED_INTENSITY_OUTPUT_PATH + "/simulated_intensity_" + str(i) + ".bmp"
    simulated_phase_output_file = SIMULATED_PHASE_OUTPUT_PATH         + "/simulated_phase_"     + str(i) + ".bmp"
    # Write to disk using the OpenCV infrastructure
    cv2.imwrite( target_output_file,              target_buf_uint8)
    cv2.imwrite( target_padded_output_file,       target_buf_padded_uint8)
    cv2.imwrite( error_output_file,               error_buf_uint8)
    cv2.imwrite( slm_display_output_file,         slm_display_buf_uint8)
    cv2.imwrite( camera_output_file,              camera_output_intensity_buf_uint8)
    cv2.imwrite( simulated_intensity_output_file, simulated_output_intensity_buf_uint8)
    cv2.imwrite( simulated_phase_output_file,     simulated_output_phase_buf_uint8)
    # Save all of these files to disk
    # do_nothing = 0
