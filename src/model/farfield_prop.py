
    
# -*- coding: utf-8 -*-
"""
Python Functions to calculate FarField (Fourier) Propagation
"""

import torch
import math
from util import util

import matplotlib.pyplot as plt  # This is python's popular plotting library.
# This is to ensure matplotlib plots inline and does not try to open a new window.
# %matplotlib inline  
# from torchvision import transforms.functional as F

# Compute what the replay field is from a given hologram given a complex tensor input of [1, W, H]
# Returns the simulated amplitude and phase wrapped up as a complex tensor
# Simply uses a Fourier Transform to derive results
def calc_replay_field(slm_plane_complex_tensor):

    # Plot the input First
    # util.plot_complex_tensor_both_cords(slm_plane_complex_tensor, "SLM Plane")

    # # Update our Replay Field by taking the fourier transform
    # replay_field_first_shift       = torch.fft.fftshift(slm_plane_complex_tensor)
    # replay_field_fourier_transform = torch.fft.fft2(replay_field_first_shift)
    # replay_field_second_shift      = torch.fft.fftshift(replay_field_fourier_transform)
    # # Re-normalise to conserve our energy
    # number_elements       = torch.numel(slm_plane_complex_tensor)
    # replay_field_complex  = (1/math.sqrt(number_elements)) * replay_field_second_shift

    # What's our input energy?
    (slm_plane_mag, slm_plane_ang) = util.conv_cartesian_to_polar_tensor(slm_plane_complex_tensor.real, slm_plane_complex_tensor.imag)
    input_energy = torch.sum(slm_plane_mag)

    # Update our Replay Field by taking the fourier transform
    replay_field_first_shift       = torch.fft.fftshift(slm_plane_complex_tensor)
    replay_field_fourier_transform = torch.fft.fft2(replay_field_first_shift)
    replay_field_second_shift      = torch.fft.fftshift(replay_field_fourier_transform)

    # Re-normalise to conserve our energy
    # (replay_plane_mag, replay_plane_ang) = util.conv_cartesian_to_polar_tensor(replay_field_second_shift.real, replay_field_second_shift.imag)
    # (replay_plane_mag, replay_plane_ang) = torch.polar(replay_field_second_shift.real, replay_field_second_shift.imag)#util.conv_cartesian_to_polar_tensor(replay_field_second_shift.real, replay_field_second_shift.imag)
    replay_plane_mag = torch.clone(replay_field_second_shift.abs())
    replay_plane_ang = torch.clone(replay_field_second_shift.angle())
    output_energy = torch.sum(replay_plane_mag.abs())
    energy_conserve_scaling = input_energy/output_energy
    # replay_field_complex  = (energy_conserve_scaling) * replay_field_second_shift
    replay_field_complex  = (energy_conserve_scaling) * torch.polar(replay_plane_mag, replay_plane_ang)#replay_field_second_shift

    # Plot the Output
    # util.plot_complex_tensor_both_cords(replay_field_complex, "Replay Field", plot_mag_log_10=True)

    # Return our complex tensor
    return replay_field_complex




# Siumilar version of the above which just takes phase as an input as-per a SLM
# Assumes that all the points are on the unit-circle with magnitude of 1 (which is what an SLM does)
# Returns the simulated amplitude and phase wrapped up as a complex tensor
# Simply uses a Fourier Transform to derive results
def calc_replay_field_for_SLM(slm_plane_phase):

    # All of our amplitudes simply lie on the unit-circle (which is what a SLM does)
    slm_plane_mag = torch.ones( (slm_plane_phase.shape),  dtype=float, requires_grad=True)
    # Load as a tensor using PyTorch's native complex-number infrastructure
    [slm_plane_real, slm_plane_imag] = util.conv_polar_to_cartesian_tensor(slm_plane_mag, slm_plane_phase)
    slm_plane_complex = torch.complex( slm_plane_real, slm_plane_imag )
    slm_plane_complex.requires_grad_(True)

    # Return our complex tensor using the above function with a complex-field input
    return calc_replay_field(slm_plane_complex)

