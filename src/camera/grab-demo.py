# For camera
from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np

### https://docs.baslerweb.com/aca1920-150um
### https://docs.baslerweb.com/trigger-source

camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
camera.Open()

camera.TriggerSource.SetValue("Line3") # Line 3 GPIO line, Pin 1 Blue
camera.TriggerMode.SetValue("On")

camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly)
grabResult = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)

# # demonstrate some feature access
# new_width = camera.Width.GetValue() - camera.Width.GetInc()
# if new_width >= camera.Width.GetMin():
#     camera.Width.SetValue(new_width)

# numberOfImagesToGrab = 100000
# camera.StartGrabbingMax(numberOfImagesToGrab)

# while camera.IsGrabbing():
#     grabResult = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)

#     if grabResult.GrabSucceeded():
#         # Access the image data.
#         print("SizeX: ", grabResult.Width)
#         print("SizeY: ", grabResult.Height)
#         img = grabResult.Array
#         print("Gray value of first pixel: ", img[0, 0])

#         # Show us our image
#         img = Image.fromarray(img, 'L')
#         img.show()

#     grabResult.Release()
camera.Close()



# print(grabResult)