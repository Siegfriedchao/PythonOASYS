# For camera
from itertools import count
from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np

import time

camera = 0

def init(trigger=False, width=1024, height=1024, offsetX=0, offsetY=0, exposure=8000):
# Updated camera init
    global camera

    camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
    camera.Open()

    # print("DeviceClass: ", camera.GetDeviceInfo().GetDeviceClass())
    # print("DeviceFactory: ", camera.GetDeviceInfo().GetDeviceFactory())
    # print("ModelName: ", camera.GetDeviceInfo().GetModelName())

    camera.PixelFormat.SetValue('Mono8')

    # Turn our Binning Off
    camera.BinningHorizontal.SetValue(1)
    camera.BinningVertical.SetValue(1)
    # Set our ROI
    # camera.Width.SetValue(1920)
    # camera.Height.SetValue(1280)
    # camera.OffsetX.SetValue(0)
    # camera.OffsetY.SetValue(0)
    # camera.Width.SetValue(1280)
    camera.Width.SetValue(width)
    camera.Height.SetValue(height)
    # camera.OffsetX.SetValue(544)
    # camera.OffsetY.SetValue(133)
    # camera.OffsetX.SetValue(480)
    # camera.OffsetY.SetValue(0)
    camera.OffsetX.SetValue(offsetX)
    camera.OffsetY.SetValue(offsetY)

    # # Set our exposure time
    # camera.ExposureTime.SetValue(7006.0) # plots used 7006 
    # camera.ExposureTime.SetValue(2000.0) # Jasper
    # camera.ExposureTime.SetValue(3000.0) # Optalysys
    # camera.ExposureTime.SetValue(13726.0)
    # camera.ExposureTime.SetValue(400.0) # Aurora with Blade, const power@2.000
    # camera.ExposureTime.SetValue(2500.0) # Aurora with Blade
    camera.ExposureTime.SetValue(exposure)

    # Setup hardware trigger
    if(trigger == True):
        camera.TriggerSource.SetValue("Line3") # We are using GPIO Line3
        camera.TriggerMode.SetValue("On")
        camera.TriggerActivation.SetValue('RisingEdge')
        # camera.TriggerDelay.SetValue(0)
        camera.TriggerDelay.SetValue(300.0)
        camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly)

    # Leave the camera open at the end so we are nice and fast
    # Close the camera.
    # camera.Close()

def set_trigger_delay(delay = 0):
    camera.TriggerSource.SetValue("Line3") # We are using GPIO Line3
    camera.TriggerMode.SetValue("On")
    camera.TriggerActivation.SetValue('RisingEdge')
    # camera.TriggerDelay.SetValue(0)
    camera.TriggerDelay.SetValue(delay)
    # camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly)

def grab_imag():

    global camera

    # camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
    # camera.Open()

    # # demonstrate some feature access
    # new_width = camera.Width.GetValue() - camera.Width.GetInc()
    # if new_width >= camera.Width.GetMin():
    #     camera.Width.SetValue(new_width)

    # numberOfImagesToGrab = 1
    # camera.GrabOne()
    # print('main thread',round(time.time() * 1000))
    t1 = round(time.time() * 1000)
    grabResult = camera.GrabOne(100, pylon.TimeoutHandling_ThrowException)
    # print('main thread',round(time.time() * 1000))
    t2 = round(time.time() * 1000)
    # print('main thread', t2 - t1)

    if grabResult.GrabSucceeded():
        # Access the image data.
        # print("SizeX: ", grabResult.Width)
        # print("SizeY: ", grabResult.Height)
        img = grabResult.Array
        # print("Gray value of first pixel: ", img[0, 0])
        # Show us our image
        # img = Image.fromarray(img, 'L')
        # img.show()
    grabResult.Release()
    # camera.Close()
    
    return img


def trig_imag(trigger_count=30):

    global camera

    count_trigger = 0
    while camera.IsGrabbing():
        # print("count_trigger: ", count_trigger)
        # if count_trigger == trigger_count - 1:
        #     print('pool thread',round(time.time() * 1000))

        grabResult = camera.RetrieveResult(100, pylon.TimeoutHandling_ThrowException)
        count_trigger += 1
        if count_trigger == trigger_count:
            count_trigger = 0
            # print("grabbing")

            break

    # grabResult = camera.RetrieveResult(100, pylon.TimeoutHandling_ThrowException)

    if grabResult.GrabSucceeded():
        # Access the image data.
        # print("SizeX: ", grabResult.Width)
        # print("SizeY: ", grabResult.Height)
        img = grabResult.Array
        # print("Gray value of first pixel: ", img[0, 0])
        # Show us our image
        # img = Image.fromarray(img, 'L')
        # img.show()
    grabResult.Release()
    # camera.Close()
    return img
