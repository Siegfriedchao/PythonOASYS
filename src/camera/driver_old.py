# For camera
from pypylon import pylon

# For displaying
from PIL import Image
import numpy as np

camera = 0

def init():

    global camera

    camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
    camera.Open()

    # # Set our ROI
    # # This is where we ahve put Romeo
    # camera.Width.SetValue(1088)
    # camera.Height.SetValue(1088)
    # camera.OffsetX.SetValue(192)
    # camera.OffsetY.SetValue(81)
    # # Set our exposure time
    # camera.ExposureTime.SetValue(1000.0) # 10ms

    # # Turn our Binning On
    # camera.BinningHorizontal.SetValue(4)
    # camera.BinningVertical.SetValue(4)


    # # Binneed Romeo!!!
    # # This is where we ahve put Romeo
    # camera.Width.SetValue(256)
    # camera.Height.SetValue(256)
    # camera.OffsetX.SetValue(64)
    # # camera.OffsetX.SetValue(0)
    # camera.OffsetY.SetValue(28)
    # # Set our exposure time
    # # camera.ExposureTime.SetValue(10000.0) # 10ms
    # camera.ExposureTime.SetValue(2005.0) # 1ms
    # # camera.ExposureTime.SetValue(000.0) # 0.1ms
    # # camera.ExposureTime.SetValue(4000.0) # 0.1ms
    # # camera.ExposureTime.SetValue(4000.0) # 0.1ms


    # Centre of the Replay Field
    # No Binning Option - 640x640
    # # Turn our Binning Off
    # camera.BinningHorizontal.SetValue(1)
    # camera.BinningVertical.SetValue(1)
    # # Set our ROI
    # camera.Width.SetValue(640)
    # camera.Height.SetValue(640)
    # camera.OffsetX.SetValue(896)
    # camera.OffsetY.SetValue(119)
    # # No Binning Option - 1920x1200
    # # Turn our Binning Off
    # camera.BinningHorizontal.SetValue(1)
    # camera.BinningVertical.SetValue(1)
    # # Set our ROI
    # camera.Width.SetValue(1920)
    # camera.Height.SetValue(1200)
    # # camera.OffsetX.SetValue(32)
    # # camera.OffsetY.SetValue(32)
    # # No Binning Option - 1920x1200
    # # Turn our Binning Off
    # camera.BinningHorizontal.SetValue(1)
    # camera.BinningVertical.SetValue(1)
    # # Set our ROI
    # camera.Width.SetValue(1216)
    # camera.Height.SetValue(1216)
    # camera.OffsetX.SetValue(608)
    # camera.OffsetY.SetValue(0)




    # # Iterative Optimisation Settings for RIO (Res + offest) + Binning
    # # These are tuned for a 60mm lens
    # # No Binning Option - 640x640
    # # # Turn our Binning Off
    # # camera.BinningHorizontal.SetValue(1)
    # # camera.BinningVertical.SetValue(1)
    # # # Set our ROI
    # # camera.Width.SetValue(640)
    # # camera.Height.SetValue(640)
    # # # camera.OffsetX.SetValue(896)
    # # # camera.OffsetY.SetValue(106)
    # # camera.OffsetX.SetValue(480)
    # # camera.OffsetY.SetValue(106)
    # # Binning Option - 640x640 binned at 4x to 128x128
    # # Set our ROI
    # camera.Width.SetValue(160)
    # camera.Height.SetValue(160)
    # camera.OffsetX.SetValue(608)
    # camera.OffsetY.SetValue(0)
    # # Turn our Binning On
    # camera.BinningHorizontal.SetValue(4)
    # camera.BinningVertical.SetValue(4)


    ##################################################################
    ############# 60mm Lens for quarter-alignment ####################
    ##################################################################
    # # Turn our Binning Off
    # camera.BinningHorizontal.SetValue(1)
    # camera.BinningVertical.SetValue(1)
    # # Binning Option - 640x640 binned at 4x to 160x160
    # camera.OffsetX.SetValue(160)
    # camera.OffsetY.SetValue(113)
    # # Set our ROI
    # camera.Width.SetValue(1472)
    # camera.Height.SetValue(996)
    # # camera.OffsetX.SetValue(96)
    # # camera.OffsetY.SetValue(32)
    # Turn our Binning Off
    camera.BinningHorizontal.SetValue(1)
    camera.BinningVertical.SetValue(1)
    # Set our ROI
    # Binning Option - 640x640 binned at 4x to 160x160
    camera.Width.SetValue(1984)
    camera.Height.SetValue(1264)
    camera.OffsetX.SetValue(0)
    camera.OffsetY.SetValue(0)
    # camera.OffsetX.SetValue(96)
    # camera.OffsetY.SetValue(32)


    # # Set our exposure time
    camera.ExposureTime.SetValue(1000.0) # 10ms
    # camera.ExposureTime.SetValue(200.0) # 10ms


    # Leave the camera open at the end so we are nice and fast

    # # Close the camera.
    # camera.Close()



def grab_imag():

    global camera

    # camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
    # camera.Open()

    # # demonstrate some feature access
    # new_width = camera.Width.GetValue() - camera.Width.GetInc()
    # if new_width >= camera.Width.GetMin():
    #     camera.Width.SetValue(new_width)

    # numberOfImagesToGrab = 1
    # camera.GrabOne()

    # while camera.IsGrabbing():

    grabResult = camera.GrabOne(100, pylon.TimeoutHandling_ThrowException)    
    # grabResult = camera.GrabOne(5000, pylon.TimeoutHandling_ThrowException)
    if grabResult.GrabSucceeded():
        # Access the image data.
        # print("SizeX: ", grabResult.Width)
        # print("SizeY: ", grabResult.Height)
        img = grabResult.Array
        # print("Gray value of first pixel: ", img[0, 0])
        # Show us our image
        # img = Image.fromarray(img, 'L')
        # img.show()
    grabResult.Release()
    # camera.Close()
    return img





# print(grabResult)