import pyvisa
import time
import sys
import numpy as np
from ThorlabsPM100 import ThorlabsPM100

#Important note, remember to change the driver to PM100D NI-VISA!!! from
# power meter driver switcher

#https://pypi.org/project/ThorlabsPM100/
#https://github.com/clade/ThorlabsPM100
#https://forums.ni.com/t5/LabVIEW/LabVIEW-does-not-recognize-Thorlabs-Power-Meter/td-p/3815515/page/2


def init(beamDiameter = 9, averageCount = 20, wavelength = 658):
    global powerMeter
    rm = pyvisa.ResourceManager()

    inst = rm.open_resource('USB0::0x1313::0x8072::1902396::INSTR') #subject to change
    powerMeter = ThorlabsPM100(inst=inst)

    powerMeter.sense.correction.wavelength = wavelength
    powerMeter.sense.correction.beamdiameter = beamDiameter # maximum 9.5mm
    powerMeter.sense.average.count = averageCount #Sets the averaging rate (1 sample takes approx. 3ms)

    powerMeter.configure.scalar.power()

    print("Set range auto and wait 500ms    ...")
    time.sleep(.5)
    powerMeter.sense.power.dc.range.auto = "ON"
    print("Set bandwidth to Low")
    powerMeter.input.pdiode.filter.lpass.state = 1

    print("Measured value    :", powerMeter.read)
    print("Measured unit    :", powerMeter.sense.power.dc.unit)
    print("Average counts    :", powerMeter.sense.average.count)
    print("Power range limit:", powerMeter.sense.power.dc.range.upper)
    print("Measurement type :", powerMeter.getconfigure)
    print("Current value    :", powerMeter.read)
    print("Wavelength       :", powerMeter.sense.correction.wavelength)

def grab_power():

    global powerMeter

    return powerMeter.read

rm = pyvisa.ResourceManager()
inst = rm.open_resource('USB0::0x1313::0x8072::1902396::INSTR') #subject to change
powerMeter = ThorlabsPM100(inst=inst)
init(beamDiameter = 9, averageCount = 1000, wavelength = 658)

print("Power range limit:", powerMeter.sense.power.dc.range.upper)

# print("Set range auto and wait 500ms    ...")
# time.sleep(.5)
# powerMeter.sense.power.dc.range.auto = "ON"

print("Set bandwidth to Low")
powerMeter.input.pdiode.filter.lpass.state = 1

# start = round(time.time() * 1000)
# for nn in range(100):
#     grab_power()
# end = round(time.time() * 1000)

# print("Elapsed time:", (end-start)/100.0)
res = [] # power meter timing experiment

for mm in range(0,200,10):
    if mm == 0:
        mm = 1
    init(beamDiameter = 9, averageCount = mm, wavelength = 658)
    time.sleep(0.5)  # wait to settle

    start = round(time.time() * 1000)
    for nn in range(100):
        grab_power()
    end = round(time.time() * 1000)
    # print("Elapsed time:", (end-start)/100.0)
    # Store the values
    res.append((end-start)/100.0)

# Now save the measured result to file
time_str = time.strftime("%Y%m%d-%H%M%S")

np.savetxt("E:/GitLab/ExperimentData/20230106/Data"+time_str+".csv", 
           np.asarray(res),
           delimiter =", ", 
           fmt ='% s')

print("Write Complete")

# if len(rm.list_resources()) > 0 and rm.open_resource(rm.list_resources()[0]).query:
#     try:
#         tlpm = rm.open_resource(rm.list_resources()[0])
#         print(rm.list_resources()[0])
#         tlpm.read_termination = '\n'
#         tlpm.write_termination = '\n'
#     except:
#         print('''\nError occured while trying to add the instrument.
#         Restart the python kernel (ctrl + .) and restart the powermeter to retry.''')

# tlpm.write('Initiate:Power') # Sets measurement mode to power [W]

# tlpm.write('Sense:Average:Count 3000') # Averages 3000 measurements per measurement
# input("""======================================================================
# Please deactivate all lasers for zero-adjustment measurement.
# Press Return to procceed.
# ======================================================================\n""")

# tlpm.write('Sense:Correction:Collect:Zero') # Performs Zero adjustment
# time.sleep(1)
# print("======================================================================\n"
# "\tZero adjumstent successfully performed")

# tlpm.write(f'Sense:Correction:Beamdiameter {beamdiameter}') # Sets beamdiameter
# if beamdiameter != 5:
#     print(f"\tBeamdiameter is set to {beamdiameter} mm.\n"
#     "======================================================================")
# else:
#     print(f"\tBeamdiameter was set to default: {beamdiameter} mm.\n"
#     "======================================================================")
