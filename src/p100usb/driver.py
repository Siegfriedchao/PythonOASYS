import pyvisa
import time
from ThorlabsPM100 import ThorlabsPM100

#Important note, remember to change the driver to PM100D NI-VISA!!! from
# power meter driver switcher

def init(beamDiameter = 9, averageCount = 20, wavelength = 658):
    global powerMeter
    rm = pyvisa.ResourceManager()

    inst = rm.open_resource('USB0::0x1313::0x8072::1902396::INSTR') #subject to change
    powerMeter = ThorlabsPM100(inst=inst)

    powerMeter.sense.correction.wavelength = wavelength
    powerMeter.sense.correction.beamdiameter = beamDiameter # maximum 9.5mm
    powerMeter.sense.average.count = averageCount #Sets the averaging rate (1 sample takes approx. 3ms)

    powerMeter.configure.scalar.power() # sets to power

    # powerMeter.system.beeper.state = 1 # turn on beeper, doesnt seem to work

    print("Set range auto and wait 500ms")
    time.sleep(.5)
    powerMeter.sense.power.dc.range.auto = "ON"
    # print("Set bandwidth to High")
    # powerMeter.input.pdiode.filter.lpass.state = 0
    print("Set bandwidth to Low")
    powerMeter.input.pdiode.filter.lpass.state = 1

    print("Measured value:", powerMeter.read)
    print("Measured unit:", powerMeter.sense.power.dc.unit)
    print("Average counts:", powerMeter.sense.average.count)
    print("Power range limit:", powerMeter.sense.power.dc.range.upper)
    print("Measurement type:", powerMeter.getconfigure)
    print("Current value:", powerMeter.read)
    print("Wavelength:", powerMeter.sense.correction.wavelength)

def grab_power():

    global powerMeter
    # print('pool thread',round(time.time() * 1000))
    power = powerMeter.read # takes approx 3ms


    return power

def grab_power_with_delay():
    global powerMeter

    # Equivalent to waiting for a second trigger signal 
    # at 50Hz, while also adding a dummy time delay to 
    # simulate the first trigger signal
    time.sleep(0.025) 

    power = powerMeter.read # takes approx 3ms

    return power

# def return_sound():
#     global powerMeter

#     powerMeter.system.beeper.immediate()