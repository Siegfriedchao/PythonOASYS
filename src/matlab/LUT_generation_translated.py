
# Imports
# Libraries
import time
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

# My own code
import slm.driver as slm
import camera.driver as cam
import p100usb.driver as pwm

from multiprocessing.pool import ThreadPool

NUM_TILES = 5

def gen_LUT(intRange = 383, arr=[1]):

    arr = np.asarray(arr)
    arrLen = len(arr)
    # Sort the array in place
    arr[::-1].sort()
    
    # Create the LUT container
    container = np.zeros([intRange + 1, arrLen])
    for number in range(0,intRange+1):
        currentNum = number
        for i in range(arrLen):
            container[number][i] = np.floor(currentNum/arr[i])
            currentNum -= container[number][i]*arr[i]

    return container

baseElement = [1,4,14,47,160]
baseElement = np.asarray(baseElement)
# LUT = gen_LUT(383, baseElement) # Array sorted in place
# print(LUT)


def load_SLM(baseElement):
    # Generate a pair of integers
    intRange = 383
    intLUT = gen_LUT(intRange, baseElement)
    intA = np.random.randint(0,intRange)
    intB = np.random.randint(0,intRange)
    # intA = 16#0	0	1	0	2
    # intB = 55#0	1	0	1	3

    lenA = len(np.nonzero(intLUT[intA + 1])[0])
    lenB = len(np.nonzero(intLUT[intB + 1])[0])
    elementA = baseElement[np.nonzero(intLUT[intA + 1])]
    elementB = baseElement[np.nonzero(intLUT[intB + 1])]

    # Unload the numbers onto SLM
    SLM1Load = []
    SLM2Load = []
    for iB in range(lenB):
        for iA in range(lenA):
            SLM1Load.append(elementA[iA])
            SLM2Load.append(elementB[iB])

    integersLoad = [intA, intB]
    loadLength = len(SLM1Load)

    # Reshape the matrix to fit the 5x5 panel
    if len(SLM1Load) != NUM_TILES*NUM_TILES:
        SLM1Load = np.append(np.asarray(SLM1Load), np.zeros(NUM_TILES*NUM_TILES - len(SLM1Load)))
        SLM2Load = np.append(np.asarray(SLM2Load), np.zeros(NUM_TILES*NUM_TILES - len(SLM2Load)))

    return SLM1Load, SLM2Load, integersLoad, loadLength

res_SLM1Load = []
res_SLM2Load = []
res_integers = []

SLM1Load, SLM2Load, integers_load, load_length = load_SLM(baseElement)
res_SLM1Load.append(SLM1Load)
res_SLM2Load.append(SLM2Load)
res_integers.append(integers_load)



matrix_BJ_org = SLM1Load.reshape((NUM_TILES,NUM_TILES))
matrix_AU_org = SLM2Load.reshape((NUM_TILES,NUM_TILES))

np.savetxt("E:/RawExperimentData/RandomNumbers/Run2/SLM_load.csv", 
           np.c_[np.asarray(res_SLM1Load), np.asarray(res_SLM2Load)],
           delimiter =", ",
           fmt ='% s')

np.savetxt("E:/RawExperimentData/RandomNumbers/Run2/Integers_load.csv", 
           res_integers,
           delimiter =", ",
           fmt ='% s')

