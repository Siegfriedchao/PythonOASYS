import matlab.engine

eng = matlab.engine.start_matlab()
s = eng.genpath('E:\GitLab\propagation-model')
eng.addpath(s, nargout=0)
a = eng.linspace(-200e-5, 200e-5, 500)

y = eng.CreateSMF(a)

eng.OpticalComputing_4f_1(nargout=0)

print(y)