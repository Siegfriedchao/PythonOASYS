import numpy as np
import matplotlib.pyplot as plt

N = 1080
x = np.linspace(0, 1, N)
image = np.tile(x, (N, 1)).T

plt.imshow(image, cmap='gray')
plt.show()