import numpy as np
from PIL import Image

# # Generate a horiztonal and a vertical grating
# # Make a numpy array first
# x_dim = 640
# y_dim = 640
# total_pixels = x_dim*y_dim
# # Generate a 1-D array programmatically, then convert it to 2-D
# test_gen_array = np.zeros(total_pixels, dtype=np.uint8)
# # Just set every 2nd pixel to on
# on_value = 255
# test_gen_array[::2] = on_value
# # Now reshape to our 2x2 image - this will give us a vertical grating
# vertical_grating_array = test_gen_array.reshape(x_dim, y_dim)
# # We also want a horizontal grating, can get my simply transposing the vertical grating
# horizontal_grating_array = np.transpose(vertical_grating_array)
# # Now generate and save our Images as 8-bit monochrome
# # vertical_grating_image = Image.fromarray(vertical_grating_array, mode='L')
# # Need to convert to 8-bit single channel greyscale or get bug, see - https://stackoverflow.com/questions/32159076/python-pil-bitmap-png-from-array-with-mode-1
# vertical_grating_image_8_bit_array   = np.array(vertical_grating_array, dtype="uint8")
# horizontal_grating_image_8_bit_array = np.array(horizontal_grating_array, dtype="uint8")
# vertical_grating_image               = Image.fromarray(vertical_grating_image_8_bit_array)
# horizontal_grating_image             = Image.fromarray(horizontal_grating_image_8_bit_array)
# # Show our Images is optial - zoom in if we do for viewability
# # vertical_grating_image.resize( size=(1000,1000), resample=Image.NEAREST).show()
# # horizontal_grating_image.resize( size=(1000,1000), resample=Image.NEAREST).show()
# # Print our Images is optional
# # print(np.asarray(vertical_grating_image))
# # print(np.asarray(horizontal_grating_image))
# # Save our Images
# vertical_grating_image.save("./input/vertical_grating.png")
# horizontal_grating_image.save("./input/horizontal_grating.png")

# # Generate a 1280x1280 cherboard array with boxes whicha re 8x8 pixels wide
# # Make a numpy array first
# x_dim = 1280
# y_dim = 1280
# total_pixels = x_dim*y_dim
# num = 64
# # Generate a 1-D array programmatically, then convert it to 2-D
# test_gen_array = np.zeros(0, dtype=np.uint8)
# # Of each 16 pixels, set the top half to be on, bottom half to be off
# on_value = 255
# # Define off lines and even lines
# odd_line = np.zeros(x_dim, dtype=np.uint8)
# for i in np.arange(x_dim):
#     if( i%(num*2) < num):
#         odd_line[i] = on_value
# even_line = np.zeros(x_dim, dtype=np.uint8)
# for i in np.arange(x_dim):
#     if( i%(num*2) >= num):
#         even_line[i] = on_value
# # Build up our 2D array - we do 8 of odd and even alternatively
# for i in range(0, y_dim, (num*2)):
#     for j in range(num):
#         test_gen_array = np.append(test_gen_array, odd_line)
#     for k in range(num):
#         test_gen_array = np.append(test_gen_array, even_line)
# # Now reshape to our 2x2 image - this will give us a vertical grating
# checkerboard_array = test_gen_array.reshape(x_dim, y_dim)
# # Null some of the pixels
# for i in range(int(x_dim/2), x_dim):
#     for j in range(0, y_dim):
#         checkerboard_array[j][i] = 255
# # Now generate and save our Images as 8-bit monochrome
# checkerboard_image = Image.fromarray(checkerboard_array, mode='L')
# checkerboard_image.show()
# checkerboard_image.save("../input/checkerboard_64_pixel_half.bmp")

# # Generate a 1280x1280 cherboard array with boxes whicha re 8x8 pixels wide
# # Make a numpy array first
# x_dim = 2560
# y_dim = 2560
# total_pixels = x_dim*y_dim
# num = 128
# # Generate a 1-D array programmatically, then convert it to 2-D
# test_gen_array = np.zeros(0, dtype=np.uint8)
# # Of each 16 pixels, set the top half to be on, bottom half to be off
# on_value = 255
# # Define off lines and even lines
# odd_line = np.zeros(x_dim, dtype=np.uint8)
# for i in np.arange(x_dim):
#     if( i%(num*2) < num):
#         odd_line[i] = on_value
# even_line = np.zeros(x_dim, dtype=np.uint8)
# for i in np.arange(x_dim):
#     if( i%(num*2) >= num):
#         even_line[i] = on_value
# # Build up our 2D array - we do 8 of odd and even alternatively
# for i in range(0, y_dim, (num*2)):
#     for j in range(num):
#         test_gen_array = np.append(test_gen_array, odd_line)
#     for k in range(num):
#         test_gen_array = np.append(test_gen_array, even_line)
# # Now reshape to our 2x2 image - this will give us a vertical grating
# checkerboard_array = test_gen_array.reshape(x_dim, y_dim)
# # Null some of the pixels
# for i in range(0, int(x_dim/2)):
#     for j in range(0, y_dim):
#         checkerboard_array[i][j] = 255
# # Now generate and save our Images as 8-bit monochrome
# checkerboard_image = Image.fromarray(checkerboard_array, mode='L')
# checkerboard_image.show()
# checkerboard_image.save("../input/checkerboard_128_pixel_half_4k_3.bmp")


### TODO: write up the code to generate 2x2

# Generate a 1280x1280 cherboard array with boxes whicha re 8x8 pixels wide
# Make a numpy array first
x_dim = 1024
y_dim = 1024
total_pixels = x_dim*y_dim
num = 512
# Generate a 1-D array programmatically, then convert it to 2-D
test_gen_array = np.zeros(0, dtype=np.uint8)
# Of each 16 pixels, set the top half to be on, bottom half to be off
on_value = 255
# Define off lines and even lines
odd_line = np.zeros(x_dim, dtype=np.uint8)
for i in np.arange(x_dim):
    if( i%(num*2) < num):
        odd_line[i] = on_value
even_line = np.zeros(x_dim, dtype=np.uint8)
for i in np.arange(x_dim):
    if( i%(num*2) >= num):
        even_line[i] = on_value
# Build up our 2D array - we do 8 of odd and even alternatively
for i in range(0, y_dim, (num*2)):
    for j in range(num):
        test_gen_array = np.append(test_gen_array, odd_line)
    for k in range(num):
        test_gen_array = np.append(test_gen_array, even_line)
# Now reshape to our 2x2 image - this will give us a vertical grating
checkerboard_array = test_gen_array.reshape(x_dim, y_dim)
# # Null some of the pixels
# for i in range(0, int(x_dim/2)):
#     for j in range(0, y_dim):
#         checkerboard_array[i][j] = 255
# Now generate and save our Images as 8-bit monochrome
checkerboard_image = Image.fromarray(checkerboard_array, mode='L')
checkerboard_image.show()
checkerboard_image.save("../input/checkerboard_512.bmp")
"""
# Generate a 1280x1280 cherboard array with boxes which are 1x1 pixels wide
# Make a numpy array first
x_dim = 1280
y_dim = 1280
total_pixels = x_dim*y_dim
# Generate a 1-D array programmatically, then convert it to 2-D
test_gen_array = np.zeros(0, dtype=np.uint8)
# Of each 16 pixels, set the top half to be on, bottom half to be off
on_value = 255
# Define off lines and even lines
odd_line = np.zeros(x_dim, dtype=np.uint8)
for i in np.arange(x_dim):
    if( i%2 < 1):
        odd_line[i] = on_value
even_line = np.zeros(x_dim, dtype=np.uint8)
for i in np.arange(x_dim):
    if( i%2 >= 1):
        even_line[i] = on_value
# Build up our 2D array - we do 8 of odd and even alternatively
for i in range(0, y_dim, 2):
    test_gen_array = np.append(test_gen_array, odd_line)
    test_gen_array = np.append(test_gen_array, even_line)
# Now reshape to our 2x2 image - this will give us a vertical grating
checkerboard_array = test_gen_array.reshape(x_dim, y_dim)
# Now generate and save our Images as 8-bit monochrome
checkerboard_image = Image.fromarray(checkerboard_array, mode='L')
checkerboard_image.show()
checkerboard_image.save("./input/checkerboard_single_pixel.bmp")
"""